<?php /* Smarty version 2.6.25, created on 2013-11-26 09:29:59
         compiled from page/template.tpl */ ?>
<style>
    .yui-editor-container {
        position: absolute;
        top: -9999px;
        left: -9999px;
        z-index: 999;
    }

    #editor {
        visibility: hidden;
        position: absolute;
    }

    .editable {
        border      : 1px dashed #cccccc;
        margin      : .25em;
        min-height  : 180px;
        overflow    : auto;
    }

    div.editable:hover {
        background-color: #FFFFCC;
        border          : 1px dashed #FFE45C;
    }

}

</style>
</head>
<div class="yui-skin-sam"><textarea id="editor_<?php echo $this->_tpl_vars['windowId']; ?>
" name="editor"></textarea></div>
<div id="content_editor_<?php echo $this->_tpl_vars['windowId']; ?>
"></div>
<input type="hidden" id="contentPage_<?php echo $this->_tpl_vars['windowId']; ?>
" name="contentPage" value="">
<div>
    <span>Templates: </span>
    <span style="padding:0px 0px 0px 33px; cursor:pointer">
        <span onclick="javascript:setTemplate(1)"><img src="/image/icon_file/layout/layout1.png" title="Template 1"></span>
        <span onclick="javascript:setTemplate(2)"><img src="/image/icon_file/layout/layout2.png" title="Template 2"></span>
        <span onclick="javascript:setTemplate(3)"><img src="/image/icon_file/layout/layout3.png" title="Template 3"></span>
        <span onclick="javascript:setTemplate(4)"><img src="/image/icon_file/layout/layout4.png" title="Template 4"></span>
        <span onclick="javascript:setTemplate(5)"><img src="/image/icon_file/layout/layout5.png" title="Template 5"></span>
    </span>
</div>
<div id="editable_cont_<?php echo $this->_tpl_vars['windowId']; ?>
">
    <?php if ($this->_tpl_vars['contentPage']): ?>
    <?php echo $this->_tpl_vars['contentPage']; ?>

    <?php else: ?>
    <div style="width:100%"><div class="editable"></div></div>
    <div style="clear:both"></div>
    <div>
        <div style="float:left; width:30%; min-height: 180px"><div class="editable" style="min-height: 180px"></div></div>
        <div style="float:left; width:70%; min-height: 180px"><div class="editable" style="min-height: 180px"></div></div>
    </div>
    <div style="clear:both"></div>
    <?php endif; ?>
</div>

<script>


function setTemplate(id)
{
    var content = "";

    switch(id){

        case 1: content = '<div style="width:100%"><div class="editable"></div></div>'+
             '<div style="clear:both"></div>'+
             '<div>'+
                '<div style="float:left; width:30%; min-height: 180px"><div class="editable" style="min-height: 180px"></div></div>'+
                '<div style="float:left; width:70%; min-height: 180px"><div class="editable" style="min-height: 180px"></div></div>'+
             '</div>'+
             '<div style="clear:both"></div>';
            break;
       case 2: content =  '<div>'+
                '<div style="float:left; width:33%; min-height: 400px"><div class="editable" style="min-height: 400px"></div></div>'+
                '<div style="float:left; width:33%; min-height: 400px"><div class="editable" style="min-height: 400px"></div></div>'+
                '<div style="float:left; width:33%; min-height: 400px"><div class="editable" style="min-height: 400px"></div></div>'+
             '</div>'+
             '<div style="clear:both"></div>';
            break;
       case 3: content = '<div>'+
                '<div style="width:100%; min-height: 150px"><div class="editable" style="min-height: 150px"></div></div>'+
                '<div style="width:100%; min-height: 150px"><div class="editable" style="min-height: 150px"></div></div>'+
                '<div style="width:100%; min-height: 150px"><div class="editable" style="min-height: 150px"></div></div>'+
             '</div>'+
             '<div style="clear:both"></div>';
            break;
       case 4: content = '<div>'+
                '<div style="float:left; width:100%; min-height: 20px"><div class="editable" style="min-height: 20px"></div></div>'+
             '</div>'+
             '<div style="clear:both"></div>';
            break;
       case 5: content = '<div>'+
                '<div style="width:100%; min-height: 200px"><div class="editable" style="min-height: 200px"></div></div>'+
                '<div style="width:100%; min-height: 200px"><div class="editable" style="min-height: 200px"></div></div>'+
             '</div>'+
             '<div style="clear:both"></div>';
            break;

       default: content = 1;      

    }

    $("#editable_cont_<?php echo $this->_tpl_vars['windowId']; ?>
").html(content);

}

(function() {
    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        editing = null;

    var myConfig = {
        height  : '250px',
        width   : '750px',
        animate : true
    };

    YAHOO.widget.Toolbar.prototype.STR_COLLAPSE = 'Clique para fechar o editor.';

    myEditor = new YAHOO.widget.Editor('editor_<?php echo $this->_tpl_vars['windowId']; ?>
', myConfig);
    myEditor.on('toolbarLoaded', function() {
        this.toolbar.on('toolbarCollapsed', function() {
            Dom.setXY(this.get('element_cont').get('element'), [-99999, -99999]);
            Dom.removeClass(this.toolbar.get('cont').parentNode, 'yui-toolbar-container-collapsed');
            myEditor.saveHTML();
            editing.innerHTML = myEditor.get('element').value;
            document.getElementById('contentPage_<?php echo $this->_tpl_vars['windowId']; ?>
').value = $("#editable_cont_<?php echo $this->_tpl_vars['windowId']; ?>
").load().html();
            editing = null;
            YAHOO.example.container.wait.hide();
        }, myEditor, true);
    }, myEditor, true);
    myEditor.render();

    Event.on('editable_cont_<?php echo $this->_tpl_vars['windowId']; ?>
', 'dblclick', function(ev) {
        
        // TODO
        //YAHOO.example.container.wait.show();
        var tar = Event.getTarget(ev);
        while(tar.id != 'editable_cont_<?php echo $this->_tpl_vars['windowId']; ?>
') {
            if (Dom.hasClass(tar, 'editable')) {

                if (editing !== null) {
                    myEditor.saveHTML();
                    editing.innerHTML = myEditor.get('element').value;
                }

                //var xy = Dom.getXY(tar);
                var xy = Dom.getXY("<?php echo $this->_tpl_vars['windowId']; ?>
");

                xy = Array(xy[0]+20, xy[1]+20)

                myEditor.setEditorHTML(tar.innerHTML);

                Dom.setXY(myEditor.get('element_cont').get('element'), xy);
                editing = tar;
            }
            tar = tar.parentNode;
        }
    });

    myEditor.on('toolbarLoaded', function() {
        //When the toolbar is loaded, add a listener to the insertimage button
        this.toolbar.on('insertimageClick', function() {
            //Get the selected element
            var _sel = this._getSelectedElement();
            //If the selected element is an image, do the normal thing so they can manipulate the image
            if (_sel && _sel.tagName && (_sel.tagName.toLowerCase() == 'img')) {
                //Do the normal thing here..
            } else {
                //Abra uma janela com as imagens
                Window().create({'url':'/file/showFileDir/?windowParent=<?php echo $this->_tpl_vars['windowId']; ?>
', 'title':'Disco virtual', 'icon':'group', 'width':920, 'height':400, 'modal':true});
                return false;
            }
        }, this, true);
    }, myEditor, true);

})();

    YAHOO.namespace("example.container");

    function createMask() {

        var content = document.getElementById("content_editor_<?php echo $this->_tpl_vars['windowId']; ?>
");

        content.innerHTML = "";

        if (!YAHOO.example.container.wait) {

            // Initialize the temporary Panel to display while waiting for external content to load

            idIndex = $( "#dialog-modal" ).css("z-index");

            // atualiza o dialog-modal
            $( "#dialog-modal" ).css("z-index", idIndex+1);

            YAHOO.example.container.wait =
                    new YAHOO.widget.Panel("wait",
                                                    { width         : "240px",
                                                      fixedcenter   : true,
                                                      close         : false,
                                                      draggable     : false,
                                                      zindex        : idIndex,
                                                      modal         : true,
                                                      visible       : false
                                                    }
                                                );

            //YAHOO.example.container.wait.setHeader("Loading, please wait...");
            YAHOO.example.container.wait.setBody('<div class="yui-skin-sam"><textarea id="editor_<?php echo $this->_tpl_vars['windowId']; ?>
" name="editor"></textarea></div>');
            YAHOO.example.container.wait.render(document.body);

        }

        // Show the Panel
        
    }
//createMask();
</script>