<?php /* Smarty version 2.6.25, created on 2015-03-16 15:04:34
         compiled from lineBus/showDataCourse.tpl */ ?>
<style>
    .detailCourse > li{
        padding: 3px;
        list-style:none;
        border: 1px solid #cccccc;
        margin: 2px;
        width: 400px;
        cursor:move;
        padding: 5px;
        background: #cccccc;
    }
    
    .detailCourse > li:hover{
        background: #999999;
        color: #ffffff;
    }
    
</style>
<input type="hidden" id="id_line_<?php echo $this->_tpl_vars['windowId']; ?>
"      value="<?php echo $this->_tpl_vars['idLine']; ?>
"/>
<input type="hidden" id="id_table_<?php echo $this->_tpl_vars['windowId']; ?>
"      value="<?php echo $this->_tpl_vars['idTable']; ?>
"/>
<input type="hidden" id="id_initFrom_<?php echo $this->_tpl_vars['windowId']; ?>
"  value="<?php echo $this->_tpl_vars['idInitFrom']; ?>
"/>
<input type="hidden" id="id_frequency_<?php echo $this->_tpl_vars['windowId']; ?>
" value="<?php echo $this->_tpl_vars['idFrequency']; ?>
"/>
<div>
    <div style="margin-left: 68px">
        <?php echo $this->_tpl_vars['elementCourse']; ?>

    </div>
    <div style="height: 242px; overflow: auto">
        <ul id="contentCourseItem_<?php echo $this->_tpl_vars['windowId']; ?>
" class="detailCourse">
            <?php $_from = $this->_tpl_vars['arrElement']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                <li id="<?php echo $this->_tpl_vars['item']['id']; ?>
" <?php if ($this->_tpl_vars['item']['position'] == 'first' || $this->_tpl_vars['item']['position'] == 'last'): ?> class="ui-state-disabled" style="background:#eeeeee;color:#666666"<?php endif; ?>><?php if ($this->_tpl_vars['item']['position'] != 'first' && $this->_tpl_vars['item']['position'] != 'last'): ?><a href="javascript:;"><img src="/image/icon/delete.png"></a> <?php endif; ?> <?php echo $this->_tpl_vars['item']['label']; ?>
</li>
            <?php endforeach; endif; unset($_from); ?>
        </ul>
    </div>
</div>
<div style="clear: both"></div>
<div id="buttons_<?php echo $this->_tpl_vars['windowId']; ?>
">
    <span id="saveHour_<?php echo $this->_tpl_vars['windowId']; ?>
" class="classButton"><span style="background: url('/image/icon/accept.png') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span>Salvar</span></span>
</div>