<?php /* Smarty version 2.6.25, created on 2015-05-15 09:38:22
         compiled from element/tab.tpl */ ?>
<div id="<?php echo $this->_tpl_vars['name']; ?>
" class="yui-navset" style="margin-top:7px">
    <ul class="yui-nav">
        <?php $_from = $this->_tpl_vars['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
            <li <?php if ($this->_tpl_vars['selected'] == $this->_tpl_vars['key']): ?>class="selected"<?php endif; ?>><a href="#<?php echo $this->_tpl_vars['key']; ?>
"><em><?php echo $this->_tpl_vars['item']['label']; ?>
</em></a></li>
        <?php endforeach; endif; unset($_from); ?>
    </ul>
    <div class="yui-content">
        <?php $_from = $this->_tpl_vars['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
            <div id="<?php echo $this->_tpl_vars['key']; ?>
"><p><?php echo $this->_tpl_vars['item']['data']; ?>
</p></div>
        <?php endforeach; endif; unset($_from); ?>
    </div>
</div>
<script>
    (function() {
        var tabView = new YAHOO.widget.TabView('<?php echo $this->_tpl_vars['name']; ?>
');
    })();
</script>