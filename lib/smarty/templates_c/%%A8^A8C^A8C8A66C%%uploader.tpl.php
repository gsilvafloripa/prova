<?php /* Smarty version 2.6.25, created on 2013-11-22 14:53:50
         compiled from file/uploader.tpl */ ?>

<fieldset>
    <legend>Selecione o arquivo</legend>
<div>
<form id="formImportData_<?php echo $this->_tpl_vars['windowId']; ?>
" action="/file/requestFile" method="POST">
<input type="hidden" id="folder_<?php echo $this->_tpl_vars['windowId']; ?>
" name="folder" value="<?php echo $this->_tpl_vars['folder']; ?>
"/>
<div>
	<div style="margin-top: 10px;">
		<input type="file" id="inputFile" name="inputFile">
	</div>
</div>
</form>
</div>
</fieldset>
<div id="buttons_up_<?php echo $this->_tpl_vars['windowId']; ?>
">
    <span id="saveUpload_<?php echo $this->_tpl_vars['windowId']; ?>
" class="classButton"><span style="background: url('/image/icon/accept.png') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span>upload</span></span>
</div>

<script type="text/javascript">
var windowId = "<?php echo $this->_tpl_vars['windowId']; ?>
";

$("#content_" + windowId).ready(function() {
	
	var divButtonsContent = $('#content_' + windowId + ' #buttons_up_' + windowId);
	
	if($('#content_bar_' + windowId + ' #buttons_up_' + windowId)[0] == undefined){
	
		var divHeight = $('#' + windowId).parent().height() + 46
		$('#' + windowId).parent().css('height', divHeight + 'px');
	
		var content_bar = "<div id='content_bar_" + windowId + "' style='height:40px; bottom:0px; padding: 4px 4px 0px 0px; background:#FFFFFF; border-top: 1px solid #eeeeee'></div>";
		$('#' + windowId).after(content_bar);
	
		$("#content_bar_" + windowId).ready(function() {
	
			// clona o elemento dos botões para o content_bar
			divButtonsContent.clone().prependTo("#content_bar_" + windowId);
			// remove o elemento
			divButtonsContent.remove();
	
			$('#saveUpload_' + windowId).click(function(){ $('#formImportData_' + windowId).submit(); });
	
		});
	
	}else{
	
		// remove o elemento
		divButtonsContent.remove();
	
	}
	
	var objFunc = Functions();
	
	$('#formImportData_' + windowId).ajaxForm({
		type 		 : 'POST',
		beforeSubmit : function(){ Window().maskWindow( windowId, 'in' ); },
		success 	 : function(response){
	
			response = eval("(" + response + ")");
	
			if(response.success == false){
	
				objFunc.messageModal(response.title, response.content);
	
			}else{
	
				objFunc.messageModal(response.title, response.content);
	
				$('#' + windowId).remove();
	
			}

			Window().maskWindow( windowId, 'out' );

		}
	});

});
</script>