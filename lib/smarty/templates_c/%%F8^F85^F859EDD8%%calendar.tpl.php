<?php /* Smarty version 2.6.25, created on 2015-06-03 10:55:41
         compiled from element/calendar.tpl */ ?>
<style>
.yui-calcontainer{
    border: 1px solid #cccccc !important;
}
.title{
    color:#999999 !important;
}
</style>
<style>
    #cal2Container_<?php echo $this->_tpl_vars['windowId']; ?>
 { display:none; position:absolute; left:240px; top:auto; z-index:1; bottom:25px}
</style>
<label for="title_<?php echo $this->_tpl_vars['windowId']; ?>
" style="float:left; margin:6px 5px 0px 0px; min-width:100px"><?php echo $this->_tpl_vars['label']; ?>
</label>
<div style="float:left">
    <INPUT type="text" name="calendarDate" id="calendarDate_<?php echo $this->_tpl_vars['windowId']; ?>
" value="<?php if ($this->_tpl_vars['selectedDate'] <> ''): ?><?php echo $this->_tpl_vars['selectedDate']; ?>
<?php endif; ?>" size="8" disabled=disabled>
</div>
<div style="float:left; padding:4px"><span id="showCalendar_<?php echo $this->_tpl_vars['windowId']; ?>
" style="cursor:pointer"><image id="imgCalendar_<?php echo $this->_tpl_vars['windowId']; ?>
" src="image/icon/calendar.png"/></span></div>
<div class="yui-skin-sam" style="font-size:10px;">
    <div id="cal2Container_<?php echo $this->_tpl_vars['windowId']; ?>
"></div>
</div>
</div>
<div id="hourMinute_<?php echo $this->_tpl_vars['windowId']; ?>
" style="<?php if ($this->_tpl_vars['selectedHour'] == '' || $this->_tpl_vars['selectedMinute'] == ''): ?>display:none<?php endif; ?>">
<div id="calendar_hour_<?php echo $this->_tpl_vars['windowId']; ?>
" style="float:left">
    <SELECT style="visibility:visible" name="calendarHour" id="calendarHour_<?php echo $this->_tpl_vars['windowId']; ?>
">
        <?php if (is_array ( $this->_tpl_vars['calendarHour'] )): ?>
            <?php $_from = $this->_tpl_vars['calendarHour']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                <OPTION <?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['selectedHour']): ?> selected <?php endif; ?>  value="<?php echo $this->_tpl_vars['item']['value']; ?>
"><?php echo $this->_tpl_vars['item']['label']; ?>
</OPTION>
            <?php endforeach; endif; unset($_from); ?>
        <?php endif; ?>
    </SELECT>
</div>

<div style="padding:4px 3px 0px 3px; float:left">:</div>
<div id="calendar_minute_<?php echo $this->_tpl_vars['windowId']; ?>
" style="float:left">
    <SELECT style="visibility:visible" name="calendarMinute" id="calendarMinute_<?php echo $this->_tpl_vars['windowId']; ?>
">
        <?php if (is_array ( $this->_tpl_vars['calendarMinute'] )): ?>
            <?php $_from = $this->_tpl_vars['calendarMinute']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                <OPTION <?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['selectedMinute']): ?> selected <?php endif; ?> value="<?php echo $this->_tpl_vars['item']['value']; ?>
"><?php echo $this->_tpl_vars['item']['label']; ?>
</OPTION>
            <?php endforeach; endif; unset($_from); ?>
        <?php endif; ?>
    </SELECT>
</div>
<div id="remove_<?php echo $this->_tpl_vars['windowId']; ?>
" style="float:left; margin: 7px; cursor: pointer"><img src="/image/icon/cross.png" title="Remover data"/></div>
</div>
<div style="clear:both"></div>
<input type="hidden" value="<?php if ($this->_tpl_vars['selectedDateDatabase'] <> ''): ?><?php echo $this->_tpl_vars['selectedDateDatabase']; ?>
<?php endif; ?>" name="<?php echo $this->_tpl_vars['name']; ?>
_dateDatabase" id="dateDatabase_<?php echo $this->_tpl_vars['windowId']; ?>
"/>
<input type="hidden" value="" name="<?php echo $this->_tpl_vars['name']; ?>
_dateView" id="dateView_<?php echo $this->_tpl_vars['windowId']; ?>
"/>
<script type="text/javascript">
    YAHOO.namespace("example.calendar");
    YAHOO.example.calendar.init = function() {

       function selectHandler(type, args, obj) {

                var selected = args[0];

                try{
                    obj.hide();
                }catch(e){}

                var elHour   = new YAHOO.util.Element('calendarHour_<?php echo $this->_tpl_vars['windowId']; ?>
').get('value');
                var elMinute = new YAHOO.util.Element('calendarMinute_<?php echo $this->_tpl_vars['windowId']; ?>
').get('value');

                var dateDatabase = selected[0][0] + "-" + selected[0][1] + "-" + selected[0][2] + " " + elHour + ":" + elMinute;
                var dateView     = selected[0][2] + "/" + selected[0][1] + "/" + selected[0][0];

                var calendarDate = new YAHOO.util.Element('calendarDate_<?php echo $this->_tpl_vars['windowId']; ?>
').set('value', dateView);
                var calendarDate = new YAHOO.util.Element('dateDatabase_<?php echo $this->_tpl_vars['windowId']; ?>
').set('value', dateDatabase);
                var calendarDate = new YAHOO.util.Element('dateView_<?php echo $this->_tpl_vars['windowId']; ?>
').set('value', dateView + " " + elHour + ":" + elMinute);

                YAHOO.util.Dom.setStyle('hourMinute_<?php echo $this->_tpl_vars['windowId']; ?>
', 'display', 'block');

 	};

        // instância o elemento calendário
        YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar("cal2_<?php echo $this->_tpl_vars['windowId']; ?>
","cal2Container_<?php echo $this->_tpl_vars['windowId']; ?>
", { title:"Seleciona uma data:", close:true } );
        YAHOO.example.calendar.cal2.selectEvent.subscribe(selectHandler, YAHOO.example.calendar.cal2, true);
        YAHOO.example.calendar.cal2.render();

        // Ao clicar em uma data do editor seta o hidden e fecha o elemento
        YAHOO.util.Event.addListener("showCalendar_<?php echo $this->_tpl_vars['windowId']; ?>
", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);

        // Atualiza os dados do hidden
        function update() {

            var valueDate    = new YAHOO.util.Element('calendarDate_<?php echo $this->_tpl_vars['windowId']; ?>
').get('value');
            valueDate = valueDate.split("/");
            valueDate = Array(Array(Array(valueDate[2], valueDate[1], valueDate[0])));

            selectHandler('update', valueDate);

        }

        // Adiciona o evento de onChange nos campos de hora e minuto para atualizar os dados
        YAHOO.util.Event.addListener(["calendarHour_<?php echo $this->_tpl_vars['windowId']; ?>
", "calendarMinute_<?php echo $this->_tpl_vars['windowId']; ?>
"], "change", update);

        function removeDate()
        {
            var calendarDate = new YAHOO.util.Element('calendarDate_<?php echo $this->_tpl_vars['windowId']; ?>
').set('value', '');
            var calendarDate = new YAHOO.util.Element('dateDatabase_<?php echo $this->_tpl_vars['windowId']; ?>
').set('value', '');
            var calendarDate = new YAHOO.util.Element('dateView_<?php echo $this->_tpl_vars['windowId']; ?>
').set('value', '');

            YAHOO.util.Dom.setStyle("hourMinute_<?php echo $this->_tpl_vars['windowId']; ?>
", 'display', "none");
        }

        // Adiciona o evento de onChange nos campos de hora e minuto para atualizar os dados
        YAHOO.util.Event.addListener("remove_<?php echo $this->_tpl_vars['windowId']; ?>
", "click", removeDate);
    }
    YAHOO.util.Event.onDOMReady(YAHOO.example.calendar.init);
</script>