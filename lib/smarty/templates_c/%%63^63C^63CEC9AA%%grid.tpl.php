<?php /* Smarty version 2.6.25, created on 2013-11-26 09:29:53
         compiled from element/grid.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'element/grid.tpl', 57, false),)), $this); ?>
<style>
    
    .contentGridPaginationLink ul{
        margin : 0px;
        padding : 0px;
    }
    
    .contentGridPaginationLink li{
        float           : left;
        list-style-type : none;
        padding         : 3px 3px 3px 3px;
        border          : 1px solid #C1C1C1;
        margin          : 0px 0px 0px 1px;
        background      : #F5F5F5;
        color           : #999999;
        min-width       : 17px;
        height          : 15px;
        border          : 1px solid #C1C1C1;
        cursor          : pointer;
        text-align      : center;
        -moz-border-radius : 2px;
    }
    
    .contentGridPaginationLink li:hover{
        background : #FCEC94;
        color      : #333333;
    }

    .contentGridPaginationLink selected{
        background:#FFFFFF;
    }

    /*.bd{
        border: 1px solid #cccccc !important;
        border-radius: 4px;
    }*/

    .yui-menu-shadow-visible{
        border-radius: 4px;
    }
    
</style>
<div id="componentGrid_<?php echo $this->_tpl_vars['windowId']; ?>
" style="width:<?php echo $this->_tpl_vars['width']; ?>
; height:auto">
<div id="contentGrid_<?php echo $this->_tpl_vars['windowId']; ?>
">
<div>

<table id="table_<?php echo $this->_tpl_vars['windowId']; ?>
" width="100%" cellspacing="0" cellpadding="0" class="tableHeader">
    <tbody>
        <tr class="tableBgHeader">
            <?php $_from = $this->_tpl_vars['metadata']['metadata']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                <?php if ($this->_tpl_vars['item']['id'] <> 'id'): ?>
                <th align="<?php echo $this->_tpl_vars['item']['align']; ?>
" width="<?php echo $this->_tpl_vars['item']['width']; ?>
"><?php echo $this->_tpl_vars['item']['header']; ?>
</th>
                <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
        </tr>

        <?php if (count($this->_tpl_vars['metadata']['data']) > 0): ?>
            <?php $this->assign('firstTd', true); ?>
            <?php $this->assign('firstTr', false); ?>

            <?php $_from = $this->_tpl_vars['metadata']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>

                <tr <?php if ($this->_tpl_vars['firstTr'] == true): ?> class="tableBgRow" <?php endif; ?>>
                    <?php if ($this->_tpl_vars['firstTr'] == true): ?> <?php $this->assign('firstTr', false); ?> <?php else: ?> <?php $this->assign('firstTr', true); ?> <?php endif; ?>
                
                    <?php $_from = $this->_tpl_vars['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['keyL'] => $this->_tpl_vars['itemL']):
?>
            
                        <?php if ($this->_tpl_vars['metadata']['metadata'][$this->_tpl_vars['keyL']]['id'] <> 'id'): ?>
                        <?php if ($this->_tpl_vars['firstTd'] == true): ?>
                        <td id="<?php echo $this->_tpl_vars['metadata']['data'][$this->_tpl_vars['key']][0]; ?>
" class="rowSegment">
                            <div style="background: none repeat scroll 0% 0% rgb(37, 158, 1); float: left; margin: 2px 5px 0pt 0pt ! important;"></div>
                            <?php if ($this->_tpl_vars['itemL'] <> ''): ?><?php echo $this->_tpl_vars['itemL']; ?>
<?php else: ?> . <?php endif; ?>
                        </td>
                         <?php $this->assign('firstTd', false); ?>
                        <?php else: ?>
                        <td id="<?php echo $this->_tpl_vars['metadata']['data'][$this->_tpl_vars['key']][0]; ?>
" class="rowRight"><?php if ($this->_tpl_vars['itemL'] <> ''): ?><?php echo $this->_tpl_vars['itemL']; ?>
<?php else: ?> &nbsp <?php endif; ?></td>
                        <?php endif; ?>
                        <?php endif; ?>
                    <?php endforeach; endif; unset($_from); ?>
                    <?php $this->assign('firstTd', true); ?>
                </tr>
            <?php endforeach; endif; unset($_from); ?>
        <?php else: ?>
        <div>Nenhum registro encontrado.</div>
        <?php endif; ?>
    </tbody>
</table>

</div>
<div  id="contentGridPagination_<?php echo $this->_tpl_vars['windowId']; ?>
"style="width:100%; height:30px; padding:10px 0px 10px 0px; float:none">
    <?php if (count($this->_tpl_vars['metadata']['data']) > 0): ?>
    <div style="float:left">
    <?php if ($this->_tpl_vars['pagination'] == true): ?>
    <select id="pagLimit_<?php echo $this->_tpl_vars['windowId']; ?>
">
        <?php $_from = $this->_tpl_vars['pagLimit']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
            <option label="<?php echo $this->_tpl_vars['item']; ?>
" <?php if ($this->_tpl_vars['item'] == $this->_tpl_vars['limit']): ?> selected="selected" <?php endif; ?> value="<?php echo $this->_tpl_vars['item']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</option>
        <?php endforeach; endif; unset($_from); ?>
    </select>
    <?php endif; ?>
    </div>
    <div class="contentGridPaginationLink" id="contentGridPaginationLink_<?php echo $this->_tpl_vars['windowId']; ?>
" style="float:left; margin:2px 1px 1px 3px">
        <ul>
            <?php if ($this->_tpl_vars['pagination'] == true): ?>
            <li id="_<?php echo $this->_tpl_vars['linkPaginationFirst']; ?>
"><< </li>
            <li id="_<?php echo $this->_tpl_vars['before']; ?>
"><</li>
            <?php endif; ?>
            <?php $_from = $this->_tpl_vars['linkPagination']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                <li id="<?php echo $this->_tpl_vars['item']; ?>
" <?php if ($this->_tpl_vars['item'] == $this->_tpl_vars['start']): ?> style="background:#FCEC94; color:#333333;" <?php endif; ?>> <?php echo $this->_tpl_vars['key']; ?>
 </li>
            <?php endforeach; endif; unset($_from); ?>
            <?php if ($this->_tpl_vars['pagination'] == true): ?>
            <li id="_<?php echo $this->_tpl_vars['after']; ?>
" > > </li>
            <li id="_<?php echo $this->_tpl_vars['linkPaginationLast']; ?>
"> >> </li>
            <?php endif; ?>
        </ul>
    </div>
    <?php endif; ?>
</div>
</div>
</div>
<input type="hidden" id="pagStart_<?php echo $this->_tpl_vars['windowId']; ?>
" name="pagStart" value="<?php echo $this->_tpl_vars['pagStart']; ?>
">