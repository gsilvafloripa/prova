<?php /* Smarty version 2.6.25, created on 2015-06-03 10:55:41
         compiled from element/select.tpl */ ?>
<div>
    <label style="float:left; margin:5px 5px 0px 0px; min-width:100px" for="<?php echo $this->_tpl_vars['name']; ?>
_<?php echo $this->_tpl_vars['windowId']; ?>
"><?php echo $this->_tpl_vars['label']; ?>
</label>
    <SELECT style="visibility:visible; <?php if ($this->_tpl_vars['size']): ?>width: <?php echo $this->_tpl_vars['size']; ?>
 <?php endif; ?>" name="<?php echo $this->_tpl_vars['name']; ?>
" id="<?php echo $this->_tpl_vars['name']; ?>
_<?php echo $this->_tpl_vars['windowId']; ?>
" <?php if ($this->_tpl_vars['disabled'] == true): ?>disabled="disabled"<?php endif; ?>>
        <?php if (is_array ( $this->_tpl_vars['data'] )): ?>
            <?php $_from = $this->_tpl_vars['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                <OPTION <?php if ($this->_tpl_vars['key'] == $this->_tpl_vars['valueSelected']): ?> selected <?php endif; ?> label="<?php echo $this->_tpl_vars['item']; ?>
" value="<?php echo $this->_tpl_vars['key']; ?>
"><?php echo $this->_tpl_vars['item']; ?>
</OPTION>
            <?php endforeach; endif; unset($_from); ?>
        <?php endif; ?>
    </SELECT>
</div>