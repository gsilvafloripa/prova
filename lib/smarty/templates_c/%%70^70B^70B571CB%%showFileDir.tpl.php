<?php /* Smarty version 2.6.25, created on 2013-11-25 00:33:42
         compiled from file/showFileDir.tpl */ ?>
<div style="padding: 10px 10px 10px 25px">
    <div>
    <div style="float:left;margin-bottom: 10px; height: 30px"><?php echo $this->_tpl_vars['listFolder']; ?>
<div style="clear:both"></div></div>
    <div style="float:left;margin:10px 0px 0px 700px; cursor:pointer"><img src="/image/icon/add.png" id="create_new_folder_<?php echo $this->_tpl_vars['windowId']; ?>
" title="Nova pasta" style="margin:5px"/><img style="margin:5px" src="/image/icon/arrow_refresh.png" title="Atualizar" id="refresh_virutal_disk_<?php echo $this->_tpl_vars['windowId']; ?>
"/></div>
    <div style="clear:both"></div>
    </div>
    <div id="images_dir_<?php echo $this->_tpl_vars['windowId']; ?>
" style="width: 100%; font-size:10px">
    <?php echo $this->_tpl_vars['files']; ?>

    </div>
</div>
<script>

    // Atualiza a pasta com o tipo selecionado
    YAHOO.util.Event.addListener("folder_<?php echo $this->_tpl_vars['windowId']; ?>
", "change", function(){
        refreshVirtualDisk();
    });

    // Atualiza a pasta com o tipo selecionado
    YAHOO.util.Event.addListener("refresh_virutal_disk_<?php echo $this->_tpl_vars['windowId']; ?>
", "click", function(){
        refreshVirtualDisk();
    });

    // Abre janela para criar uma nova pasta no disco virtual
    YAHOO.util.Event.addListener("create_new_folder_<?php echo $this->_tpl_vars['windowId']; ?>
", "click", function(){
        Window().create({'url':'/file/showCreateNewFolder/', 'title':'Criar nova pasta', 'icon':'folder_add', 'width':400, 'height':200, 'modal':1})
    });

    function refreshVirtualDisk()
    {

        Window().maskWindow('<?php echo $this->_tpl_vars['windowId']; ?>
', 'in');

        var folder = new YAHOO.util.Element('folder_<?php echo $this->_tpl_vars['windowId']; ?>
').get('value');

        YAHOO.util.Connect.asyncRequest ( "GET" , "/file/showListFileDir?folder="+folder, {

            success : function(obj){
                var objReturn = eval("(" + obj.responseText + ")");
                result        = objReturn.result.response;
                // atualiza os dados
                YAHOO.util.Dom.get("images_dir_<?php echo $this->_tpl_vars['windowId']; ?>
").innerHTML = result.content;

                Window().maskWindow('<?php echo $this->_tpl_vars['windowId']; ?>
', 'out');
            }
        });

        }

    <?php if ($this->_tpl_vars['windowParent'] && ! $this->_tpl_vars['elementParent']): ?>
        (function() {
            var Dom  = YAHOO.util.Dom,
            Event    = YAHOO.util.Event,
            myEditor = YAHOO.widget.EditorInfo.getEditorById("editor_<?php echo $this->_tpl_vars['windowParent']; ?>
");

            //Add a listener to the parent of the images
            Event.on("images_dir_<?php echo $this->_tpl_vars['windowId']; ?>
", 'click', function(ev) {
                var tar = Event.getTarget(ev);

                //Somente para imagens
                if (tar && tar.tagName && (tar.tagName.toLowerCase() == 'img')) {

                    //Coloca o foco no editor
                    myEditor._focusWindow();

                    pathFile = tar.getAttribute('src', 2);

                    fileInfo = pathFile.split('?file=');

                    if(fileInfo.length >= 2){

                        fileName = fileInfo[1].split('/');
                        size     = fileName.length;
                        fileName = fileName[size-1];

                        insertFile = '<a href="'+fileInfo[1]+'" target="_blank">'+fileName+'</a>';

                        myEditor.execCommand('inserthtml', insertFile);
                    }else{
                        insertFile = pathFile;
                        myEditor.execCommand('insertimage', insertFile);
                    }

                    //Fecha a janela
                    $("#<?php echo $this->_tpl_vars['windowId']; ?>
").remove();
                }
            });
        })();
    <?php endif; ?>
    <?php if ($this->_tpl_vars['elementParent']): ?>
        (function() {
            var Dom  = YAHOO.util.Dom,
            Event    = YAHOO.util.Event;

            var imgHeight = <?php if ($this->_tpl_vars['imgHeight']): ?><?php echo $this->_tpl_vars['imgHeight']; ?>
<?php else: ?>''<?php endif; ?>;
            var imgWidth  = <?php if ($this->_tpl_vars['imgWidth']): ?><?php echo $this->_tpl_vars['imgWidth']; ?>
<?php else: ?>''<?php endif; ?>;

            //Add a listener to the parent of the images
            Event.on("images_dir_<?php echo $this->_tpl_vars['windowId']; ?>
", 'click', function(ev) {

                var setImg = true;

                var tar = Event.getTarget(ev);

                //adiciona somente para imagens
                if (tar && tar.tagName && (tar.tagName.toLowerCase() == 'img')) {
                    // verifica se foi passado dimensoes para a imagem
                    if(imgHeight && imgWidth){
                        // atributos de dimensoes da imagem
                        var data = tar.getAttribute('data')
                        data = data.split("x");

                        if((imgWidth > data[0] && data[0] <= 0) || (imgHeight > data[1] && data[1] <= 0 )){
                            setImg = false;
                        }
                    }

                    if(setImg == true){
                        // instância o elemento
                        el = YAHOO.util.Dom.get("<?php echo $this->_tpl_vars['elementParent']; ?>
");

                        // adiciona ao elemento da janela
                        el.innerHTML = "<img src='"+ tar.getAttribute('src', 2)+"' />"

                        // seta o hidden
                        new YAHOO.util.Element('elementFile_<?php echo $this->_tpl_vars['name']; ?>
_<?php echo $this->_tpl_vars['windowParent']; ?>
').set('value', tar.getAttribute('src', 2));

                        //Fecha a janela
                        $("#<?php echo $this->_tpl_vars['windowId']; ?>
").remove();
                    }else{
                        // alerta de erro
                        idDialog = "#dialog-modal";

                        // insere a mensagem de erro
                        $( idDialog ).html( "Selecione uma imagem com as seguintes especificações:<br><br>Largura: <b>" + imgWidth + "px</b><br>Altura: <b>" + imgHeight + "px</b>" );

                        // abre uma informação de erro
                        $( idDialog ).dialog({
                                modal           : true,
                                draggable	: false,
                                resizable	: false,
                                title           : "<div style='float:left; background:url(/image/icon/cross.png) no-repeat scroll -1px 1px transparent; padding: 0px 0px 0px 17px'></di><div>Erro</div>",
                                zIndex          : 1000000,
                                buttons         : {Ok: function(){$(this).dialog( "close" );}}
                        });
                    }
                }
            }, imgHeight, imgWidth);
        })();
    <?php endif; ?>
</script>