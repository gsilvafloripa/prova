<?php /* Smarty version 2.6.25, created on 2013-12-03 16:02:18
         compiled from file/showFileMovie.tpl */ ?>
<div style="padding: 40px 10px 10px 25px">
    <div style="float: left">
        <label for="url_<?php echo $this->_tpl_vars['windowId']; ?>
" style="float:left; margin:6px 5px 0px 0px; min-width:80px">Endereço YouTube</label><input type="text" maxlength="300" size="50" value="http://www.youtube.com/embed/" id="url_<?php echo $this->_tpl_vars['windowId']; ?>
" name="url">
        <div style="clear: both"></div>
    </div>
    <div id="buttons_<?php echo $this->_tpl_vars['windowId']; ?>
">
        <span id="button_<?php echo $this->_tpl_vars['windowId']; ?>
" class="classButton"><span style="background: url('/image/icon/accept.png') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span>Adicionar</span></span>
    </div>
    <div style="clear: both"></div>
</div>
<script>

    (function() {
        var Dom  = YAHOO.util.Dom,
        Event    = YAHOO.util.Event,
        myEditor = YAHOO.widget.EditorInfo.getEditorById("editor_<?php echo $this->_tpl_vars['windowParent']; ?>
");

        //Add a listener to the parent of the images
        Event.on("button_<?php echo $this->_tpl_vars['windowId']; ?>
", 'click', function(ev) {
            var tar = Event.getTarget(ev);

            //Coloca o foco no editor
            myEditor._focusWindow();

            var url = $("#url_<?php echo $this->_tpl_vars['windowId']; ?>
").val();

            if(url != ''){

                var strElementMovie = "%BEGIN_MOVIE%" + url + "%END_MOVIE%";

                myEditor.execCommand('inserthtml', strElementMovie);

                //Fecha a janela
                $("#<?php echo $this->_tpl_vars['windowId']; ?>
").remove();

            }

        });
    })();

</script>