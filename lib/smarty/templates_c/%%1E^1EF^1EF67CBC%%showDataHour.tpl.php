<?php /* Smarty version 2.6.25, created on 2015-02-27 10:10:24
         compiled from lineBus/showDataHour.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'lineBus/showDataHour.tpl', 53, false),)), $this); ?>
<style>
    .detailHour > li{
        float: left;
        padding: 6px 5px;
        list-style:none;
        border: 1px solid #cccccc;
        margin-left: 2px;
        margin-top: 2px;
        width: 55px;
        text-align: center
    }

    .detailHour a{
        color: #cccccc;
    }
    
    .detailHour > li:hover a{
        font-weight: bold;
        color: #cc0000;
    }

    .detailHour > li:hover{
        background: #e1e1e1;
    }
    
    .deleteHour{
        opacity: 0.7;
    }
    
    .infoHour{
        opacity: 0.7;
    }

</style>
<input type="hidden" id="id_line_<?php echo $this->_tpl_vars['windowId']; ?>
"      value="<?php echo $this->_tpl_vars['idLine']; ?>
"/>
<input type="hidden" id="id_table_<?php echo $this->_tpl_vars['windowId']; ?>
"      value="<?php echo $this->_tpl_vars['idTable']; ?>
"/>
<input type="hidden" id="id_initFrom_<?php echo $this->_tpl_vars['windowId']; ?>
"  value="<?php echo $this->_tpl_vars['idInitFrom']; ?>
"/>
<input type="hidden" id="id_frequency_<?php echo $this->_tpl_vars['windowId']; ?>
" value="<?php echo $this->_tpl_vars['idFrequency']; ?>
"/>
<div id="tabContentHour_<?php echo $this->_tpl_vars['windowId']; ?>
" class="yui-navset" style="margin-top:5px 0px 5px 5px !important">
    <ul class="yui-nav">
        <li class="selected"><a href="tabHour_1_<?php echo $this->_tpl_vars['windowId']; ?>
"><em>Segunda à Sexta</em></a></li>
        <li><a href="tabHour_2_<?php echo $this->_tpl_vars['windowId']; ?>
"><em>Sábado</em></a></li>
        <li><a href="tabHour_3_<?php echo $this->_tpl_vars['windowId']; ?>
"><em>Domingo e feriado</em></a></li>
    </ul>
    <div class="yui-content" style="font-size: 12px">
        <div id="tabHour_1_<?php echo $this->_tpl_vars['windowId']; ?>
">
            <div style="margin-left: 68px; margin-top: 10px">
                <div style="float: left; margin-top: 5px; font-weight: bold">Adicionar horário: </div>
                <div style="float: left"><input type="text" alt="" maxlength="100" size="5" value="" id="insertHour_1_<?php echo $this->_tpl_vars['windowId']; ?>
" name="insertHour_1"></div>
                <div style="clear: both"></div>
            </div>
            <div style="height: 198px; overflow: auto">
            <?php if (count($this->_tpl_vars['arrElement'][1]) > 0): ?>
                    <ul id="tabHourItem_1_<?php echo $this->_tpl_vars['windowId']; ?>
" class="detailHour">
                        <?php $_from = $this->_tpl_vars['arrElement'][1]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                            <li id="<?php echo $this->_tpl_vars['item']['index']; ?>
" class="<?php echo $this->_tpl_vars['item']['index']; ?>
_1_<?php echo $this->_tpl_vars['windowId']; ?>
">
                            <?php echo $this->_tpl_vars['item']['label']; ?>

                            <div><a class="deleteHour" href="javascript:;"><img src="/image/icon/cross.png"/></a> - <a href="javascript:;" class="infoHour"><img src="/image/icon/add.png"/></a></div>
                            </li>
                        <?php endforeach; endif; unset($_from); ?>
                    </ul>
            <?php else: ?>
                <ul id="tabHourItem_1_<?php echo $this->_tpl_vars['windowId']; ?>
" class="detailHour"></ul>
                <!--
                <div class="emptyData">Nenhum horário configurado</div>
                -->
            <?php endif; ?>
            </div>
        </div>
        <div id="tabHour_2_<?php echo $this->_tpl_vars['windowId']; ?>
">
            <div style="margin-left: 68px; margin-top: 10px">
                <div style="float: left; margin-top: 5px; font-weight: bold">Adicionar horário: </div>
                <div style="float: left"><input type="text" alt="" maxlength="100" size="5" value="" id="insertHour_2_<?php echo $this->_tpl_vars['windowId']; ?>
" name="insertHour_2"></div>
                <div style="clear: both"></div>
            </div>
            <div style="height: 198px; overflow: auto">
            <?php if (count($this->_tpl_vars['arrElement'][2]) > 0): ?>
                <ul id="tabHourItem_2_<?php echo $this->_tpl_vars['windowId']; ?>
" class="detailHour">
                    <?php $_from = $this->_tpl_vars['arrElement'][2]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                        <li id="<?php echo $this->_tpl_vars['item']['index']; ?>
" class="<?php echo $this->_tpl_vars['item']['index']; ?>
_2_<?php echo $this->_tpl_vars['windowId']; ?>
">
                        <?php echo $this->_tpl_vars['item']['label']; ?>

                        <div><a class="deleteHour" href="javascript:;" class="infoHour"><img src="/image/icon/cross.png"/></a> - <a href="javascript:;"><img src="/image/icon/add.png"/></a></div>
                        </li>
                    <?php endforeach; endif; unset($_from); ?>
                </ul>
            <?php else: ?>
                <ul id="tabHourItem_2_<?php echo $this->_tpl_vars['windowId']; ?>
" class="detailHour"></ul>
                <!--
                <div class="emptyData">Nenhum horário configurado</div>
                -->
            <?php endif; ?>
            </div>
        </div>
        <div id="tabHour_3_<?php echo $this->_tpl_vars['windowId']; ?>
">
            <div style="margin-left: 68px; margin-top: 10px">
                <div style="float: left; margin-top: 5px; font-weight: bold">Adicionar horário: </div>
                <div style="float: left"><input type="text" alt="" maxlength="100" size="5" value="" id="insertHour_3_<?php echo $this->_tpl_vars['windowId']; ?>
" name="insertHour_3"></div>
                <div style="clear: both"></div>
            </div>
            <div style="height: 198px; overflow: auto">
            <?php if (count($this->_tpl_vars['arrElement'][3]) > 0): ?>
                <ul id="tabHourItem_3_<?php echo $this->_tpl_vars['windowId']; ?>
" class="detailHour">
                    <?php $_from = $this->_tpl_vars['arrElement'][3]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
                        <li id="<?php echo $this->_tpl_vars['item']['index']; ?>
" class="<?php echo $this->_tpl_vars['item']['index']; ?>
_3_<?php echo $this->_tpl_vars['windowId']; ?>
">
                        <?php echo $this->_tpl_vars['item']['label']; ?>

                        <div><a class="deleteHour" href="javascript:;"><img src="/image/icon/cross.png"/></a> - <a href="javascript:;" class="infoHour"><img src="/image/icon/add.png"/></a></div>
                        </li>
                    <?php endforeach; endif; unset($_from); ?>
                </ul>
            <?php else: ?>
                <ul id="tabHourItem_3_<?php echo $this->_tpl_vars['windowId']; ?>
" class="detailHour"></ul>
                <!--
                <div class="emptyData">Nenhum horário configurado</div>
                -->
            <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div style="clear: both"></div>
<div id="buttons_<?php echo $this->_tpl_vars['windowId']; ?>
">
    <span id="saveHour_<?php echo $this->_tpl_vars['windowId']; ?>
" class="classButton"><span style="background: url('/image/icon/accept.png') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span>Salvar</span></span>
</div>

<script>
    // define as tabs
    var tabView = new YAHOO.widget.TabView("tabContentHour_<?php echo $this->_tpl_vars['windowId']; ?>
");
</script>