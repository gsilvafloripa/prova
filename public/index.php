<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

// definir o tempo da sessao

session_start();

date_default_timezone_set('America/Sao_Paulo');

require("../config/constantes.php");

require(PATH_MODEL . "database.php");
require(PATH_MODEL . "frontController.php");
require(PATH_MODEL . "metaDialog.php");
require(PATH_MODEL . "metaResult.php");
require(PATH_MODEL . "modelWindow.php");
require(PATH_MODEL . "template.php");
require(PATH_MODEL . "sysFunctions.php");
require(PATH_MODEL . "handlePost.php");
require(PATH_MODEL . 'element/elementTab.php');
require(PATH_MODEL . 'element/elementInput.php');
require(PATH_MODEL . 'element/elementFile.php');
require(PATH_MODEL . 'element/elementCalendar.php');
require(PATH_MODEL . 'element/elementFieldset.php');
require(PATH_MODEL . 'element/elementTextarea.php');
require(PATH_MODEL . 'element/elementSelect.php');
require(PATH_MODEL . 'element/elementCheckTree.php');
require(PATH_MODEL . 'element/elementForm.php');
require(PATH_MODEL . 'element/elementGrid.php');
require(PATH_MODEL . 'element/elementButton.php');
require(PATH_MODEL . 'element/elementTreeView.php');

function _tst($data){
    print("<pre>");
    print_r($data);
    print("</pre>");
}

try{
 
    $controller = new FrontController();

    $controller->run();

}catch(Exception  $e){
    echo $e->getMessage();
}