// @autor Gustavo Silva
// @size 01/02/2011

$(document).ready(

    Grid = function(){
        
        return {

            Init : function( windowId, idGrid, url ){

                Window().maskWindow( windowId, 'in' );

                splitUrl = url.split("?");

                // adiciona o id da janela
                if(splitUrl.length > 1){
                    url = url + "&windowId=" + windowId+"&start=0&limit=10";
                }else{
                    url = url + "?windowId=" + windowId+"&start=0&limit=10";
                }

                $.getJSON( url, function( data, status, xhr ){

                    $("#" + idGrid).html( data.result.response.content );

                    Grid().OnClick( windowId, url );

                    Window().maskWindow( windowId, 'out' );

                }, idGrid);

            },

            Sync : function( windowId, id, url ){

                Window().maskWindow( windowId, 'in' );

                $.getJSON( url, function( data, status, xhr ){

                    $("#componentGrid_"+windowId).remove();

                    // renderiza o conte�do
                    $("#" + id).html( data.result.response.content );

                    Grid().OnClick(windowId, url );

                    Window().maskWindow( windowId, 'out' );

                }, id, windowId);

            },
            
            OnClick : function( windowId, url ){

                $("#contentGridPaginationLink_"+windowId+" li").click(function(){

                    id = '';
                    idLi = this.id;
                    idLi = idLi.split("_");

                    // verifica se sao botoes rapidos
                    if(( idLi.length == 1 ) && ( idLi[0] != '_')){

                        $("#contentGridPaginationLink_"+windowId+" li").css({'background-color' : '', 'color' : ''});

                        $(this).css({'background-color' : '#FCEC94', 'color' : '#333333'});

                        id = this.id;
                    }else{

                        if( this.id != "_" ){

                            $("#contentGridPaginationLink_"+windowId+" li").css({'background-color' : ''});

                            $("#" + idLi[1]).css({'background-color' : '#FCEC94', 'color' : '#333333'});

                            id = idLi[1];
                        }
                    }

                    if( id != '' ){

                        url = url.split( "?" );

                        //alert($("#pagLimit").val());
                        limit = $('#pagLimit_'+windowId).find('option').filter(':selected').text();

                        urlParams = url[1].split("&start");

                        Grid().Sync( windowId, 'grid_'+windowId, url[0] + "?" + urlParams[0] + "&start=" + id + "&limit=" + limit);

                        //Grid().Sync( windowId, 'grid_'+windowId, url[0] + "?windowId="+windowId+"&start=" + id + "&limit=" + limit);
                    }

                }, url, windowId);

                $("#pagLimit_"+windowId).change(function(){

                   start = $("#pagStart_"+windowId).val();

                   limit = $('#pagLimit_'+windowId).find('option').filter(':selected').text();

                   url = url.split( "?" );

                   urlParams = url[1].split("&start");

                   Grid().Sync( windowId, 'grid_'+windowId, url[0] + "?" + urlParams[0] + "&start=" + start + "&limit=" + limit);
                   //Grid().Sync( windowId, 'grid_'+windowId, url[0] + "?windowId="+windowId+"&start=" + start + "&limit=" + limit);

                }, url, windowId)

            }

        }
    });