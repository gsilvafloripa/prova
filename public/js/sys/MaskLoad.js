// @autor Gustavo Silva
// @since  01/03/2011
$(document).ready(

    MaskLoad = function(){
        
        return {

            Init : function( id, status ){

                // verifica se possui a mascara
                checkDiv = $( "#maskLoading_" + id )[0];

                if(( status == 'in' ) && ( checkDiv == undefined )){

                    $("#mask").html('<div class="maskLoading" id="maskLoading_' + id + '"><span id="contentMaskLoading_' + id + '" style="position:absolute; margin-left:-40px; margin-top:-40px; padding:5px;"><img src="/image/loadingBar.gif" style="margin:5% 0px 0px 0px"></br><b>Carregando...</b><span></div>')
    
                    var obj = $("#" + id);

                    var contentMask = $( "#contentMaskLoading_" + id );

                    contentMask.css( "top" , (obj.height()/2) + "px");
                    contentMask.css( "left", (obj.width()/2) + "px");
    
                    var mask = $( "#maskLoading_" + id);

                    mask.width( obj.width() );
                    mask.height( obj.height() );
                    mask.css( "top", obj.position().top + "px");
                    mask.css( "left", obj.position().left + "px");
                    mask.css( "padding-top"   , obj.css("padding-top"));
                    mask.css( "padding-left"  , obj.css("padding-left"));
                    mask.css( "padding-right" , obj.css("padding-right"));
                    mask.css( "padding-botton", obj.css("padding-botton"));

                    mask.fadeIn(200);
                }else{
                    // remove a mascara
                    if( checkDiv != 0 ){
                        var mask = $( "#maskLoading_" + id);
                        mask.fadeOut(200);
                        mask.remove();
                   }
                }
            }
        }
    });