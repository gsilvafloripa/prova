// @autor Gustavo Silva
// @since  01/03/2011

$(document).ready(

    Login = function(){
        
        return {

            ProcessData : function(divMask, contentForm){

                //Inicia a máscara para o conteúdo
                MaskLoad().Init( divMask, 'in' );

                // define o elemento
                element = $('#' + contentForm);

                url    = element.attr('action');
                params = element.serialize();

                $.ajax({
                        type	: 'post',
                        data	: params,
                        dataType: 'json',
                        url	: url,
                        success	: function(response){

                                response = response.result.response;

                                try{ dialog = response.dialog.code; }catch(e){  dialog = null; }

                                // verifica se houve algum erro
                                if(dialog != null){

                                        idDialog = "#dialog-modal";

                                        title = (dialog == 1) ? "Atenção" : "Erro";

                                        title = (response.dialog.title != '') ? title + " - " + response.dialog.title : title;

                                        $( idDialog ).html( response.dialog.message );

                                        // abre uma informação de erro
                                        $( idDialog ).dialog({
                                                modal           : true,
                                                draggable	: false,
                                                resizable	: false,
                                                title           : "<div style='float:left; background:url(/image/icon/application_error.png) no-repeat scroll -1px 1px transparent; padding: 0px 0px 0px 17px'></di><div>"+title+"</div>",
                                                buttons         : {Ok: function(){ $(this).dialog( "close" );} }
                                        });

                                        MaskLoad().Init( divMask, 'out' );
                                }else{
                                        window.location = '/';
                                }
                        }
                });
            },

            linkRecoverPassword : function(){
                if($("#contentPassword").css("display") == "none"){
                    $("#contentPassword").css("display", "block");
                }else{
                    $("#contentPassword").css("display", "none");
                }
            }

        }
    });
    
$(function(){
	$("#buttonEnter" ).click(function(){ Login().ProcessData('content', 'contentForm') });
        $("#buttonRecoverPassword" ).click(function(){ Login().ProcessData('contentPassword', 'contentPasswordForm') });
        $("#linkRecoverPassword" ).click(function(){ Login().linkRecoverPassword() });
});
