
GLOBAL_INSTANCE_GRID = new Object();

YAHOO.util.Event.onDOMReady(function () {

    NewGrid = function(){

        return{

            Create : function(windowId, obj){

                if($('#content_bar_' + windowId)[0] == undefined){

                    var content_bar = "<div id='content_bar_" + windowId + "' style='height:44px; bottom:0px; padding: 4px 4px 0px 0px; background:#FFFFFF; border-top: 1px solid #eeeeee'></div>";
                    $('#' + windowId).after(content_bar);

                }

                //YAHOO.util.Event.onContentReady("content_bar_" + windowId, function() {
                $("#content_bar_" + windowId).ready(function() {

                   YAHOO.util.Dom.get("content_bar_" + windowId).innerHTML = "<div id='content_bar_pagination_"+windowId+"' style='width: 430px; margin-left: auto; margin-right: auto;'> <div id='content_bar_pagination_render_"+windowId+"' style='position: relative; text-align: center; margin-left: auto; margin-right: auto;'></div><div id='label_pag_register_" + windowId + "' style='text-align: center; font-size:11px; color:#999999'></div></div>";

                    // campos do grid
                    var columnDefs = obj.fields;

                    var urlExec = obj.url;

                    if(urlExec.indexOf("?") <= 0){
                        urlExec = urlExec + "?";
                    }else{
                        urlExec = urlExec + "&";
                    }

                    // DataSource instance
                    var objDataSource = new YAHOO.util.DataSource( urlExec );
                    objDataSource.responseType = YAHOO.util.DataSource.TYPE_JSON;

                    objDataSource.responseSchema = {
                        resultsList: "records",
                        fields: columnDefs,
                        // Access to values in the server response
                        metaFields: {
                            totalRecords: "totalRecords",
                            startIndex: "startIndex"
                        }
                    };

                    // Customize request sent to server to be able to set total # of records
                    var generateRequest = function(oState, oSelf) {
                    	
                    	YAHOO.util.Event.onContentReady("dataTable_" + windowId, function() {

                            // adicionan loading
                            Functions().mask("dataTable_" + windowId);

                        });
                    	
                        // Get states or use defaults
                        oState          = oState || { pagination: null, sortedBy: null };
                        var sort        = (oState.sortedBy) ? oState.sortedBy.key : "id";
                        var dir         = (oState.sortedBy && oState.sortedBy.dir === YAHOO.widget.DataTable.CLASS_DESC) ? "desc" : "asc";
                        var startIndex  = (oState.pagination) ? oState.pagination.recordOffset : 0;
                        var results     = (oState.pagination) ? oState.pagination.rowsPerPage  : 10;

                        // Build custom request
                        return  "sort=" + sort + "&dir=" + dir + "&startIndex=" + startIndex + "&results=" + (startIndex + results) + "&formatJson=true";
                    };

                    // Define o tamanho do grid
                    var eleWindow  = $('#' + windowId);
                    var widthGrid  = (eleWindow.innerWidth()) + 'px';
                    var heightGrid = (eleWindow.innerHeight() - 53) + 'px';

                    // Configuração
                    var myConfigs = {
                        generateRequest : generateRequest,
                        //MSG_LOADING     : 'Aguarde, carregando...',
                        MSG_EMPTY       : 'Nenhum registro encontrado',
                        initialRequest  : generateRequest(),
                        width           : widthGrid,
                        height          : heightGrid,
                        dynamicData     : true,
                        selectionMode   : "single",
                        sortedBy        : {key:"id", dir:YAHOO.widget.DataTable.CLASS_ASC},
                        paginator       : new YAHOO.widget.Paginator({ rowsPerPage           : 10,
                                                                       containers            : "content_bar_pagination_render_" + windowId,
                                                                       previousPageLinkLabel : "<",
                                                                       firstPageLinkLabel    : "<<",
                                                                       lastPageLinkLabel     : ">>",
                                                                       nextPageLinkLabel     : ">" })
                    };

                    // DataTable instance
                    //var objDataTable = new YAHOO.widget.DataTable("dataTable_" + windowId, columnDefs, objDataSource, myConfigs);
                    var objDataTable = new YAHOO.widget.ScrollingDataTable("dataTable_" + windowId, columnDefs, objDataSource, myConfigs);

                    // Subscribe to events for row selection 
                    objDataTable.subscribe("rowMouseoverEvent" , objDataTable.onEventHighlightRow); 
                    objDataTable.subscribe("rowMouseoutEvent"  , objDataTable.onEventUnhighlightRow); 
                    objDataTable.subscribe("rowClickEvent"     , objDataTable.onEventSelectRow);

                    // Update totalRecords on the fly with values from server
                    objDataTable.doBeforeLoadData = function(oRequest, oResponse, oPayload) {

                    	Functions().unmask("dataTable_" + windowId);
                    	oPayload.totalRecords            = oResponse.meta.totalRecords;
                        oPayload.pagination.recordOffset = oResponse.meta.startIndex;

                        YAHOO.util.Dom.get("label_pag_register_"+windowId).innerHTML = 'Total de registros: ' + oResponse.meta.totalRecords;

                        return oPayload;
                    };

                    YAHOO.util.Event.addListener("update", "click", function() {
                        objDataSource.sendRequest(generateRequest(), {
                            success : objDataTable.onDataReturnSetRows,
                            failure : objDataTable.onDataReturnSetRows,
                            scope   : objDataTable,
                            argument: objDataTable.getState()
                        });
                    });

                    try{

                        // botão para atualizar o grid
                        var oElementRefreshGrid = document.getElementById( "refreshGrid_" + windowId );

                        YAHOO.util.Event.addListener(oElementRefreshGrid, "click", function() {
                            // mostra marcara de loading
                            objDataTable.showTableMessage(myConfigs.MSG_LOADING);
    
                            objDataSource.sendRequest(generateRequest(), {
                                success : objDataTable.onDataReturnSetRows,
                                failure : objDataTable.onDataReturnSetRows,
                                scope   : objDataTable,
                                argument: objDataTable.getState()
                            });
                        });

                    }catch(e){}

                    var onContextMenuClick = function(p_sType, p_aArgs, p_obj) {

                        // Extract which TR element triggered the context menu
                        var elRow = this.parent.contextEventTarget;

                        elRow = p_obj.objDataTable.getTrEl(elRow);

                        // pega o id da linha selecionada
                        var rowId = p_obj.objDataTable.getRecord(elRow);
                        // ************************** JÁ ESTÁ VINDO COM A URL PREENCHIDA *************************************
                        // chama metodo para executar uma acao
                        NewGrid().ExecAction( windowId, rowId.getData().id, p_obj.action );

                        // redefine a url original
                        //p_obj.action.action.url = actionUrl;

                    };
                    
                    GLOBAL_INSTANCE_GRID[windowId] = {dataTable : objDataTable, dataSource : objDataSource, fnRequest : generateRequest};

                    try{
                        var objContextMenu = obj.contextMenu
    
                        if(objContextMenu.length > 0){
    
                            // adiciona os itens do menu de contexto
                            var contextMenu = new YAHOO.widget.ContextMenu("contextmenu_dataTable_" + windowId, {trigger:objDataTable.getTbodyEl()});
    
                            for(var i in objContextMenu){
                                // adiciona menu de contexto
                                contextMenu.addItem({text: objContextMenu[i].label, onclick: { fn: onContextMenuClick, obj: {objDataTable:objDataTable, action: objContextMenu[i]}}});
                            }
    
                            // Renderiza o menu de contexto
                            contextMenu.render("dataTable_" + windowId);

                            var onDoubleClick = function(oArgs) {
                                // consulta o rowId
                                rowId = objDataTable.getRecord(oArgs.target.id).getData().id;

                                // chama metodo para executar uma acao
                                NewGrid().ExecAction( windowId, rowId, objContextMenu[0] );
                            }
    
                            // adiciona o evento de double click
                            objDataTable.subscribe("rowDblclickEvent", onDoubleClick);
    
                        }
                    }catch(e){}

                    return {
                        ds: objDataSource,
                        dt: objDataTable
                    };
            });

            },

            ExecAction : function(windowId, rowId, objAction){

                // define a url original
                actionUrl = objAction.action.url;

                splitUrl = actionUrl.split("?");

                // adiciona o id da janela
                if(splitUrl.length > 1){
                    url = actionUrl + "&rowId=" + rowId + "&idParent=" + windowId;
                }else{
                    url = actionUrl + "?rowId=" + rowId + "&idParent=" + windowId;
                }

                var newObjAction = objAction;

                // redefine a url do json
                newObjAction.action.url = url;
                
                newObjAction.action.idParent = windowId;

                // cria a nova janela
                strJson = JSON.stringify(newObjAction.action);

                newObjAction.action.url = actionUrl;

                //Window().create(p_obj.action);
                eval(objAction.method + "(" + strJson + ")");
            },
            
            Refresh : function(elRender){
            	
            	var objDataTable    = GLOBAL_INSTANCE_GRID[elRender].dataTable;
            	var objDataSource   = GLOBAL_INSTANCE_GRID[elRender].dataSource;
            	var generateRequest = GLOBAL_INSTANCE_GRID[elRender].fnRequest;

                objDataSource.sendRequest(generateRequest(), {
                    success : objDataTable.onDataReturnSetRows,
                    failure : objDataTable.onDataReturnSetRows,
                    scope   : objDataTable,
                    argument: objDataTable.getState()
                });
            	
            },
            
            Delete : function(json){
            	
            	var elRender = json.idParent;
            	
            	$("#dialog-confirm").html("Deseja realmente excluir?");

                $( "#dialog-confirm" ).dialog({
                        modal		: true,
                        draggable	: false,
                        resizable	: false,
                        zIndex      : 1000000000,
                        title		: 'Atenção',
                        buttons		: {"Não": function(){ $(this).dialog( "close" );},
                                       "Sim":function(){
                                    	   
                                    	    $(this).html('Aguarde, processsando... <img src="/image/loading.gif">');
                                    	    
                                    	    var objElementDialog = this;
                                    	   
                                            $.ajax({
                                                type		: 'post',
                                                dataType    : 'json',
                                                url			: json.url,
                                                success		: function(response){
                                                	
                                                	$('#dialog-confirm').next().remove();
                                                	
                                                	Process().HandlePost(response, elRender);
                                                	
                                                	$(objElementDialog).dialog("close");
                                                        
                                                }
                                            });
                                            
                                       }
                                      }
                });
            	
            	
            	
            	
            }

        }
    }

});