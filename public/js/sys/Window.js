// @autor Gustavo Silva
// @since  01/03/2011

$(document).ready(

    Window = function(){

        return {
        	
        	getId : function(){

				var windowId;

                //Controle do windowId
                windowId = parseInt( Functions().getCookieWindow( "windowId" ) );

                if( isNaN(windowId) ){
                    Functions().setCookieWindow( "windowId", 1 );
                    windowId = 1;
                }else{
                    windowId = windowId + 1;
                    Functions().setCookieWindow( "windowId", windowId );
                }

                // define o id da janela
                windowId = "win_" + windowId;

				return windowId;

			},

            create :  function( obj ){

                windowId = this.getId();

                var contentWindow = "<div id='" + windowId + "'>" + "<div class='contentWindow' id='content_" + windowId + "' style='display:none'></div></div>";

                // adiciona a div para a crição da janela
                $('#sysContent').append(contentWindow);

                $(function(){
                    $('#' + windowId).dialogr({
                        width		:obj.width,
                        height		:obj.height,
                        modal       :obj.modal,
						draggable   :obj.modal == 1 ? false : true,
                        resizable	:false,
                        maximized	:false,
						minimized   :true,
                        title		: "<div style='float:left; background:url(/image/icon/"+obj.icon+".png) no-repeat scroll -1px -1px transparent; padding: 0px 0px 0px 17px'></di><div>"+obj.title+"</div>",
                        open		: function(){
                            Window().maskWindow( windowId, 'in' );
                        }
                    });
                });

                this.load(obj.url, windowId)
            },

            load :  function( url, windowId ){

                splitUrl = url.split("?");

                // adiciona o id da janela
                if(splitUrl.length > 1){
                    url = url + "&windowId=" + windowId;
                }else{
                    url = url + "?windowId=" + windowId;
                }

                YAHOO.util.Connect.asyncRequest ( "GET" , url, {

                  success   : function(obj){
                	
                	  	var objReturn = eval("(" + obj.responseText + ")");
                	  
                      	// ==== (MIGRAÇÃO) ====
	                    if(objReturn.success){
	                    	
	                    	var response = objReturn;
	                		
	                		Window().ProcessElements(response);
	                		
	                		try{ $("#content_bar_" + windowId).remove(); }catch(e){}

                            zIndex = $( "#dialog-modal" ).css("z-index");

                            // adiciona o conteúdo na janela
                            $( "#content_" + windowId ).html( response.content );

                            $( "#content_" + windowId ).css("z-index", zIndex);

                            // atualiza o dialog-modal
                            $( "#dialog-modal" ).css("z-index", zIndex+1);
	                		
	                		try{
	
	                            var divButtonsContent = $("#buttons_" + windowId);
	
	                            if(divButtonsContent[0] != undefined){
	
									var content_bar = "<div id='content_bar_" + windowId + "' style='height:40px; bottom:0px; padding: 4px 4px 0px 0px; background:#FFFFFF; border-top: 1px solid #eeeeee'></div>";
									
									$('#' + windowId).after(content_bar);
	
									// clona o elemento dos botões para o content_bar
									divButtonsContent.clone().prependTo("#content_bar_" + windowId);
									// remove o elemento
									//divButtonsContent.remove();
									$("#content_" + windowId + " #buttons_" + windowId).remove();
	
								}
								
								//try{
									
									actionButton = $("#previous_" + windowId).attr("actionButton");
									
									$("#previous_" + windowId).click(function(){
										
										eval(actionButton);
										
									});
									
								//}catch(e){console.log(e);}
	                            
	                            // monta ações dos botões
                                try{
                                    $("#submit_" + windowId ).click( function(){ Process().ProcessData( windowId ); });
								}catch(e){}
				
								// clique para remover janela
	                            try{ $("#cancel_" + windowId ).click( function(){ $('#' + windowId).remove();} ); }catch(e){}
	
	                        }catch(e){}
	                        
	                        Window().maskWindow( windowId, 'out' );
	                    	
                            $('#content_' + windowId ).css("display", "block");
	
	                        // executa o parse
	                        $(function(){
	                            Functions().parseScript( document.getElementById( 'content_' + windowId ) );
	                        });
	                		
	                	}else{
	                		
	                		result        = objReturn.result.response;
	                      	var response  = result;
	                		
	                		if( obj.status != "error" ){
	
	                            if(response.connection == false){
	                                // redireciona usuario sem sessão
	                                window.location = "/";
	                            }
	
	                            try{dialog = response.dialog.code;}catch(e){dialog = null;}
	
	                            // verifica se possui algum dialog
	                            if( dialog == null ){
	
	                                try{
	                                    $("#content_bar_" + windowId).remove();
	                                }catch(e){}
	
	                                zIndex = $( "#dialog-modal" ).css("z-index");
	
	                                // adiciona o conteúdo na janela
	                                $( "#content_" + windowId ).html( result.content );
	
	                                $( "#content_" + windowId ).css("z-index", zIndex);
	
	                                // atualiza o dialog-modal
	                                $( "#dialog-modal" ).css("z-index", zIndex+1);
	
	                                try{
	
	                                    var divButtonsContent = $("#buttons_" + windowId);
	
	                                    if(divButtonsContent[0] != undefined){
	
	    									var content_bar = "<div id='content_bar_" + windowId + "' style='height:40px; bottom:0px; padding: 4px 4px 0px 0px; background:#FFFFFF; border-top: 1px solid #eeeeee'></div>";
	    									$('#' + windowId).after(content_bar);
	
	    									// clona o elemento dos botões para o content_bar
	    									divButtonsContent.clone().prependTo("#content_bar_" + windowId);
	    									// remove o elemento
	    									//divButtonsContent.remove();
	    									$("#content_" + windowId + " #buttons_" + windowId).remove();
	
	    								}
										
										try{
									
											actionButton = $("#previous_" + windowId).attr("actionButton");
											
											$("#previous_" + windowId).click(function(){
												
												eval(actionButton);
												
											});
											
										}catch(e){}
	
	                                    // monta ações dos botões
	                                    try{
	                                        $("#submit_" + windowId ).click( function(){
	
	    									var messageConfirm = '';
	    				
	    									try{
	    										messageConfirm = $("#submit_" + windowId).attr("messageConfirm");
	    									}catch(e){}
	    				
	    									if( messageConfirm != undefined ){
	    				
	    										$("#dialog-confirm").html( messageConfirm );
	    					
	    										$( "#dialog-confirm" ).dialog({
	    										modal		: true,
	    										draggable	: false,
	    										resizable	: false,
	    										zIndex    	: 1000000000,
	    										title		: 'Informação',
	    										buttons		: {"Não": function(){ $(this).remove();},
	    												   "Sim": function(){ Process().ProcessData( windowId ); $(this).remove();}
	    												  }
	    										});
	    									}else{
	    										Process().ProcessData( windowId );
	    									}
	    				
	    									});
	    								}catch(e){}
	    				
	    								try{
	                                        $("#cancel_" + windowId ).click( function(){ $('#' + windowId).remove();} );
	                                    }catch(e){}
	
	                                }catch(e){}
	
	                                // executa o parse
	                                $(function(){
	                                    Functions().parseScript( document.getElementById( 'content_' + windowId ) );
	                                });
	                            }
	
	                            Window().maskWindow( windowId, 'out' );
	
	                            $('#content_' + windowId ).css("display", "block");
	
	                        }else{
	                            idDialog = "#dialog-modal";
	
	                            // insere a mensagem de erro
	                            $( idDialog ).html( "Não foi possível estabelecer uma conexão.<br>Sistema off-line" );
	
	                            // abre uma informação de erro
	                            $( idDialog ).dialog({
	                                    modal       : true,
	                                    draggable	: false,
	                                    resizable	: false,
	                                    title       : "<div style='float:left; background:url(/image/icon/cross.png) no-repeat scroll -1px 1px transparent; padding: 0px 0px 0px 17px'></di><div>Conexão</div>",
	                                    zIndex      : 3000000,
	                                    buttons     : {Ok: function(){$(this).remove();}}
	                            });
	                        }
	                		
	                	}

                  },
                  failure   : function(){
                      alert('erro');
                  }
                  //argument  : ['foo','bar']
                });

            },

            /**
             * cria mascara para as janelas
             */
            maskWindow : function( id, status ){

                // verifica se possui a mascara
                checkDiv = $( "#maskLoading_" + id )[0];

                if(( status == 'in' ) && ( checkDiv == undefined )){

                    /*$("#" + id).append('<div class="maskLoading" id="maskLoading_' + id + '"><span id="contentMaskLoading_' + id + '" style="position:absolute; margin-left:-40px; margin-top:-40px; padding:5px;"><img src="/image/loadingBar.gif" style="margin:5% 0px 0px 0px"></br><b>Carregando...</b><span></div>')
    
                    var obj = $("#" + id);
                    var objContent = $("#content_" + id);

                    var contentMask = $( "#contentMaskLoading_" + id );

                    contentMask.css( "top"           , (obj.height()/2) + "px");
                    contentMask.css( "left"          , (obj.width()/2) + "px");
    
                    var mask = $( "#maskLoading_" + id);

                    // calculo para retirar barra de rolagem
                    var scroll = (objContent.height() > obj.height()) ? 16 : 0;
                    mask.width( ((obj.width())-scroll) + "px");
                    var objHeight = (objContent.height() > obj.height()) ? objContent.height() : obj.height();
                    mask.height( (objHeight) + "px" );
                    mask.css( "top", "0.1px");
                    mask.css( "left", obj.position().left + "px");

                    mask.fadeIn(200);*/

					Functions().mask(id);

                }else{

                    // remove a mascara
                    /*if( checkDiv != 0 ){
                        var mask = $( "#maskLoading_" + id);
                        mask.fadeOut(200);
                        mask.remove();
                    }*/

					Functions().unmask(id);

                }
            },
            
            ProcessElements : function(response){

                if(response.display == 'modal'){

                    this.createModal(response.message, response.messageTitle, response.messageType);

                }else if(response.display == 'notify'){

                    var status  = response.messageTimer == false     ? true     : false;
                    var timer   = response.messageTimer == false     ? 0        : response.messageTimer;
                    var typeCls = response.messageType  == undefined ? 'notice' : response.messageType;
                    
                    jQuery.noticeAdd({
                        text        : response.message,
                        stay        : status,
                        stayTime    : timer,
                        type        : typeCls
                    });

                }
                
            },
            
            /**
             * Cria janela modal
             * 
             * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
             * 
             */
            createModal : function(message, title, type, objButtons){

				var windowId = this.getId();

				// verifica se possui o objButton definido
				if(objButtons == undefined){ objButtons = {OK: function(){ $(this).remove(); }}; }

				windowId = "dialog-modal-" + windowId;

				// adiciona a div para a crição da janela
                $('#sysContent').append("<div id='" + windowId + "'></div>");

				// insere a mensagem de erro
				$( "#" + windowId ).html( '<div class="contentModal" style="padding:10px">' + message + '</div>' );

				// cria uma modal
				$( "#" + windowId ).dialog({
					modal       : true,
					draggable	: false,
					resizable	: false,
					title       : '',
					zIndex      : 3000000,
					buttons     : objButtons
				});

				// remove o título
				$("#" + windowId).ready(function(){

					//$( "#" + windowId ).prev('div').css('display', 'none');

					//var typeCls = response.messageType == undefined ? 'notice' : response.messageType;
					
					/*success = 47a508
					error   = CC0000
					warning = f19e0e
					info    = 068bc5*/

					$( "#" + windowId ).css('cssText', 'background: #F7F6F6 !important');
					$( "#" + windowId ).next('div').css('cssText', 'background: #F7F6F6 !important');
					$( "#" + windowId ).css('color', '#666666');
					

				});

			}
        }
    });