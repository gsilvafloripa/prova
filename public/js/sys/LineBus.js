$(document).ready(

    LineBus = function(windowId){

        windowId = windowId;

        return {

            Init : function(){},

            showConfiguration : function(type){

                var objLineBus = this;

                $('#content_bar_' + windowId).load(function(){

                    $('#content_bar_' + windowId).html('<span id="save_' + windowId + '" style="float: none" class="classButton"><span style="background: url(\'/image/icon/accept.png\') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span>Confirmar e salvar</span></span>');

                });

                $('#comboboxLine_' + windowId).change(function(){
					// value selecionado
					var id = $("option:selected", this).val();

					if(id != ''){ Functions().renderCombobox('comboboxLine_' + windowId, 'comboboxRoute_' + windowId, '', '/lineBus/getDataComboboxRoute/?id=' + id, 0, {value:0, description:'--- Selecione ---'}); }
				});

                $('#search_' + windowId).click(function(){

                    var varLine      = $('#comboboxLine_'       + windowId).val();
					var varIdTable   = $('#comboboxTable_'      + windowId).val();
                    var varIinitFrom = $('#comboboxRoute_'      + windowId).val();
                    var varFrequency = $('#comboboxFrequency_'  + windowId).val();

                    var params = {line : varLine, idTable: varIdTable, initFrom : varIinitFrom, frequency : varFrequency};

                    if(type == 'hour'){

                        objLineBus.showHourConfiguration(params);

                    }else if(type == 'course'){

                        objLineBus.showCourseConfiguration(params);

                    }else if(type == 'itinerario'){

                        //
						objLineBus.showItinerarioConfiguration(params);

                    }
                    
                    

                });

            },

            showCourseConfiguration : function(params){

                var objFunctions = Functions();

				objFunctions.mask('contentCourse_' + windowId);

				$.ajax({
                        type	: 'get',
                        dataType: 'json',
                        url		: '/lineBus/showDataCourse?windowId=' + windowId,
                        data    : params,
                        success	: function(response){

							if(response.success == true){

								$('#contentCourse_' + windowId).html(response.content);

								var divButtonsContent = $('#contentCourse_' + windowId + ' #buttons_' + windowId);

								if($('#content_bar_' + windowId + ' #buttons_' + windowId)[0] == undefined){

									var divHeight = $('#' + windowId).parent().height() + 46
									$('#' + windowId).parent().css('height', divHeight + 'px');

									var content_bar = "<div id='content_bar_" + windowId + "' style='height:40px; bottom:0px; padding: 4px 4px 0px 0px; background:#FFFFFF; border-top: 1px solid #eeeeee'></div>";
									$('#' + windowId).after(content_bar);

									$("#content_bar_" + windowId).ready(function() {

										// clona o elemento dos botões para o content_bar
										divButtonsContent.clone().prependTo("#content_bar_" + windowId);
										// remove o elemento
										divButtonsContent.remove();
										
										$('#saveHour_' + windowId).click(function(){

											var arrCourse = Array();
											$('#contentCourseItem_' + windowId + ' > li').each(function(i){
												arrCourse.push($(this).attr('id'));
											});

											arrResult = {arrCourse : arrCourse, line: $('#id_line_'+windowId).val(), initFrom: $('#id_initFrom_'+windowId).val(), idTable : $('#id_table_'+windowId).val()};

											objFunctions.mask('contentCourse_' + windowId);

											$.ajax({
													type	: 'post',
													dataType: 'json',
													url		: '/lineBus/saveDataCourse',
													data    : arrResult,
													success	: function(response){

														if(response.success == true){

															

														}else{

															Functions().messageModal('Atenção', response.content);

														}

														objFunctions.unmask('contentCourse_' + windowId);
							
													}
											});

										});

									});

								}else{

									// remove o elemento
									divButtonsContent.remove();

								}

								// cria o drag drop
								$('#contentCourseItem_' + windowId).sortable({
									placeholder	: "ui-state-highlight",
									items		: "li:not(.ui-state-disabled)"
								});

								$('#idCourse_' + windowId).change(function(){
	
									var id = $("option:selected", this).val();

									if(id > 0){

										var status = true;

										$('#contentCourseItem_' + windowId + ' > li').each(function(){

											value = $(this).attr('id');

											if(value == id) status = false;

										});

										if(status == true){

											var text = '';

											$("option:selected", this).each(function () {
												text = $(this).text();
											});

											$('#contentCourseItem_' + windowId + ' > li:last' ).before('<li id="' + id + '"><a href="javascript:;"><img src="/image/icon/delete.png"></a> ' + text + '</li>');

										}else{

											Functions().messageModal('Atenção', 'A via seleciona já existe');

										}

									}

									$('#contentCourseItem_' + windowId + ' > li > a' ).click(function(){ $(this).parent('li').fadeOut(700, function() { $(this).remove(); }); });
	
								});

								$('#contentCourseItem_' + windowId + ' > li > a' ).click(function(){ $(this).parent('li').fadeOut(700, function() { $(this).remove(); }); });

							}else{

								Functions().messageModal('Atenção', response.content);

							}

							objFunctions.unmask('contentCourse_' + windowId);

                        }
                });

            },

			showItinerarioConfiguration : function(params){

                var objFunctions = Functions();

				objFunctions.mask('contentItinerario_' + windowId);

				$.ajax({
                        type	: 'get',
                        dataType: 'json',
                        url		: '/lineBus/showDataItinerario?windowId=' + windowId,
                        data    : params,
                        success	: function(response){

							if(response.success == true){

								$('#contentItinerario_' + windowId).html(response.content);

								//var divButtonsContent = $("#buttons_" + windowId);
								var divButtonsContent = $('#contentItinerario_' + windowId + ' #buttons_' + windowId);

								if($('#content_bar_' + windowId + ' #buttons_' + windowId)[0] == undefined){

									var divHeight = $('#' + windowId).parent().height() + 46
									$('#' + windowId).parent().css('height', divHeight + 'px');

									var content_bar = "<div id='content_bar_" + windowId + "' style='height:40px; bottom:0px; padding: 4px 4px 0px 0px; background:#FFFFFF; border-top: 1px solid #eeeeee'></div>";
									$('#' + windowId).after(content_bar);

									$("#content_bar_" + windowId).ready(function() {

										// clona o elemento dos botões para o content_bar
										divButtonsContent.clone().prependTo("#content_bar_" + windowId);
										// remove o elemento
										divButtonsContent.remove();
										
										$('#saveHour_' + windowId).click(function(){

											var arrItinerario = Array();
											$('#contentItinerarioItem_' + windowId + ' > li').each(function(i){
												arrItinerario.push($(this).attr('id'));
											});

											arrResult = {arrItinerario : arrItinerario, line: $('#id_line_'+windowId).val(), initFrom: $('#id_initFrom_'+windowId).val(), idTable: $('#id_table_'+windowId).val()};

											objFunctions.mask('contentItinerario_' + windowId);

											$.ajax({
													type	: 'post',
													dataType: 'json',
													url		: '/lineBus/saveDataItinerario',
													data    : arrResult,
													success	: function(response){

														if(response.success == true){

															

														}else{

															Functions().messageModal('Atenção', response.content);

														}

														objFunctions.unmask('contentItinerario_' + windowId);
							
													}
											});

										});

									});

								}else{

									// remove o elemento
									divButtonsContent.remove();

								}

								// cria o drag drop
								$('#contentItinerarioItem_' + windowId).sortable({
									placeholder	: "ui-state-highlight",
									items		: "li:not(.ui-state-disabled)"
								});


								$('#idItinerario_' + windowId).change(function(){
	
									var id = $("option:selected", this).val();

									if(id > 0){

										var status = true;

										$('#contentItinerarioItem_' + windowId + ' > li').each(function(){

											value = $(this).attr('id');

											if(value == id) status = false;

										});

										if(status == true){

											var text = '';

											$("option:selected", this).each(function () {
												text = $(this).text();
											});

											$('#contentItinerarioItem_' + windowId).prepend('<li id="' + id + '"><a href="javascript:;"><img src="/image/icon/delete.png"></a> ' + text + '</li>');

										}else{

											Functions().messageModal('Atenção', 'A rua seleciona já existe');

										}

									}

									$('#contentItinerarioItem_' + windowId + ' > li > a' ).click(function(){ $(this).parent('li').fadeOut(700, function() { $(this).remove(); }); });
	
								});

								$('#contentItinerarioItem_' + windowId + ' > li > a' ).click(function(){ $(this).parent('li').fadeOut(700, function() { $(this).remove(); }); });

							}else{

								Functions().messageModal('Atenção', response.content);

							}

							objFunctions.unmask('contentItinerario_' + windowId);

                        }
                });

            },

            showHourConfiguration: function(params){

                var objFunctions = Functions();
				
				objLineBus = this;

				objFunctions.mask('contentHour_' + windowId);

                $.ajax({
                        type	: 'get',
                        dataType: 'json',
                        url		: '/lineBus/showDataHour?windowId=' + windowId,
                        data    : params,
                        success	: function(response){

							if(response.success == true){

								$('#contentHour_' + windowId).html(response.content);

								//var divButtonsContent = $("#buttons_" + windowId);
								var divButtonsContent = $('#contentHour_' + windowId + ' #buttons_' + windowId);

								if($('#content_bar_' + windowId + ' #buttons_' + windowId)[0] == undefined){

									var divHeight = $('#' + windowId).parent().height() + 46
									$('#' + windowId).parent().css('height', divHeight + 'px');

									var content_bar = "<div id='content_bar_" + windowId + "' style='height:40px; bottom:0px; padding: 4px 4px 0px 0px; background:#FFFFFF; border-top: 1px solid #eeeeee'></div>";
									$('#' + windowId).after(content_bar);

									$("#content_bar_" + windowId).ready(function() {

										// clona o elemento dos botões para o content_bar
										divButtonsContent.clone().prependTo("#content_bar_" + windowId);
										// remove o elemento
										divButtonsContent.remove();

										$('#saveHour_' + windowId).click(function(){ objLineBus.saveHour(); });

									});

								}else{

									// remove o elemento
									divButtonsContent.remove();

								}

								objLineBus.setFieldHour(1);
								objLineBus.setFieldHour(2);
								objLineBus.setFieldHour(3);

								$('.detailHour > li > div > a.deleteHour' ).click(function(){ $(this).parent().parent('li').fadeOut(700, function() { $(this).remove(); }); });

								$('.detailHour > li > div > a.infoHour' ).click(function(){

									//

									var idLine      = $('#id_line_'		 + windowId).val();
									var idInitFrom  = $('#id_initFrom_'  + windowId).val();
									var idTable     = $('#id_table_'	 + windowId).val();
									var idFrequency = $('#id_frequency_' + windowId).val();

									var strSplit = $(this).parent().parent('li')[0].className;
									strSplit     = strSplit.split('_');

									var idDayWeek   = strSplit[1];
									var hour        = strSplit[0];

									var params = "?line="+idLine+"&initFrom="+idInitFrom+"&idTable="+idTable+"&idFrequency="+idFrequency+"&idDayWeek="+idDayWeek+"&hour="+hour;

									Window().create({'url' : '/lineBus/showDetail/' + params, 'title': 'Informações adicionais', 'icon': 'user', 'width': 500, 'height': 150, 'modal':1});

								});

							}else{

								Functions().messageModal('Atenção', response.content);

							}

							objFunctions.unmask('contentHour_' + windowId);

                        }
                });

            },

            setFieldHour : function(idField){

                var elementField = '#insertHour_'  + idField + '_' + windowId;
                var elementTab   = '#tabHourItem_' + idField + '_' + windowId;
                var elementClass = '_' + idField + '_' + windowId;

				$('#insertHour_' + idField + '_' + windowId).mask("99:99");

                $('#insertHour_' + idField + '_' + windowId).keypress(function(event){

                if ( event.which == 13 ) {
    
                    var valueHour       = $(elementField).val();
                    var valueHourLabel  = valueHour;
                    valueHour           = valueHour.replace(':', '');
    
                    if(valueHour <= 2359){
    
                        var value         = 0;
                        var valueSelected = -1;
                        var status        = true;
    
                        $(elementTab + ' > li').each(function(){
    
                            value = $(this).attr('id');
    
                            if(value < valueHour) valueSelected = value;
    
                            if(value == valueHour) status = false;
    
                        });
    
                        if(status == true){
    
                            var tpl = '<li id="' + valueHour + '" class="' + valueHour + elementClass + '">' + valueHourLabel + '<div><a class="deleteHour" href="javascript:;"><img src="/image/icon/cross.png"/></a> <a href="javascript:;" class="infoHour"><img src="/image/icon/add.png"/></a></a></div></li>';

                            if(valueSelected > -1){

                                $('.' + valueSelected + elementClass).after(tpl);

                            }else{

                                if(value > 0){

                                    $(elementTab + ' > li:first').before(tpl);

                                }else{

                                    $(elementTab).append(tpl);

                                    $('#tabHour_' + idField + '_' + windowId + ' > div.emptyData').remove();

                                }

                            }

							Functions().parseScript( document.getElementById( 'content_' + windowId ) );

                            $(this).val('');
                            $(this).focus();

                        }else{

                            Functions().messageModal('Atenção', 'O horário "' + valueHourLabel + '" já existe');
                            $(this).focus();

                        }

                    }else{

                        Functions().messageModal('Atenção', 'Horário inválido');
                        $(this).focus();

                    }

                }

            });

            },

            saveHour : function(){

                var arrResult = new Object;

                var arrHour1 = Array();
                $('#tabHourItem_1_' + windowId + ' > li').each(function(i){
                    arrHour1.push($(this).attr('id'));
                });

                var arrHour2 = Array();
                $('#tabHourItem_2_' + windowId + ' > li').each(function(i){
                    arrHour2.push($(this).attr('id'));
                });

                var arrHour3 = Array();
                $('#tabHourItem_3_' + windowId + ' > li').each(function(i){
                    arrHour3.push($(this).attr('id'));
                });

                arrResult = {week1 : arrHour1, week2 : arrHour2, week3 : arrHour3, line: $('#id_line_'+windowId).val(), idTable: $('#id_table_'+windowId).val(), initFrom: $('#id_initFrom_'+windowId).val(), frequency: $('#id_frequency_'+windowId).val()};

                Functions().mask('contentHour_' + windowId);

				$.ajax({
                        type	: 'post',
                        dataType: 'json',
                        url		: '/lineBus/saveDataHour',
                        data    : arrResult,
                        success	: function(response){

                            if(response.success == true){

								

							}else{

								Functions().messageModal('Atenção', response.content);

							}
							
							Functions().unmask('contentHour_' + windowId);

                        }
                });

            },
            
            showHour : function(){
            
	            $('#idTableFrom_' + windowId).change(function(){
	            	
	            	var objFunctons = Functions();
	            	
					// value selecionado
					var id = $("option:selected", this).val();
					
					objFunctons.mask('contentHour_' + windowId);
					
					$.ajax({
                        type	: 'post',
                        dataType: 'json',
                        url		: '/lineBus/listHourLine?'+id,
                        success	: function(response){
                        	
                        	$('#contentHour_' + windowId).html(response.content);
                        	
                        	objFunctons.unmask('contentHour_' + windowId);

                        }
					});

				});
	            
            }

        }
    }
);