// @autor Gustavo Silva
// @since  01/03/2011

$(document).ready(

    Process = function(){
        
        return {

            ProcessData : function(windowId){

                //Inicia a máscara para o conteúdo
                Window().maskWindow( windowId, 'in' );

                // define o elemento
                element = $('#form_' + windowId);

                url    = element.attr('action');
                //params = element.serialize();

                var sync = YAHOO.util.Connect;

                sync.setForm("form_" + windowId);
                sync.asyncRequest ( "POST" , url, {

                    success: function(obj){
                    	
                    	Process().HandlePost(obj, windowId);

                    	Window().maskWindow( windowId, 'out' );
                    	
                    }

                });

            },
            
            HandlePost : function(response, windowId){
            	
            	if(typeof response == 'undefined' || typeof response.responseText == 'string'){
            		response = eval("(" + response.responseText + ")");
            	}
            	
            	// ==== (MIGRAÇÃO) ====
            	if(response.display != undefined){
            		
            		if(response.display == undefined){
            			response = eval("(" + response.responseText + ")");
            		}
                    
                    this.ProcessElements(response);
                    
                    if(response.redirect != null){
                        
                        // recarrega os dados requisitados na mesma janela
                        Window().load(response.redirect, windowId);
                        
                    }else if(response.windowClose == true){
                            
                        // remove a janela
                        $("#" + windowId).remove();
                        
                    }
            		
            	}else{
            		
            		var obj = response;
            	
	            	// verificar se já está retornando objeto
	            	try{
	            		obj.result.response;
	            		var objReturn = obj;
	            	}catch(e){
	            		var objReturn = eval("(" + obj.responseText + ")");
	            	}
	            	
	                result        = objReturn.result.response;
	                var response  = result;
	
	                if(result.connection == false){
	                    // redireciona usuario sem sessão
	                    window.location = "/";
	                }
	
	                try{
	                    dialog = response.dialog.code;
	                }catch(e){
	                    dialog = null;
	                }
	                
	                // faz o tratamento dos dados com erro
	                if(dialog != null){
	                	
	            		var statusReturn = true;
	
	                    if(response.dialog.code != 5){
	                    	
	                    	// código de erro
	                    	if(statusReturn == 3){
	                    		
	                    		statusReturn = false;
	                    		
	                    	}
	            		
	                		title = (response.dialog.title != '') ? 'Atenção' + " - " + response.dialog.title : 'Atenção';
	
	                        idDialog = "#dialog-modal";
	
	                        // insere a mensagem de erro
	                        $( idDialog ).html( '<div style="padding:10px">' + response.dialog.message + '</div>' );
	
	                        // abre uma informação de erro
	                        $( idDialog ).dialog({
	                                modal       : true,
	                                draggable	: false,
	                                resizable	: false,
	                                title       : "<div style='float:left; padding: 0px 0px 0px 17px'></di><div>"+title+"</div>",
	                                zIndex      : 3000000,
	                                buttons     : {Ok: function(){$(this).dialog( "close" );}}
	                        });
	                        
	                    }
	                        
	                }else{
	                	
	                    // verifica se o retorno é uma url
	                    try{url = eval("(" + response.content + ")");}catch(e){url = null}
	
	                    if(url != null){
	                        // recarrega os dados requisitados na mesma janela
	                        Window().load(url.url, windowId);
	                    }else{
	                        // remove a janela
	                        $("#"+windowId).remove();
	                    }
	                }
	                
	                try{
	                	// tenta atualizar o grid
	                	NewGrid().Refresh(response.dialog.winParent);
	                }catch(e){}
	                
	                return statusReturn;
	                
            	}
            	
            },
            
            ProcessElements : function(response){

                // Processa e cria alguns elementos da janela
            	Window().ProcessElements(response);
                
                // tentar atualizar o elemento grid
                if(response.winParent != null){
                
                	try{ NewGrid().Refresh(response.winParent); }catch(e){}
                	
                }

            }

        }
    })