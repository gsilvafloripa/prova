$(document).ready(

    Functions = function(){
        
        return {

                /**
                 * Gera data e hora do sistema
                 */
                DateTimeSystem : function(){

                    var dateTime = new Date;
                    var date = this.FormatDateTime(dateTime.getHours()) + ":" + this.FormatDateTime(dateTime.getMinutes()) + ":" + this.FormatDateTime(dateTime.getSeconds());

                    //document.getElementById("timer_sys").innerHTML = date;
                    $("#timer_sys").html( date );

                    setTimeout( function(){ Functions().DateTimeSystem() }, 1000 );

                },

                FormatDateTime : function( value ){

                    return (value > 9) ? "" + value : "0" + value;

                },

                sleep : function sleep(milliseconds) {
                    var start = new Date().getTime();
                    for (var i = 0; i < 1e7; i++) {
                      if ((new Date().getTime() - start) > milliseconds){
                        break;
                      }
                    }
                },

                /**
                * Executa javascript
                * @param obj_container {object} Objeto que contem o codigo JS
                */
                parseScript: function(obj_container){

                    var arr_tags = obj_container.getElementsByTagName( 'script' );
     
                    for ( var i = 0; i < arr_tags.length; i++ ) {
                        var obj_tag = document.createElement( 'script' );

                        if ( arr_tags[i].getAttribute( 'src') ) {
                            obj_tag.setAttribute( 'src', arr_tags[i].getAttribute( 'src' ) );

                        } else {
                            obj_tag.text = arr_tags[i].innerHTML;
                        }
         
                        try {document.body.appendChild( obj_tag )}catch(e){};
                    }
                },
                
                IsArray : function(o){
                    return( typeof( o.length) == "undefined" ) ? false : true;
                },

                /**
                 * Cria cookie no sistema
                 */
                setCookieWindow : function( cookie_name, cookie_value){

                    expire_in_days = 1000;

                    var expire = new Date();
                    expire.setTime(expire.getTime() + 1000*60*60*24*parseInt(expire_in_days));
                    cookie_expire = "; expires=" + expire.toGMTString();

                    document.cookie = escape( cookie_name ) + "=" + escape( cookie_value ) + cookie_expire;
                },

                getCookieWindow : function( cookie_name ){

                    if ( !document.cookie.match( eval("/" + escape( cookie_name ) + "=/") ) )
                    {
                        return false;
                    }

                    return unescape( document.cookie.replace( eval("/^.*?" + escape( cookie_name ) + "=([^\\s;]*).*$/"), "$1" ) );

                },

                renderCombobox : function(idChangeElement, idElement, idDivElement, url, valueSelect, valueDefault){

                    $("#" + idElement).html("<option value='0'>Carregando...</option>");

                    // desabilita o elemento
                    $("#" + idElement ).attr('disabled', 'disabled');

                    // adiciona a imagem loading
                    //$("#" + idDivElement ).append("<div id='loadingSearch' style='float:left; background: url(\"/image/loading2.gif\") no-repeat scroll 2px 9px; height:28px; width:48px'></div>");

                    if(idChangeElement != ''){
                        // desabilita o elemento
                        $("#" + idChangeElement ).attr('disabled', 'disabled');
                    }

                    $.ajax({
                            type	: 'get',
                            dataType: 'json',
                            url		: url,
                            success	: function(response){

                                var result = response.result;
        
                                var arrResult = eval( "(" +  result.response.content + ")" );

                                if(valueDefault == undefined){
                                    options = "<option value='0'></option>";
                                }else{
                                    options = "<option value='" + valueDefault.value + "'>" + valueDefault.description + "</option>";
                                }
        
                                for(var i in arrResult){
                                    options += '<option value="' + arrResult[i].value + '">' + arrResult[i].text + '</option>';
                                }
        
                                // limpa o elemento
                                $("#" + idElement).html( "" );
        
                                // adiciona os dados no elemento
                                $("#" + idElement).html( options );
        
                                if(idChangeElement != ''){
                                    // habilita o elemento
                                    $("#" + idChangeElement).removeAttr('disabled');
                                }
        
                                // habilita o elemento
                                $("#" + idElement).removeAttr('disabled');
        
                                if(valueSelect != undefined){
        
                                    $('#' + idElement).find('option[value="'+valueSelect+'"]').attr('selected',true);
        
                                }
        
                                // remove o loading
                                $("#loadingSearch" ).remove();
                            }
                    });
                },

                messageModal : function(title, content){

                    // insere a mensagem de erro
                    $( "#dialog-modal" ).html( "<div style='padding:10px'>" + content + "</div>");

                    // abre uma informa��o de erro
                    $( "#dialog-modal" ).dialog({
                            modal       : true,
                            draggable	: false,
                            resizable	: false,
                            title       : "<div style='float:left; background:url(/image/icon/cross.png) no-repeat scroll -1px 1px transparent; padding: 0px 0px 0px 17px'></di><div>" + title + "</div>",
                            zIndex      : 3000000,
                            buttons     : {Ok: function(){$(this).dialog( "close" );}}
                    });

                },

                /**
                * cria mascara para as janelas
                */
               mask : function(id){

                    strOpacity = 0.7;

                   var strWidth  = $('#' + id).width();
                   var strHeight = $('#' + id).height();

                   var strCenterWidth  = Math.round((strWidth - 120)/2);
                   var strCenterHeight = Math.round((strHeight - 30)/2);

                   $('#' + id).prepend("<div id='maskLoading_" + id + "' style='position:absolute;z-index:30'><div style='position:absolute;background:#eeeeee; border-radius:5px; padding:5px; z-index:31; width:120px; font-weight:bold; color:#A55200; top:"+strCenterHeight+"px; left:" + strCenterWidth + "px'><div style='background:#ffffff; padding:5px; border:1px solid #DADADA'><div style='float:left'><img src='/image/loading.gif'/></div><div style='float:left; margin-left:10px; color:#666666'>Aguarde...</div><div style='clear:both'></div></div></div><div style='position:fixed; width:"+strWidth+"px; height:" + strHeight+ "px; z-index:5;opacity:0.7; background:#ffffff'></div></div>");

               },

               unmask : function(id){

                var mask = $( "#maskLoading_" + id);
                mask.remove();

            }

        }
        
    })