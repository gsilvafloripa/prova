$(document).ready(function(){

    TreeView = function(windowId, elRender){

        // define o id da janela
        TreeView.windowId = windowId;

        // define onde será renderizado o elemento
        TreeView.elRender = elRender != undefined ? elRender : windowId;

        return {

            /**
             * Cria o objeto TreeView
             * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
             * @param jsonData
             * @return object
             */
            __createObject : function(jsonData){

                var elRender = "dataTreeView_" + TreeView.elRender;

                // cria o objeto do elemento
                var objTreeView = new YAHOO.widget.TreeView(elRender, jsonData);

                // renderiza o elemento
                objTreeView.render();

                return objTreeView;

            },

            /**
             * TreeView com texto ou checkbox
             * @autor Gustavo Silva <gsilva.floripa@gmail.com.br>
             * @param jsonData
             * @return void
             */
            simpleRender : function(jsonData){

                var objTreeView = this.__createObject(jsonData);

                // ação para treeView que tiver checkBox
                objTreeView.subscribe('clickEvent',function(oArgs) {

                    try{

                        var ElementInput = $('#' + oArgs.node.contentElId + ' input');

                        if(ElementInput.attr('checked')){ ElementInput[0].checked = false; }else{ ElementInput[0].checked = true; }

                    }catch(e){}

                });

            }

        }

    }

});