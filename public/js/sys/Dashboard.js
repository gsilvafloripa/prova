// @autor Gustavo Silva
// @size 01/03/2011

$(document).ready(

    Dashboard = function(){
        
        return {
                /**
                 * Inicializa propriedades do sistema
                 */
                Init : function(){

                    // Inicializa relógio do sistema
                    var func = Functions()

                    func.DateTimeSystem();
                    
                    $(document).ready(function(){
                    
	                    jQuery.noticeAdd({
	                        text        : 'Bem vindo ao sistema',
	                        stay        : false,
	                        stayTime    : 3000,
	                        type        : 'info'
	                    });
	                    
                    });

                },

                closeSystem : function(){

                    $("#dialog-confirm").html("Deseja realmente sair do sistema?");

                    $( "#dialog-confirm" ).dialog({
                            modal	: true,
                            draggable	: false,
                            resizable	: false,
                            zIndex      : 1000000000,
                            title	: 'Sair',
                            buttons	: {"Não": function(){ $(this).dialog( "close" );},
                                           "Sim":function(){
                                                $.ajax({
                                                    type	: 'post',
                                                    dataType    : 'json',
                                                    url		: "/index/closeSystem",
                                                    success	: function(response){
                                                            window.location = '/';
                                                    }
                                                });
                                           }
                                          }
                    });
                },

                postItLayout : function(id, top, left, content){

                    content = (content == null || content == '') ? 'Digite aqui...' : content;
    
                    postIt  = '';
                    postIt += '<div class="postit" id="postit-' + id + '" style="position:absolute; top:' + top + 'px; left:' + left + 'px; border-radius:4px; background:#FFF1A3; border:1px solid #ffffff; padding-left:1px; padding-right:1px;">';
                    postIt += '  <div style="float:left; color:#ffffff; cursor:move; padding-top:5px; padding-bottom:5px; padding-left:5px; padding-right:5px; text-align:center; color:#333333; font-weight:bold; width:75%">Lembrete!</div><div style="float:right; margin-right:5px; margin-top:5px; font-size:10px; font-weight:bold"><a style="background:url(\'/image/icon/delete.png\') no-repeat scroll -1px -1px transparent; padding-left:14px"href="#" id="postitClose-' + id + '"></a></div><div style="clear:both"></div>';
                    postIt += '  <textarea id="postitContent-' + id + '" style="background:#FFF1A3; overflow:auto; border:none; resize: horizontal; resize:both; width:120px; height:100px">' + content + '</textarea>';
                    postIt += '</div>';
    
                    return postIt;
    
                },
    
                postit: function(json){
    
                    objApp = this;
                    postIt = '';
    
                    var objLocationShort = $("#sysContent");

                    try{
    
                        json[0].top;
    
                        $.each(json, function(index, value) {
    
                            postIt = objApp.postItLayout(value.id, value.top, value.left, value.content);
    
                            $("#sysContent").append(postIt);
    
                            $("#postit-" + value.id).draggable({
                                containment : $( "#sysContent" ),
                                stop        : function(event, ui) {
        
                                   objApp.savePostit( this.id, ui.position.top, ui.position.left);
        
                               }
                            });
    
                            $('#postitContent-' + value.id).blur(function(e) {
                                objApp.savePostit(e.target.id, undefined, undefined, e.currentTarget.value);
                            });
    
                            $('#postitClose-' + value.id).click(function(e) {
                                $.post("/index/deletePostit", {id: e.target.id});
                                var id = e.target.id;
                                id = id.split("-");
                                $("#postit-" + id[1]).remove();
                            });
    
                        });
    
                    }catch(e){
    
                        //postIt += objApp.createPostit();
    
                    }
    
                },
    
                createPostit : function(){
    
                    objApp = this;
    
                    $.ajax({
        
                            url     : "/index/createPostit",
                            success : function(json){

                                json = eval("(" + json + ")");
                                json = json.result.response.content;

                                var top  = 5;
                                var left = 645;
    
                                var result = objApp.postItLayout(json, top, left, 'Digite aqui...');
    
                                objApp.savePostit("#postit-" + json, top, left);
    
                                $("#sysContent").append(postIt);
    
                                $("#postit-" + json).draggable({
                                    containment : $( "#sysContent" ),
                                    stop        : function(event, ui) {
    
                                       objApp.savePostit( this.id, ui.position.top, ui.position.left);
    
                                   }
                                });
    
                                $('#postitContent-' + json).blur(function(e) {
                                    objApp.savePostit(e.target.id, undefined, undefined, e.currentTarget.value);
                                });
    
                                $('#postitClose-' + json).click(function(e) {
                                    $.post("/index/deletePostit", {id: e.target.id});
                                    var id = e.target.id;
                                    id = id.split("-");
                                    $("#postit-" + id[1]).remove();
                                });
    
                            }
    
                    }, objApp);
    
                },
    
                savePostit : function(id, top, left, content){
    
                    var vid       = id;
                    var vtop      = (top     == undefined) ? '' : top;
                    var vleft     = (left    == undefined) ? '' : left;
                    var vcontent  = (content == undefined) ? '' : content;
    
                    $.post("/index/savePostit", {id: vid, top: vtop, left: vleft, content: vcontent });
    
                }
        }
        
    })

Dashboard().Init();