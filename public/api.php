<?php
/**
 * @author Gustavo Silva <gsilva.floripa@gmail.com>
 * @since 03/06/2015
 * 
 */

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_start();

date_default_timezone_set('America/Sao_Paulo');

require("../config/constantes.php");

require(PATH_MODEL . "database.php");
require(PATH_MODEL . "sysFunctions.php");
require(PATH_MODEL . "api/apiRest.php");

function _tst($data){
    print("<pre>");
    print_r($data);
    print("</pre>");
}
 
$controller = new ApiRest();
$controller->run();
