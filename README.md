# README #

Teste de Conhecimentos - Analista Desenvolvedor

### Informações sobre o projeto ###

Nesta prova de conhecimento eu utilizei um framework simples criado por mim em 2011, utilizando PHP 0.0, Javascript (Jquery, Yui) e Mysql. 
Sua interface simula uma ambiente desktop na web, utilizando menus dinâmicos, menus de contexto, janela drag and drop, grid, e tooltips de informações sobre as ações.
Para interagir no grid, você pode clicar com o botão direto do mouse em um registro específico e ele mostrará as ações relacionadas.

=================== IMPORTANTE ================

Todas as respostas das questõs desta prova estão no menu "Questões" no sistema.
===============================================

### Confiurações para rodar o sistema ###

1. - Configuar um virtualHost
     Sugestão de dominio: prova.dev.com.br

2. - Subir o banco de dados
     O SQL do banco está na pasta "lib"

3. - Configurar as constantes
     Arquivo disponível na pasta "congif/constantes.php"
     define('DB_USER', 'XXXXXXX');
     define('DB_PASS', 'XXXXXXX');
     define('DB_NAME', 'XXXXXXX');
     define('DB_PORT', '3306');
     define('DB_HOST', 'XXXXXXX');
     define('DB_TYPE', 'mysql');

4. - Login e senha de acesso
     Login: gsilva
     Senha: b456a123 

4. - Para auxiliar a instalação e execução pode se instalar no firefox o firebug para possível verificações de erro.

### Arquivos desenvolvidos exclusivamente para este teste ###

1. controller/controllerQuestions.hp
   model/questions/questions.php
   model/taskManager.php

2. public/api.php
   model/api/api.php
   model/api/apiRest.php
   model/api/taskmanager/ApiTaskmanager.php

3. Configuração do .htaccess para direcionamento da API