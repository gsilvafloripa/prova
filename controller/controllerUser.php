<?php

/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class controllerUser{

    /**
     * Cria novo usuário para acesso ao sistema
     * @bypasspermission
     */
    public static function showUser()
    {
        $windowId = $_GET['windowId'];
        $element  = '';
        $rowId    = isset($_GET['rowId']) ? $_GET['rowId'] : null;
        $action   = 'C'; // C(create), U(update)

        // buscar dados do usuário se tiver o rowId
        if(!is_null($rowId)){
            // informa que é uma action de UPDATE
            $action = 'U';
            
            // cria o elemento hidden
            $hidden        = new ElementInput('hidden',"rowId_{$windowId}", $windowId);
            $hidden->value = $rowId;
            $element      .= $hidden->toHtml();

            // consulta
            $data   = Database::load("SELECT * FROM sys_users WHERE id = '{$rowId}'");
        }

        $hidden        = new ElementInput('hidden',"windowId", $windowId);
        $hidden->value = $windowId;
        $element      .= $hidden->toHtml();

        $name        = new ElementInput('text', 'name', $windowId);
        $name->label = 'Nome';
        $name->size  = 40;
        $name->value = $action == 'U' ? $data->name : null;
        $element    .= $name->toHtml();
        
        $email        = new ElementInput('text', 'email', $windowId);
        $email->label = 'E-mail';
        $email->size  = 40;
        $email->value = $action == 'U' ? $data->email : null;
        $element     .= $email->toHtml();

        $login        = new ElementInput('text', 'login', $windowId);
        $login->label = 'Login';        
        if($action == 'U'){
            $login->value    = $data->login;
            $login->disabled = true;
        }
        $element     .= $login->toHtml();

        $password        = new ElementInput('password', 'password', $windowId);
        $password->label = 'Senha';
        if($action == 'U'){
            $password->value    = '1234';
            $password->disabled = true;
        }
        $element        .= $password->toHtml();

        //TODO@ COLOCAR STATUS PELA SYS_LIST
        $status        = new ElementSelect('status', $windowId);
        $status->label = 'Status';
        $status->valueSelected = $action == 'U' ? $data->status : null;
        $status->data  = array(1 => 'Ativo', 2 => 'Inativo');
        $element      .= $status->toHtml();

        $calendar        = new ElementCalendar('expireDate', $windowId);
        $calendar->label = 'Data de expiração';
        $calendar->value = $action == 'U' ? $data->expire_date : null;
        $element        .= $calendar->toHtml();

        $fieldset = new ElementFieldset('Novo usuário', $element);
        $element  = $fieldset->toHtml();

        $buttons = new ElementButton($windowId);
        $buttons->setButtom('Salvar', 'accept', 'submit');
        $buttons->setButtom('Cancelar', 'cross', 'cancel');

        // cria o form com os elementos
        $form       = new ElementForm($windowId, "/user/requestUser/");
        $form->setButtons($buttons->toHtml());
        $form->data = $element;        

        return $form->toHtml();
    }

    /**
     * Cria novo usuário para acesso ao sistema
     * @bypasspermission
     */
    public static function requestUser()
    {
        $error    = null;
        $windowId = $_POST['windowId'];
        $rowId    = isset($_POST["rowId_{$windowId}"]) ? $_POST["rowId_{$windowId}"] : null;
        $action   = 'C'; // C(create), U(update)

        if(!is_null($rowId)){
            $action = 'U';
        }

        $status     = $_POST['status'];
        $expireDate = $_POST['expireDate_dateDatabase'];

        $name = trim($_POST['name']);
        if(($name == '') or (strLen($name) < 10)){
            $error[] = 'Nome inválido';
        }

        $email = $_POST['email'];
        if(SysFunctions::validateEmail($email) == false){
            $error[] = 'E-mail inválido';
        }

        // somente para action de Create (Insert)
        if($action == 'C'){

            $login = $_POST['login'];
            if($login != ''){
                // consulta para ver se login está em uso
                $sql = Database::load("SELECT id FROM sys_users WHERE login = '{$login}'");

                if($sql->id != ''){
                    $error[] = "Login '{$login}' em uso";
                }
            }else{
                $error[] = 'Login inválido';
            }

            $password = $_POST['password'];
            if((strlen($password) < 6) or (preg_match('/^[a-zA-Z0-9]+$/', $password) == false)){
                $error[] = "Senha inválida (mínimo 6 caractes, com letras maiúsculas, mínúsculas e números)";
            }
        }

        if(!is_null($error)){
            // instância classe de dialog
            $metaDialog = New MetaDialog();
            $metaDialog->code    = 3;
            $metaDialog->message = SysFunctions::formatMessageDialog($error);

            return $metaDialog;
        }else{

            $date = date("Y-m-d H:i:s");
            $user = $_SESSION['system']['user']['id'];

            if($action == 'C'){
                // cria novo usuário
                $values ="'{$date}', '$user', '{$date}', '$user', '{$name}', '{$email}', '{$login}', md5('{$password}'), '{$expireDate}', {$status}";
                Database::execution("INSERT INTO sys_users (date_created, created_by, date_modification, modificated_by, name, email, login, password, expire_date, status) VALUES({$values})");
            }elseif($action == 'U'){
                // Atualiza os dados do usuário
                Database::execution("UPDATE sys_users SET date_modification = '{$date}', modificated_by = '{$user}', name='{$name}', email='{$email}', expire_date = '{$expireDate}', status = {$status} WHERE id = '{$rowId}'");
            }
        }
    }

    /**
     * Mostra tpl com os dados do grid
     * @bypasspermission
     * @return template
     */
    public static function showGrid()
    {
        $windowId = $_GET['windowId'];
        $metadata = array();

        // url para acessar os dados o grid
        $metadata['url']      = "/user/listUsers";
        // campos do grid
        $metadata['fields'][] = array('key' => 'id'   , 'label' => 'ID'    , 'width' => 0  , 'sortable' => 'false', 'resizeable' => 'false', 'hidden' => true);
        $metadata['fields'][] = array('key' => 'name' , 'label' => 'Nome'  , 'width' => 200, 'sortable' => 'true' , 'resizeable' => 'true' , 'hidden' => false);
        $metadata['fields'][] = array('key' => 'login', 'label' => 'Login' , 'width' => 150, 'sortable' => 'true' , 'resizeable' => 'true' , 'hidden' => false);
        $metadata['fields'][] = array('key' => 'email', 'label' => 'E-mail', 'width' => 300, 'sortable' => 'true' , 'resizeable' => 'true' , 'hidden' => false);
        // menu de contexto
        $metadata['contextMenu'][] = array('label' => 'Editar usuário', 'method' => 'Window().create', 'action' => array('url' => '/user/showUser/'       , 'title' => 'Editar usuário', 'icon' => 'user', 'width' => 600, 'height' => 310, 'modal' => 0));
        $metadata['contextMenu'][] = array('label' => 'Permissões'    , 'method' => 'Window().create', 'action' => array('url' => '/user/showPermission/' , 'title' => 'Permissoes'    , 'icon' => 'user', 'width' => 600, 'height' => 310, 'modal' => 0));
        $metadata['contextMenu'][] = array('label' => 'Log de acesso' , 'method' => 'Window().create', 'action' => array('url' => '/user/showGridUserLog/', 'title' => 'Log'           , 'icon' => 'user', 'width' => 700, 'height' => 400, 'modal' => 0));

        $template = new Template();

        $template->assign('windowId', $windowId);
        $template->assign('params'  , json_encode($metadata));

        return $template->fetch("element/newGrid.tpl");
    }

    /**
     * Lista os usuários do sistema
     * @return json
     */
    public static function listUsers()
    {
        $startIndex = $_GET['startIndex'];
        $limit      = isset($_GET['limit'])      ? $_GET['limit']      : 10;
        $start      = isset($_GET['startIndex']) ? $_GET['startIndex'] : 0;

        // busca o total de registro
        $dataAmount = Database::load("SELECT COUNT(*) as count FROM sys_users ");

        //LIMIT {$start}, {$limit}
        $users = Database::loadAll("SELECT * FROM sys_users LIMIT {$start}, {$limit}");

        $records = array();

        foreach($users as $result){

            $data['id']    = $result->id;
            $data['name']  = $result->name;
            $data['login'] = $result->login;
            $data['email'] = $result->email;

            $records[] = $data;
        }

        $arrDataGrid['recordsReturned'] = (int) count($records);
        $arrDataGrid['totalRecords']    = (int) $dataAmount->count;
        $arrDataGrid['startIndex']      = (int) $startIndex;
        $arrDataGrid['sort']            = null;
        $arrDataGrid['dir']             = 'asc';
        $arrDataGrid['pageSize']        = (int) 10;
        $arrDataGrid['records']         = $records;

        return json_encode($arrDataGrid);
    }

    /**
     * Altera a senha o usuário
     * @bypasspermission
     */
    public static function showChangePassword()
    {
        $windowId = $_GET['windowId'];
        $element  = '';
        $rowId    = $_SESSION['system']['user']['id'];

        $hidden        = new ElementInput('hidden',"windowId", $windowId);
        $hidden->value = $windowId;
        $element      .= $hidden->toHtml();

        // Consulta os dados do usuário
        $sql = Database::load("SELECT name, login FROM sys_users WHERE id = '{$rowId}'");

        $name           = new ElementInput('text', 'name', $windowId);
        $name->label    = 'Nome';
        $name->size     = 40;
        $name->value    = $sql->name;
        $name->disabled = true;
        $element        .= $name->toHtml();

        $login          = new ElementInput('text', 'login', $windowId);
        $login->label   = 'Login';
        $login->size    = 40;
        $login->value   = $sql->login;
        $login->disabled = true;
        $element        .= $login->toHtml();

        $password        = new ElementInput('password', 'passwordOld', $windowId);
        $password->label = 'Senha atual';
        $login->size     = 40;
        $element        .= $password->toHtml();

        $password        = new ElementInput('password', 'password', $windowId);
        $password->label = 'Nova senha';
        $login->size     = 40;
        $element        .= $password->toHtml();

        $password        = new ElementInput('password', 'passwordConf', $windowId);
        $password->label = 'Confirmar senha';
        $login->size     = 40;
        $element        .= $password->toHtml();

        $fieldset = new ElementFieldset('Alterar senha', $element);
        $element  = $fieldset->toHtml();

        $buttons = new ElementButton($windowId);
        $buttons->setButtom('Salvar', 'accept', 'submit');
        $buttons->setButtom('Cancelar', 'cross', 'cancel');

        // cria o form com os elementos
        $form       = new ElementForm($windowId, "/user/requestChangePassword/");
        $form->setButtons($buttons->toHtml());
        $form->data = $element;

        return $form->toHtml();
    }

    /**
     * Altera a senha o usuário
     * @bypasspermission
     */
    public static function requestChangePassword()
    {
        $error   = null;
        $idUser  = $_SESSION['system']['user']['id'];
        $pwdOld  = $_POST['passwordOld'];
        $pwd     = $_POST['password'];
        $pwdConf = $_POST['passwordConf'];

        // Consulta os dados do usuário
        $sql = Database::load("SELECT password FROM sys_users WHERE id = '{$idUser}'");

        // verifica se a senha atual confere
        if(md5($pwdOld) == $sql->password){
            // verifica se a senha nova possui mais de 6 caracteres e se possui letra e número
            if((strlen($pwd) >= 6) and (preg_match('/^[a-zA-Z0-9]+$/', $pwd) != false)){
                // verifica se a confirmação da senha está correta
                if($pwd != $pwdConf){
                    $error = " - Confirmação de senha incorreta";
                }
            }else{
                $error = " - Senha inválida (mínimo 6 caractes, com letras maiúsculas, mínúsculas e números)";
            }            
        }else{
            $error = " - Senha atual inválida";
        }

        if(!is_null($error)){
            // instância classe de dialog
            $metaDialog = New MetaDialog();
            $metaDialog->code    = 3;
            $metaDialog->message = $error;

            return $metaDialog;
        }else{

            $date = date("Y-m-d H:i:s");
            $user = $_SESSION['system']['user']['id'];

            // Altera a senha do usuário
            Database::execution("UPDATE sys_users SET date_modification = '{$date}', modificated_by = '{$user}', password=md5('{$pwd}') WHERE id = '{$idUser}'");
        }
    }

    /**
     * Altera dados do usuário
     * @bypasspermission
     */
    public static function showChangeData()
    {
        $windowId = $_GET['windowId'];
        $element  = '';
        $idUser   = $_SESSION['system']['user']['id'];

        $hidden        = new ElementInput('hidden',"windowId", $windowId);
        $hidden->value = $windowId;
        $element      .= $hidden->toHtml();

        // consulta dados do usuário
        $sql = Database::load("SELECT name, email FROM sys_users WHERE id = '{$idUser}'");

        $name        = new ElementInput('text', 'name', $windowId);
        $name->label = 'Nome';
        $name->size  = 40;
        $name->value = $sql->name;
        $element    .= $name->toHtml();

        $email        = new ElementInput('text', 'email', $windowId);
        $email->label = 'E-mail';
        $email->size  = 40;
        $email->value = $sql->email;
        $element     .= $email->toHtml();

        $fieldset = new ElementFieldset('Alterar dados', $element);
        $element  = $fieldset->toHtml();

        $buttons = new ElementButton($windowId);
        $buttons->setButtom('Salvar', 'accept', 'submit');
        $buttons->setButtom('Cancelar', 'cross', 'cancel');

        // cria o form com os elementos
        $form       = new ElementForm($windowId, "/user/requestChangeData/");
        $form->setButtons($buttons->toHtml());
        $form->data = $element;

        return $form->toHtml();
    }

    public static function requestChangeData()
    {
        $error   = null;
        $idUser  = $_SESSION['system']['user']['id'];
        $name    = $_POST['name'];
        $email   = $_POST['email'];

        if(trim($name) == ''){
            $error[] = 'Informe o nome';
        }

        if(SysFunctions::validateEmail($email) == false){
            $error[] = 'E-mail inválido';
        }

        if(!is_null($error)){
            // instância classe de dialog
            $metaDialog = New MetaDialog();
            $metaDialog->code    = 3;
            $metaDialog->message = SysFunctions::formatMessageDialog($error);

            return $metaDialog;
        }else{

            $date = date("Y-m-d H:i:s");
            $user = $_SESSION['system']['user']['id'];

            // Altera a senha do usuário
            Database::execution("UPDATE sys_users SET date_modification = '{$date}', modificated_by = '{$user}', name = '{$name}', email = '{$email}' WHERE id = '{$idUser}'");
        }
    }

    /**
     *  Monta menu recursivo
     *  @param $idParent
     */
    public static function menuAppSystem( $rowId, $idParent = 0)
    {
        $menu = array();

        $sqlApp  = Database::loadAll("SELECT id,id_sys_application FROM sys_user_application WHERE id_sys_user = {$rowId}");

        $arrUserPermission = array();

        foreach($sqlApp as $value){
            $arrUserPermission[$value->id_sys_application] = $value->id_sys_application;
        }

        // consulta dados do menu
        $sql = Database::loadAll("SELECT * FROM sys_menu WHERE id_parent = {$idParent} AND visible = 1 ORDER BY key_order DESC");

        foreach($sql as $key => $value){

            $checked = isset($arrUserPermission[$value->id_fk_application]) ? true : false;

            $elCheckbox = $value->id_fk_application > 0 ? array('checked' => $checked, 'value' => $value->id_fk_application, 'name' => 'check[]') : array();

            $menu[] = array('text' => $value->label, 'checkbox' => $elCheckbox, 'expanded' => true, 'chieldren' => self::menuAppSystem($rowId, $value->id));

        }

        return $menu;
    }
    
    public static function showPermission()
    {
        $windowId = $_GET['windowId'];
        $rowId    = $_GET['rowId'];
        $element  = '';

        $hidden        = new ElementInput('hidden',"windowId", $windowId);
        $hidden->value = $windowId;
        $element      .= $hidden->toHtml();

        $hiddenRowId        = new ElementInput('hidden',"rowId", $rowId);
        $hiddenRowId->value = $rowId;
        $element           .= $hiddenRowId->toHtml();

        $treeView = new ElementTreeView($windowId, self::menuAppSystem($rowId));
        $element .= $treeView->toHtml();

        $buttons = new ElementButton($windowId);
        $buttons->setButtom('Salvar', 'accept', 'submit');
        $buttons->setButtom('Cancelar', 'cross', 'cancel', 'bt_cancel');

        // cria o form com os elementos
        $form       = new ElementForm($windowId, "/user/requestPermission/");
        $form->setButtons($buttons->toHtml());
        $form->data = $element;

        return $form->toHtml();
    }

    public static function requestPermission()
    {
        $error   = null;
        $rowId   = $_POST['rowId'];
        $idUser  = $_SESSION['system']['user']['id'];
        $data    = $_POST['check'];;

        if(!is_null($error)){
            // instância classe de dialog
            $metaDialog = New MetaDialog();
            $metaDialog->code    = 3;
            $metaDialog->message = SysFunctions::formatMessageDialog($error);

            return $metaDialog;
        }else{

            $delete = Database::execution("DELETE FROM sys_user_application WHERE id_sys_user = {$rowId}");

            $date = date("Y-m-d H:i:s");
            $user = $_SESSION['system']['user']['id'];

            $arrData = array();

            foreach($data as $value){
                if(!empty($value)){
                    $arrData[] = "('{$date}', $user, '{$date}', $user, $value, $rowId)";
                }
            }

            $values = implode(",", $arrData);

            Database::execution("INSERT INTO sys_user_application (date_created, created_by, modificated_by, date_modification, id_sys_application, id_sys_user) VALUES {$values}");

        }
    }
    
    /**
     * Mostra tpl com os dados do grid
     * @bypasspermission
     */
    public static function showGridUserLog()
    {
        $windowId = $_GET['windowId'];
        $rowId    = $_GET['rowId'];
        $metadata = array();

        // url para acessar os dados o grid
        $metadata['url']      = "/user/listUserLog?rowId={$rowId}";
        // campos do grid
        $metadata['fields'][] = array('key' => 'id'       , 'label' => 'ID'          , 'width' => 0  , 'sortable' => 'false', 'resizeable' => 'false', 'hidden' => true);
        $metadata['fields'][] = array('key' => 'date'     , 'label' => 'Data'        , 'width' => 60 , 'sortable' => 'true' , 'resizeable' => 'true' , 'hidden' => false);
        $metadata['fields'][] = array('key' => 'ip'       , 'label' => 'Ip'          , 'width' => 100, 'sortable' => 'true' , 'resizeable' => 'true' , 'hidden' => false);
        $metadata['fields'][] = array('key' => 'ip2'      , 'label' => 'Servidor'    , 'width' => 100, 'sortable' => 'true' , 'resizeable' => 'true' , 'hidden' => false);
        $metadata['fields'][] = array('key' => 'userAgent', 'label' => 'Info Usuário', 'width' => 300, 'sortable' => 'true' , 'resizeable' => 'true' , 'hidden' => false);

        $template = new Template();

        $template->assign('windowId', $windowId);
        $template->assign('params'  , json_encode($metadata));

        return $template->fetch("element/newGrid.tpl");
    }

    /**
     * Lista os usuários do sistema
     * 
     */
    public static function listUserLog()
    {
        $rowId      = $_GET['rowId'];
        $startIndex = $_GET['startIndex'];
        $limit      = isset($_GET['limit'])      ? $_GET['limit']      : 10;
        $start      = isset($_GET['startIndex']) ? $_GET['startIndex'] : 0;

        // busca o total de registro
        $dataAmount = Database::load("SELECT COUNT(*) as count FROM sys_user_log WHERE id_fk_user = {$rowId}");

        //LIMIT {$start}, {$limit}
        $sql = Database::loadAll("SELECT * FROM sys_user_log WHERE id_fk_user = {$rowId}  LIMIT {$start}, {$limit}");

        $records = array();

        foreach($sql as $result){

            $data['id']        = $result->id;
            $data['date']      = date("d/m/Y", strtotime($result->date));
            $data['ip']        = $result->remote_addr;
            $data['ip2']       = $result->http_x_forwaded_for;
            $data['userAgent'] = $result->user_agent;

            $records[] = $data;
        }

        $arrDataGrid['recordsReturned'] = (int) count($records);
        $arrDataGrid['totalRecords']    = (int) $dataAmount->count;
        $arrDataGrid['startIndex']      = (int) $startIndex;
        $arrDataGrid['sort']            = null;
        $arrDataGrid['dir']             = 'asc';
        $arrDataGrid['pageSize']        = (int) 10;
        $arrDataGrid['records']         = $records;

        return json_encode($arrDataGrid);
    }

}