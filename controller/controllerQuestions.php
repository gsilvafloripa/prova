<?php
include_once(PATH_MODEL.'questions/questions.php');
include_once(PATH_MODEL.'questions/taskManager.php');

/**
 * Classe para implementação das questão da prova
 *
 * @author Gustavo Silva <gsilva.floripa@gmail.com>
 * @since 03/06/2015
 */
class controllerQuestions extends ModelWindow
{
    
    /**
     * Método controller que define a estrutura do grid 
     * para chamar e receber os dados
     * 
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return object MetaResult
     */
    public function showGridQuestion1()
    {
        $metadata = array();

        // campos do grid
        $metadata['fields'][] = array('key' => 'id', 'label' => 'ID', 'width' => 0, 'sortable' => 'false', 'resizeable' => 'false', 'hidden' => true);
        $metadata['fields'][] = array('key' => 'indice', 'label' => 'Índice', 'width' => 200, 'sortable' => 'true', 'resizeable' => 'false', 'hidden' => false);
        $metadata['fields'][] = array('key' => 'label', 'label' => 'Label', 'width' => 200, 'sortable' => 'true', 'resizeable' => 'false', 'hidden' => false);
        
        // define a url de chamada dos dados referente ao grid
        $metadata['url'] = "/questions/listGridQuestion1";

        // instancia template para html
        $template = new Template();

        $template->assign('windowId', $this->getWindowId());
        $template->assign('params'  , json_encode($metadata));

        $this->metaResult->content = $template->fetch("element/newGrid.tpl");
        
        return $this->metaResult;
    }
    
    /**
     * Método controler responsável por mostrar os dados no grid
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return json
     */
    public function listGridQuestion1()
    {
        $startIndex = $_GET['startIndex'];
        $start = isset($_GET['startIndex']) ? $_GET['startIndex'] + 1 : 1;
        $limit = $start + 9;

        // instancia a classe Questions
        $modelQuestion = new ModelQuestions();
        
        $records = $modelQuestion->listlNumbers($start, $limit);

        $arrDataGrid['recordsReturned'] = (int) count($records);
        $arrDataGrid['totalRecords']    = (int) 100;
        $arrDataGrid['startIndex']      = (int) $startIndex;
        $arrDataGrid['sort']            = null;
        $arrDataGrid['dir']             = 'asc';
        $arrDataGrid['pageSize']        = (int) 10;
        $arrDataGrid['records']         = $records;

        return json_encode($arrDataGrid);
    }
    
    /**
     * Método responsável por mostrar os dados da questão 2
     *
     * @author Gustavo Silva<gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @since Object | MetaResult
     */
    public function showQuestion2()
    {
        // define o exemplo não otimizado
        $font1 =
        '<?
        if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] == true) {
            header("Location: http://www.google.com");
            exit();
        } elseif (isset($_COOKIE["Loggedin"]) && $_COOKIE["Loggedin"] == true) {
            header("Location: http://www.google.com");
            exit();
        }';
        
        // Instancia a classe para criar elemento TextArea
        $elTextArea1 = new ElementTextarea('element1', $this->getWindowId());
        
        $elTextArea1->label = "Código não otimizado";
        $elTextArea1->cols = 100;
        $elTextArea1->rows = 8;
        $elTextArea1->disabled = true;
        $elTextArea1->value = $font1;
        
        // define o exemplo otimizado
        $font2 =
        '<?
        ##=========================================
        ## Só verificar se existe não serve, pois a variável pode estar setada como false
        ##=========================================
        $loggedinSession = isset($_SESSION["loggedin"]) ? $_SESSION["loggedin"] : false;
        $loggedinCookie = isset($_COOKIE["Loggedin"]) ? $_COOKIE["Loggedin"] : false;
        
        if ($loggedinSession == true or $loggedinCookie == true) {
            header("Location: http://www.google.com");
            exit();
        }';
        
        // Instancia a classe para criar elemento TextArea
        $elTextArea2 = new ElementTextarea('element2', $this->getWindowId());
        
        $elTextArea2->label = "Código com otimização";
        $elTextArea2->cols = 100;
        $elTextArea2->rows = 10;
        $elTextArea2->disabled = true;
        $elTextArea2->value = $font2;
        
        // instancia a classe para criação do fieldSet
        $elFieldSet = new ElementFieldset('Questão 2 - Otimização de código', $this->getWindowId());
        
        // define o conteúdo do fieldSet
        $elFieldSet->value = $elTextArea1->toHtml() . $elTextArea2->toHtml();
        
        // renderiza o elemento e define no conteúdo da janela
        $this->metaResult->content = $elFieldSet->toHtml();
        
        return $this->metaResult;
    }
    
    /**
     * Método responsável por mostrar os dados da questão 3
     *
     * @author Gustavo Silva<gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @since Object | MetaResult
     */
    public function showQuestion3()
    {
        // define o exemplo não otimizado
        $font1 =
        '<?php
            class MyUserClass
            {
                public function getUserList()
                {
                    $dbconn = new DatabaseConnection("localhost","user","password");
                    $results = $dbconn->query("select name from user");
                    
                    sort($results);
                    
                    return $results;
                }
            }';
        
        // Instancia a classe para criar elemento TextArea
        $elTextArea1 = new ElementTextarea('element1', $this->getWindowId());
        
        $elTextArea1->label = "Código não otimizado";
        $elTextArea1->cols = 100;
        $elTextArea1->rows = 8;
        $elTextArea1->disabled = true;
        $elTextArea1->value = $font1;
        
        // define o exemplo otimizado
        $font2 =
        '<?php
            ## =======================================
            ## NESTE CASO EM ESPECÍFICO EU DEIXA DA MESMA FORMA, POIS, ESTÁ RETORNANDO SOMENTE 1 CAMPO
            ## QUE NESTE EXEMPLO O PROCESSAMENTO DE ORDENAÇÃO DO PHP SERIA MAIS RÁPIDO
            ## =======================================
            
            class MyUserClass
            {
                public function getUserList()
                {
                    $dbconn = new DatabaseConnection("localhost","user","password");
                    $results = $dbconn->query("select name from user");
                    
                    sort($results);
                    
                    return $results;
                }
            }';
        
        // Instancia a classe para criar elemento TextArea
        $elTextArea2 = new ElementTextarea('element2', $this->getWindowId());
        
        $elTextArea2->label = "Código com otimização";
        $elTextArea2->cols = 100;
        $elTextArea2->rows = 10;
        $elTextArea2->disabled = true;
        $elTextArea2->value = $font2;
        
        // instancia a classe para criação do fieldSet
        $elFieldSet = new ElementFieldset('Questão 3 - Otimização de código', $this->getWindowId());
        
        // define o conteúdo do fieldSet
        $elFieldSet->value = $elTextArea1->toHtml() . $elTextArea2->toHtml();
        
        // renderiza o elemento e define no conteúdo da janela
        $this->metaResult->content = $elFieldSet->toHtml();
        
        return $this->metaResult;
    }
    
    /**
     * Método controller que define a estrutura do grid 
     * para chamar e receber os dados da questão 4
     * 
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return object MetaResult
     */
    public function showGridQuestion4()
    {
        $metadata = array();

        // campos do grid
        $metadata['fields'][] = array('key' => 'id', 'label' => 'ID', 'width' => 0, 'sortable' => 'false', 'resizeable' => 'false', 'hidden' => true);
        $metadata['fields'][] = array('key' => 'title', 'label' => 'Título', 'width' => 200, 'sortable' => 'true', 'resizeable' => 'false', 'hidden' => false);
        $metadata['fields'][] = array('key' => 'description', 'label' => 'Descrição', 'width' => 250, 'sortable' => 'true', 'resizeable' => 'false', 'hidden' => false);
        $metadata['fields'][] = array('key' => 'priority', 'label' => 'Prioridade', 'width' => 250, 'sortable' => 'true', 'resizeable' => 'false', 'hidden' => false);
        
        // define o menu de contexto
        $metadata['contextMenu'][] = array('label' => 'Editar tarefa', 'method' => 'Window().create', 'action' => array('url' => '/questions/showFormTaskManager/' , 'title' => 'Editar tarefa', 'icon' => 'page_white', 'width' => 700, 'height' => 300, 'modal' => 0));
        $metadata['contextMenu'][] = array('label' => 'Excluir', 'method' => 'NewGrid().Delete', 'action' => array('url' => '/questions/deleteTaskManager/'));

        
        // define a url de chamada dos dados referente ao grid
        $metadata['url'] = "/questions/listGridQuestion4";

        // instancia template para html
        $template = new Template();

        $template->assign('windowId', $this->getWindowId());
        $template->assign('params'  , json_encode($metadata));
        
        $template->assign('topBar'  , SysFunctions::createButtonNewWindow('Adicionar',
                                                                          '/questions/showFormTaskManager',
                                                                          'Questão 4',
                                                                          'add',
                                                                          700,
                                                                          300,
                                                                          0
                                                                          ));

        $this->metaResult->content = $template->fetch("element/newGrid.tpl");
        
        return $this->metaResult;
    }
    
    /**
     * Método controler responsável por mostrar os dados no grid 4
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return json
     */
    public function listGridQuestion4()
    {
        $startIndex = $_GET['startIndex'];
        $start = isset($_GET['startIndex']) ? $_GET['startIndex']  : 0;
        $limit = $startIndex;

        // instancia a classe Questions
        $modelQuestion = new ModelTaskManager();
        
        $records = $modelQuestion->listDataByPriority();
        
        $arrDataGrid['recordsReturned'] = (int) count($records);
        $arrDataGrid['totalRecords']    = (int) ($records);
        $arrDataGrid['startIndex']      = (int) $startIndex;
        $arrDataGrid['sort']            = null;
        $arrDataGrid['dir']             = 'asc';
        $arrDataGrid['pageSize']        = (int) 10;
        $arrDataGrid['records']         = $records;

        return json_encode($arrDataGrid);
    }
    
    /**
     * Método responsável por montar o formulário da questão 4
     * 
     * @author Gustavo Silva
     * @since 03/06/2015
     * @return object MetaResult
     */
    public function showFormTaskManager()
    {
        // buscar dados do usuário se tiver o rowId
        if ($this->getEditMode() == true) {
            
            // cria o elemento hidden
            $hidden        = new ElementInput('hidden',"rowId", $this->getWindowId());
            $hidden->value = $this->getRowId();
            $arrElement[]  = $hidden->toHtml();

            $modelTaskManager = new ModelTaskManager();
            
            // consulta os dados da linha
            $objData = $modelTaskManager->getById($this->getRowId());

        }

        $element            = new ElementInput('hidden',"windowId", $this->getWindowId());
        $element->value     = $this->getWindowId();
        $arrElement[]       = $element->toHtml();
        
        $element            = new ElementInput('hidden',"winParent", $this->getWindowId());
        $element->value     = $this->getWinParent();
        $arrElement[]       = $element->toHtml();

        $element            = new ElementInput('text', 'title', $this->getWindowId());
        $element->label     = 'Título';
        $element->size      = 50;
        $element->value     = isset($objData) ? $objData->title : null;
        $arrElement[]       = $element->toHtml();

        $element            = new ElementInput('text', 'priority', $this->getWindowId());
        $element->label     = 'Prioridade';
        $element->size      = 5;
        $element->value     = isset($objData) ? $objData->priority : null;
        $arrElement[]       = $element->toHtml();
        
        // instancia elemento TextArea
        $elTextArea = new ElementTextarea('description', $this->getWindowId());
        
        $elTextArea->label = "Descrição";
        $elTextArea->cols = 60;
        $elTextArea->rows = 5;
        $elTextArea->value = isset($objData) ? $objData->description : null;;
        $arrElement[] = $elTextArea->toHtml();

        $element = implode(" ", $arrElement);

        $fieldset = new ElementFieldset('Gerenciardo de Tarefas', $element);
        $element  = $fieldset->toHtml();

        $buttons = new ElementButton($this->getWindowId());
        $buttons->setButtom('Salvar', 'accept', 'submit');

        // cria o form com os elementos
        $form       = new ElementForm($this->getWindowId(), "/questions/requestFormTaskManager/");
        $form->setButtons($buttons->toHtml());
        $form->data = $element;

        $this->metaResult->content = $form->toHtml();
        
        return $this->metaResult;
    }
    
    /**
     * Método responsável por salvar os dados do formulário
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return object MetaResult
     */
    public function requestFormTaskManager()
    {
        // instancia classe model do gerenciador
        $modelTaskManager = new ModelTaskManager();
        
        try {
            // instancia medotodo para salvar os dados
            $modelTaskManager->save($_POST, $this->getRowId());
        
            $this->metaResultSave->setMessage('Salvo com sucesso');
            $this->metaResultSave->setSaved(true);
            $this->metaResultSave->setMetaResultSave();
        } catch (Exception $e) {
            $this->metaResultSave->setMessage($e->getMessage());
        }
        
        return $this->metaResultSave;
    }
    
    /**
     * Método responsável por deletar um reigstro específico
     *
     * @author Gustavo Andrade <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return object MetaResult
     */
    public function deleteTaskManager()
    {
        // instancia classe model do gerenciador
        $modelTaskManager = new ModelTaskManager();
        
        try {
            // instancia medotodo para salvar os dados
            $modelTaskManager->deleteById($this->getRowId());
        
            $this->metaResultSave->setMessage('Deletado com sucesso', 0, 'modal', 'false');
            $this->metaResultSave->setSaved(true);
            $this->metaResultSave->setMetaResultSave();
        } catch (Exception $e) {
            $this->metaResultSave->setMessage($e->getMessage());
        }
        
        return $this->metaResultSave;
    }
}
