<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class controllerIndex
{
    /**
     *  Módulo principal
     *  @bypasspermission
     */
    public static function index()
    {
        $template = new Template();

        $template->assign('title', TITLE);

        //Verifica se o usuário está logado
        if(isset($_SESSION['system']['user'])){

	    // Cria o menu do usuário
	    $menu   = self::menuRecursive();
	    $menu[] = array("text" => '<span style="margin: 2px 0px; background: url(\'/image/icon/delete.png\') no-repeat scroll 0px 0pt transparent; padding: 1px 11px;"></span><span>Sair</span>', "url" => "javascript: Dashboard().closeSystem()");
		$menu[] = array("text" => '<span style="margin: 2px 0px; background: url(\'/image/icon/note_add.png\') no-repeat scroll 0px 0pt transparent; padding: 1px 11px;"></span><span>Add Lembrete</span>', "url" => "javascript: Dashboard().createPostit()");

            $template->assign('dataMenu'  , json_encode($menu), true);
            $template->assign('user'      , $_SESSION['system']['user']['login']);
            $template->assign('startDate' , date("d/m/Y H:i", strtotime($_SESSION['system']['user']['startDate'])));

			$template->assign('postit'	  , self::getPostit());

            $template->display('dashboard.tpl');

        }else{
            $template->display('login.tpl');
        }
    }

    /**
     *  Finaliza sistema
     *  Módulo principal
     *  @bypasspermission
     */
    public static function closeSystem()
    {
        unset($_SESSION['system']);
    }
    
    /**
     *  Monta menu recursivo
     *  @param $idParent
     */
    public static function menuRecursive( $idParent = 0 )
    {
        $return  = null;
        $content = '';

        // consulta dados do menu
        $menu = Database::loadAll("SELECT * FROM sys_menu WHERE id_parent = {$idParent} AND visible = 1 ORDER BY key_order DESC");

        $sqlUserApp = Database::loadAll("SELECT id_sys_user, id_sys_application FROM sys_user_application WHERE id_sys_user = {$_SESSION['system']['user']['id']}");

        $appUser = array();

        foreach($sqlUserApp as $data){
            $appUser[$data->id_sys_application] = $data->id_sys_application;
        }

        foreach($menu as $key=>$value){

            $handle  = '';

            $title    = $value->label;

			$disabled = false;

            //Verifica se possui alguma aplicação associada
            if(!is_null($value->id_fk_application) and ($value->id_fk_application != 0)){

                // consulta dados da aplicação
                $application = Database::load("SELECT * FROM sys_application WHERE id = {$value->id_fk_application}");

                if(isset($appUser[$value->id_fk_application])){

                    $json = "{'url':'$application->url', 'title':'$application->title', 'icon':'$application->icon', 'width':$application->width, 'height':$application->height, 'modal':$application->modal}";
                    $handle = "javascript:Window().create({$json})";

                }else{
					$disabled = true;
				}
            }

			$data    = array();
	
			// define o titulo
			$icon = (empty($value->id_fk_application)) ? $value->icon : $application->icon;
			$data["text"] = '<span style="margin: 2px 0px; background: url(\'/image/icon/' . $icon . '.png\') no-repeat scroll 0px 0pt transparent; padding: 1px 11px;"></span><span>' . $title . '</span>';
	
			// define se o menu esta habilitado
			$data["disabled"] = $disabled;
	
			//$data["helptext"] = "";
	
			// verifica se possui uma acao
			if($handle){
				$data["url"] = $handle;
			}
	
			// verifica se possui algum submenu
			$resultData = self::menuRecursive( $value->id );
	
			if($resultData != ''){
				$data["submenu"]["id"] = $value->id;
				$data["submenu"]["itemdata"] = array($resultData);
			}
	
			$content[] = $data;
        }

        return $content;
    }

    /**
     *  Autentica dados do usuário
     *  Módulo principal
     *  @bypasspermission
     */
    public static function userAuthentication()
    {
        $error	  = null;
        $login    = $_POST["login"];
        $password = $_POST["password"];
        $date     = "'".date("Y-m-d H:i:s")."'";

        if($login != '' && $password != ''){

            // consulta a tabela de usuários do sistema
            $sql = Database::load("SELECT id, login, status, expire_date FROM sys_users WHERE login = '{$login}' AND password = md5('{$password}')");

            if(!is_null($sql->id)){

                if($sql->status == 1){

                    // verifica o expire date

                    if(($sql->expire_date == '0000-00-00 00:00:00') or ($sql->expire_date > date("Y-m-d H:i:s"))){

                        // cria sessao do usuário
                        $_SESSION['system']['user'] = array('id' => $sql->id, 'login' => $sql->login, 'startDate' => date("Y-m-d H:i:s"));

                        //registra na sys_user_log
                        $sqlFields['id_fk_user']            = $sql->id;
                        $sqlFields['date']                  = $date;
                        $sqlFields['remote_addr']           = "'".$_SERVER['REMOTE_ADDR']."'";
                        $sqlFields['http_x_forwaded_for']   = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? "'".$_SERVER['HTTP_X_FORWARDED_FOR']."'" : "''";
                        $sqlFields['user_agent']            = "'".$_SERVER['HTTP_USER_AGENT']."'";

                        Database::insert("sys_user_log", $sqlFields);

                        return true;
                    }else{
                        $error = 'Usuário com prazo expirado para acesso ao sistema';
                    }
                }else{
                    $error = 'Usuário inativo';
                }
            }else{
                $error = 'Usuário não identificado.';
            }
        }else{
            $error = 'Login e senha incorretos.';
        }

        if($error != null){
            // instância classe de erro
            $metaError = New MetaDialog();
            $metaError->code 	= 3; //Error
            $metaError->message = $error;

            return $metaError;
        }
    }

    /**
     *  Recuperar senha de acesso
     *  Módulo principal
     *  @bypasspermission
     */
    public static function recoverPassword()
    {
        $error	  = null;
        $login    = trim($_POST["login"]);
        $date     = "'".date("Y-m-d H:i:s")."'";

        if(!empty($login)){

            // consulta a tabela de usuários do sistema
            $sql = Database::load("SELECT id, email, login FROM sys_users WHERE login = '{$login}'");

            if(isset($sql->email)){

                $newPassword = SysFunctions::createNewPassword(6, true, true, true, false);

                // grava nova senha do usuário
                Database::execution("UPDATE sys_users SET password = md5('{$newPassword}') WHERE id = {$sql->id}");

                $template = new Template();

                $template->assign('login'   , $sql->login);
                $template->assign('password', $newPassword);
                $template->assign('linkSys' , URL_SYS);

                $content = $template->fetch('user/recoverPassword.tpl');

                SysFunctions::sendEmailSystem($sql->email, $content, 'Senha de acesso');
                
            }else{
                $error = 'O e-mail informado não foi localizado.';
            }

        }else{
            $error = 'E-mail incorreto';
        }

        if($error != null){
            // instância classe de erro
            $metaDialog = New MetaDialog();
            $metaDialog->code 	= 3; //Error
            $metaDialog->title 	= '';
            $metaDialog->message = $error;

            return $metaDialog;
        }else{
            // instância classe de erro
            $metaDialog = New MetaDialog();
            $metaDialog->code 	= 1; //Error
            $metaDialog->title 	= '';
            $metaDialog->message = 'Nova senha enviada para o e-mail ' . $sql->email;

            return $metaDialog;
        }
    }
	
	/**
     * Cria um novo Post-it
     * @author Gustavo Silva
     */
    public static function createPostit()
    {
        $id = Database::insert("sys_postit", array('id_user' => $_SESSION['system']['user']['id']));

		$sql = Database::load("SELECT id FROM sys_postit WHERE id_user = {$_SESSION['system']['user']['id']} ORDER BY id DESC LIMIT 1");

        return $sql->id;
    }
    
    /**
     * Retorna os postits do usuário
     * @author Gustavo Silva
     */
    private static function getPostit()
    {
        $postit = array();
        
        $objSql = Database::loadAll("SELECT * FROM sys_postit WHERE id_user = {$_SESSION['system']['user']['id']}");

        foreach($objSql as $value){

            $postit[] = array('id' => $value->id, 'top' => $value->position_top, 'left' => $value->position_left, 'content' => $value->content);

        }

        return json_encode($postit);        
    }

    /**
     * Salva as localizações do shorcut
     * @author Gustavo Silva
     */
    public static function savePostit()
    {
        // busca o nome do postit
        list($class, $id) = explode("-", $_POST['id']);

        $top     = !empty($_POST['top'])     ? $_POST['top']     : null;
        $left    = !empty($_POST['left'])    ? $_POST['left']    : null;
        $content = !empty($_POST['content']) ? $_POST['content'] : null;

        $params = array();

        if(!is_null($top) && !is_null($left)){
            $params['position_top']  = $top;
            $params['position_left'] = $left;
        }

        if(!is_null($content)){
            $params['content'] = $content;
        }

        Database::update("sys_postit", $params, "id = {$id}");

        return true;
    }

	/**
     * Deleta um postit
     * @author Gustavo Silva
     */
    public static function deletePostit()
    {
        // busca o nome do postit
        list($class, $id) = explode("-", $_POST['id']);

        Database::execution("DELETE FROM sys_postit WHERE id = {$id}");

        return true;
    }
}