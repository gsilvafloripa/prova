<?php

/**
 * Classe responsável pelas ações e regras de negócio.
 *
 * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
 * @since 01/02/2013
 * 
**/
class ModelWindow
{
    
    private $editMode = false;
    private $windowId = null;
    private $rowId = null;
    private $winParent = null;
    private $element  = array();
    private $application = null;
    public  $metaResult;
    public  $metaResultSave;
    
    public function __construct()
    {
        $this->metaResult     = new MetaResult();
        $this->metaResultSave = new MetaResultSave(false);
        
        if (isset($_GET['app'])) {
            $this->setApplication($_GET['app']);
        }
        
        if (isset($_POST['app'])) {
            $this->setApplication($_POST['app']);
        }
        
        if (isset($_GET['windowId'])) {
            $this->setWindowId($_GET['windowId']);
        }
        
        if (isset($_POST['windowId'])) {
            $this->setWindowId($_POST['windowId']);
        }
        
        if (isset( $_GET['rowId'])) {
            $this->setEditMode( true );
            $this->setRowId($_GET['rowId']);
        }
        
        if (isset( $_POST['rowId'])) {
            $this->setEditMode( true );
            $this->setRowId($_POST['rowId']);
        }
        
        if (isset( $_GET['idParent'])) {
            $this->setWinParent($_GET['idParent']);
        }
        
        if (isset( $_POST['winParent'])) {
            $this->setWinParent($_POST['winParent']);
        }
        
        $this->metaResultSave->windowId  = $this->getWindowId();
        $this->metaResultSave->winParent = $this->getWinParent();
        $this->metaResultSave->rowId     = $this->getRowId();
        
    }

    public function setElement($element)
    {
        $this->element[] = $element;
    }
    
    public function getElement()
    {
        return implode(' ', $this->element);
    }
    
    public function setWindowId($id)
    {
        $this->windowId = $id;
    }
    
    public function getWindowId()
    {
        return $this->windowId;
    }
    
    public function setRowId($id)
    {
        $this->rowId = $id;
    }
    
    public function getRowId()
    {
        return $this->rowId;
    }
    
    public function setWinParent($id)
    {
        $this->winParent = $id;
    }
    
    public function getWinParent()
    {
        return $this->winParent;
    }
    
    public function setEditMode($action)
    {
        $this->editMode = $action;
    }
    
    public function getEditMode()
    {
        return $this->editMode;
    }
    
    public function setApplication($value)
    {
        $this->application = $value;
    }
    
    public function getApplication()
    {
        return $this->application;
    }
    
}
