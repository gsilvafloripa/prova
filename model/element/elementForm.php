<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class ElementForm
{
    public $windowId    = null;
    public $method      = 'POST';
    public $data        = null;
    public $action      = null;
    private $buttons    = null;

    /**
     *  M�todos construtor
     */
    public function __construct($windowId, $action, $method = 'POST')
    {
        $this->windowId = $windowId;
        $this->action   = $action;
        $this->method   = $method;
    }

    /**
     * Seta os botões para a janela de dados
     */
    public function setButtons($buttons)
    {
        $this->buttons = $buttons;
    }

    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId'   , $this->windowId);
        $template->assign('method'     , $this->method);
        $template->assign('action'     , $this->action);
        $template->assign('name'       , 'form');
        $template->assign('id'         , 'form_'.$this->windowId);
        $template->assign('form'       , $this->data);
        $template->assign('buttons'    , $this->buttons);

        return $template->fetch('element/form.tpl');
    }
}