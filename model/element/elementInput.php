<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */


class ElementInput
{
    public $windowId   = null;
    public $type        = 'text';
    public $name        = null;
    public $label       = null;
    public $value       = null;
    public $disabled    = false;
    public $size        = '20px';
    public $maxlength   = 100;
    public $alt         = '';
    public $onfocus     = null;
    public $onblur      = null;
    public $onselect    = null;
    public $onchange    = null;
    
    /**
     *  M�todos construtor
     */
    public function __construct($type = 'text', $name, $windowId)
    {
        $this->type     = $type;
        $this->name     = $name;
        $this->windowId = $windowId;
    }
    
    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId' , $this->windowId);
        $template->assign('type'     , $this->type);
        $template->assign('name'     , $this->name);
        $template->assign('label'    , $this->label);
        $template->assign('value'    , $this->value);
        $template->assign('disabled' , $this->disabled);
        $template->assign('size'     , $this->size);
        $template->assign('maxlength', $this->maxlength);
        $template->assign('alt'      , $this->alt);

        return $template->fetch('element/input.tpl');
    }

}