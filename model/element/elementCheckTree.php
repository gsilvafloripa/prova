<?php

class ElementCheckTree
{
    public $windowId = null;
    private $data    = null;

    /**
     *  M�todos construtor
     */
    public function __construct($windowId, $data)
    {
        $this->windowId = $windowId;
        $this->data     = $data;
    }
    
    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId' , $this->windowId);
        $template->assign('data'     , $this->data);

        return $template->fetch('element/checkTree.tpl');
    }

}