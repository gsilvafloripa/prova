<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class ElementTextarea
{
    public $windowId    = null;
    public $name        = null;
    public $label       = null;
    public $value       = null;
    public $rows        = 3;
    public $cols        = 25;
    public $disabled    = false;
    public $onfocus     = null;
    public $onblur      = null;
    public $onselect    = null;
    public $onchange    = null;
    public $strLimit    = null;
    
    /**
     *  M�todos construtor
     */
    public function __construct($name, $windowId)
    {
        $this->name     = $name;
        $this->windowId = $windowId;
    }
    
    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId' , $this->windowId);
        $template->assign('name'     , $this->name);
        $template->assign('label'    , $this->label);
        $template->assign('value'    , $this->value);
        $template->assign('rows'     , $this->rows);
        $template->assign('cols'     , $this->cols);
        $template->assign('disabled' , $this->disabled);
        $template->assign('strLimit' , $this->strLimit);

        return $template->fetch('element/textarea.tpl');
    }

}