<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class ElementGrid
{
    private $windowId     = null;
    private $metadata     = null;
    public  $fieldSort    = 'id';
    public  $pageSize     = 10;
    public  $width        = '100%';
    public  $pagination   = true;
    public  $pagLimit     = true;
    public  $contextMenu  = null;
    private  $limit       = 10;
    private  $start       = 0;
    private  $before      = 0;
    private  $after       = 0;
    private  $first       = 0;
    private  $last        = 0;

    /**
     *  M�todos construtor
     */
    public function __construct( $windowId, $metadata )
    {
        $this->windowId = $windowId;
        $this->metadata = $metadata;
        $this->limit    = isset($_GET['limit']) ? $_GET['limit'] : 10;
        $this->start    = isset($_GET['start']) ? $_GET['start'] : 0;
    }

    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('metadata'            , $this->metadata);
        $template->assign('width'               , $this->width);

        $linkPagination = $this->_linkPagination();
        
        //combobox com os limites
        $pagLimit = array(10, 15, 25, 50, 100);

        $template->assign('windowId'            , $this->windowId);
        $template->assign('linkPagination'      , $linkPagination);
        $template->assign('linkPaginationFirst' , $this->first);
        $template->assign('linkPaginationLast'  , $this->last);
        $template->assign('start'               , $this->start);
        $template->assign('limit'               , $this->limit);
        $template->assign('before'              , $this->before);
        $template->assign('after'               , $this->after);
        $template->assign('pagination'          , $this->pagination);
        $template->assign('pagLimit'            , $pagLimit);
        $template->assign('pagStart'            , $this->start);
        $template->assign('pagination'          , $this->pagination);
        $template->assign('contextMenu'         , $this->contextMenu);

        return $template->fetch('element/grid.tpl');
    }

    /**
     *  Monta o link de pagina��o
     */
    private function _linkPagination()
    {
        $amount     = $this->metadata['amount'];

        $amountLink = ceil($amount/$this->limit);

        $positionStart = 0;

        $arrLink = array();

        for($i = 1; $i <= $amountLink; $i++){

            $arrLink[$i] = isset($arrLink[$i-1]) ? $arrLink[$i-1] + $this->limit : 0;

            if($arrLink[$i] == $this->start){
                $positionStart = $i;
            }

        }

        $this->before = $arrLink[$positionStart - 1];
        $this->after  = $arrLink[$positionStart + 1];
        
        $this->first  = ($this->start == $arrLink[1] ? '' : $arrLink[1]);
        $linkPaginationLast = count($arrLink);
        $this->last   = ($this->start == $arrLink[$linkPaginationLast]    ? '' : $arrLink[$linkPaginationLast]);

        //if($amountLink <= 1){
        //    $this->pagination = false;
        //}

        return $arrLink;
    }
}