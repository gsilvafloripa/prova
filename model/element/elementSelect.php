<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */


class ElementSelect
{
    public $windowId      = null;
    public $name          = null;
    public $label         = null;
    public $data          = null;
    public $disabled      = false;
    public $size          = null;
    public $onfocus       = null;
    public $onblur        = null;
    public $onselect      = null;
    public $onchange      = null;
    public $valueSelected = null;
    
    /**
     *  Métodos construtor
     */
    public function __construct($name, $windowId)
    {
        $this->name     = $name;
        $this->windowId = $windowId;
    }
    
    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId'      , $this->windowId);
        $template->assign('name'          , $this->name);
        $template->assign('label'         , $this->label);
        $template->assign('data'          , $this->data);
        $template->assign('disabled'      , $this->disabled);
        $template->assign('size'          , $this->size);
        $template->assign('valueSelected' , $this->valueSelected);

        return $template->fetch('element/select.tpl');
    }

}