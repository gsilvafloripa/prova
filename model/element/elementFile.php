<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class ElementFile
{
    public $windowId   = null;
    public $name        = null;
    public $label       = null;
    public $width       = null;
    public $height      = null;
    public $value       = null;
    public $disabled    = false;
    public $alt         = '';
    public $message     = '';

    /**
     *  Método construtor
     */
    public function __construct( $name, $windowId)
    {
        $this->name     = $name;
        $this->windowId = $windowId;
    }

    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId' , $this->windowId);
        $template->assign('name'     , $this->name);
        $template->assign('label'    , $this->label);
        $template->assign('width'    , $this->width);
        $template->assign('height'   , $this->height);
        $template->assign('value'    , $this->value);
        $template->assign('message'  , $this->message);
        $template->assign('disabled' , $this->disabled);
        $template->assign('alt'      , $this->alt);

        return $template->fetch('element/file.tpl');
    }

}