<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class ElementFieldset
{
    public $legend  = null;
    public $value   = null;
    
    /**
     *  M�todos construtor
     */
    public function __construct($legend, $value)
    {
        $this->legend = $legend;
        $this->value  = $value;
    }
    
    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('legend' , $this->legend);
        $template->assign('value'  , $this->value);

        return $template->fetch('element/fieldset.tpl');
    }

}