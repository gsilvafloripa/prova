<?php

class ElementTreeView
{
    public  $windowId = null;
    private $data    = null;
    public  $elRender = null;

    /**
     *  MÈtodos construtor
     */
    public function __construct($windowId, $data)
    {
        $this->windowId = $windowId;
        $this->data     = $this->normalizeData($data);
    }
    
    /**
     * Normaliza os dados para transformar em json
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @param array('text', 'checkbox' => array('checked', 'value', 'name'), 'expanded', 'chieldren')
     */
    public function normalizeData($data = array())
    {
        $return = array();

        foreach($data as $value){

            // verifica se é o tipo checkbox
            if(isset($value['checkbox']) and count($value['checkbox'])){

                $checked = $value['checkbox']['checked'] == true ? "checked='checked'" : '';

                $html = "<input type='checkbox' {$checked} id='{$value['checkbox']['name']}_{$this->windowId}' name='{$value['checkbox']['name']}' value='{$value['checkbox']['value']}'><label for='{$value['checkbox']['name']}_{$this->windowId}'>{$value['text']}</label>";

            }else{

                $html = $value['text'];

            }

            $return[] = array('type' => 'HTML', 'html' => $html, 'expanded' => $value['expanded'], 'children' => $this->normalizeData($value['chieldren']));

        }

        return $return;

    }
    
    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $this->elRender = $this->elRender != null ? $this->elRender : $this->windowId;

        $template->assign('windowId' , $this->windowId);
        $template->assign('elRender' , $this->elRender);
        $template->assign('data'     , json_encode($this->data));

        return $template->fetch('element/treeView.tpl');
    }

}