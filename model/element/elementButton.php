<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

class ElementButton
{
    public  $windowId = null;
    private $buttons  = array();

    /**
     * Método construtor
     * @param $windowId
     * @param $buttons array :
     */
    public function __construct($windowId)
    {
        $this->windowId = $windowId;
    }

    /**
     * Seta os tipos de botões
     * @param $label
     * @param $icon
     * @param $type (submit, cancel, previous)
     * @param $class
     * @param $onClick
     */
    public function setButtom($label, $icon, $type, $class = '', $messageConfirm = '', $action='')
    {
        $this->buttons[] = array('label' => $label, 'icon' => $icon, 'type' => $type, 'class' => $class, 'messageConfirm' => $messageConfirm, 'action' => $action);
    }

    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId' , $this->windowId);
        $template->assign('buttons'  , $this->buttons);

        return $template->fetch('element/button.tpl');
    }
}