<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */


class ElementCalendar
{
    public $windowId   = null;
    public $name        = null;
    public $label       = null;
    public $value       = null;
    public $onfocus     = null;
    public $onblur      = null;
    public $onselect    = null;
    public $onchange    = null;

    /**
     *  M�todos construtor
     */
    public function __construct($name, $windowId)
    {
        $this->name     = $name;
        $this->windowId = $windowId;
    }

    public function _hour($value = '')
    {
        $return = array();

        for($i=0; $i <= 23; $i++){
            $return[] = array('value' => str_pad($i, 2, "0", STR_PAD_LEFT), 'label' => str_pad($i, 2, "0", STR_PAD_LEFT));
        }

        return $return;
    }

    public function _minute($value = '')
    {
        $return = array();

        for($i=0; $i <= 59; $i++){
            $return[] = array('value' => str_pad($i, 2, "0", STR_PAD_LEFT), 'label' => str_pad($i, 2, "0", STR_PAD_LEFT));
        }

        return $return;
    }

    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $selectedHour           = '';
        $selectedMinute         = '';
        $selectedDate           = '';
        $selectedDateDatabase   = '';
        
        // verifica se é uma data válida
        if($this->value != '' and $this->value != '' and SysFunctions::sysCheckDate($this->value) == true){
            $selectedDate         = date("d/m/Y", strtotime($this->value));
            $selectedHour         = date("H"    , strtotime($this->value));
            $selectedMinute       = date("i"    , strtotime($this->value));
            $selectedDateDatabase = date("Y-m-d H:i", strtotime($this->value));
        }

        $template->assign('windowId'                , $this->name."_".$this->windowId);
        $template->assign('name'                    , $this->name);
        $template->assign('value'                   , $this->value);
        $template->assign('label'                   , $this->label);
        $template->assign('selectedDate'            , $selectedDate);
        $template->assign('selectedHour'            , $selectedHour);
        $template->assign('selectedMinute'          , $selectedMinute);
        $template->assign('selectedDateDatabase'    , $selectedDateDatabase);
        $template->assign('calendarHour'            , $this->_hour());
        $template->assign('calendarMinute'          , $this->_minute());

        return $template->fetch('element/calendar.tpl');
    }

}