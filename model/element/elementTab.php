<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */


class ElementTab
{
    public $windowId = null;
    public $name     = null;
    public $selected = 0;
    private $data    = array();

    /**
     *  Métodos construtor
     */
    public function __construct($name, $windowId)
    {
        $this->name     = $name;
        $this->windowId = $windowId;
    }

    /**
     * Adiciona nova tab
     * @param $label
     * @param $data
     */
    public function addTab($label, $data)
    {
        $this->data[] = array('label' => $label, 'data' => $data);
    }

    /**
     * Renderiza o html
     */
    public function toHtml()
    {
        $template = new Template();

        $template->assign('windowId' , $this->windowId);
        $template->assign('name'     , $this->name.'_'.$this->windowId);
        $template->assign('data'     , $this->data);
        $template->assign('selected' , $this->selected);

        return $template->fetch('element/tab.tpl');
    }

}
