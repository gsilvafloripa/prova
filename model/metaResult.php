<?php
/**
 * @author Gustavo Silva
 * @since 27/03/2011
 */

class MetaResult
{
    public $success      = true;
    
    public $content       = '';
    
    public $windowClose   = true;
    
    public $redirect;
    
    public $windowId      = null;
    
    public $winParent     = null;
    
    public $rowId         = null;
    
    public $display;
    
    public $message;
    
    public $code;
    
    public $messageType;
    
    public $messageTimer  = 8000;
    
	/**
     * 
     * Enter description here ...
     * @param unknown_type $msg
     * @param unknown_type $code
     * @param unknown_type $display
     * @param unknown_type $closeWindow
     */
    public function setMessage( $msg, $code = 0, $display = 'modal', $closeWindow = true ) {
        
        $this->message = $msg;
        $this->code    = $code;
        $this->display = $display;
        $this->windowClose = $closeWindow;
    }
    
}


/**
 * Classe padrão para retorno de resutados de cadastros em geral.
 * Exibe notify padrão para "sucesso" e "erro"
 *
 * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
 * @since 11/01/2013
**/
class MetaResultSave extends MetaResult {
    
    private $saved = false;
    
    /**
     * Construtor verifica se deve exibir o notify de sucesso ou a modal de erro.
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @since 11/01/2013
     *
     * @param boolean   $saved      Resultado do sql, se salvou ou ocorreu erro.
     * @param string    $msgError   Mensagem a ser exibida na modal de erro.
    **/
    public function __construct( $_saved, $msg = '' ) {
        
        $this->saved    = $_saved;
        $this->message  = $msg;
        
    }
    
    /**
     * 
     * Enter description here ...
     */
    public function setMetaResultSave() {
        
        $this->messageTimer = 8000;
        
        if( $this->saved ) {
            
            $this->message = empty( $this->message ) ? 'Salvo com sucesso.' : $this->message;
            
            $this->success      = true;
            $this->display      = 'notify';
            $this->messageType  = 'success';
            
            
        } else {
            
            $errorMessage = 'Não foi possível salvar os dados.';
            
            if($this->code != 0 and $this->message != ''){
                
                $this->message = utf8_encode("[{$this->code}] {$this->message} <br/> {$errorMessage}");
                
            }
            
            $this->success = false;
            
            if( $this->display == '' ) {
                $this->display = 'modal';
            }
            
        }
        
    }
    
    public function setSaved( $value ) {
        $this->saved = $value;
    }
    
}

