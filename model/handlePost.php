<?php

class HandlePost{

    private $fieldPost     = array();
    private $fieldIgnore   = array();
    private $dataField     = array();
    private $metadata      = array();
    private $message       = array();
    private $processedData = array();

    /**
     * M�todo construtor da classe
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    public function __construct($fieldPost, $fieldIgnore = array())
    {
        $this->fieldPost   = $fieldPost;
        $this->fieldIgnore = $fieldIgnore;
    }
    
    /**
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    public function main()
    {
        $this->setFields();

        return $this->process();
    }

    /**
     * Monta os metadados do post
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    private function setFields()
    {
		// monta o metadado o POST
		foreach($this->fieldPost as $key => $data){

			// verifica se o campo � para ser ignorado
			if(in_array($key, $this->fieldIgnore)) continue;

			$this->dataField[$key] = array('name' => $key, 'value' => $data);

		}
    }
    
    /**
     * Seta os metadados dos campos que ser�o validados
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    public function setMetadata($data = array())
    {
        $this->metadata = $data;
    }

    /**
     * Processa os campos relacionados
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    private function process()
    {
        $return     = true;
        $countError = 0;

		// verifica as 
		foreach($this->dataField as $data){

			if(array_key_exists($data['name'], $this->metadata)){

				$metadata = $this->metadata[$data['name']];

				// verifica se � campos obrigat�rio
				if($metadata['mandatory'] == true || $data['value'] != ''){

                    // valida os dados do campo
                    $return = $this->validateData($metadata['function'], $data['value']);

					if($return == false){

                        $this->setMessage($metadata['msgError']);
                        $countError++;

                    }

				}

                // remonta os dados processados
                $arrDataProcessed                       = array();
                $arrDataProcessed['label']              = $metadata['label'];
                $arrDataProcessed['status']             = $return;
                $arrDataProcessed['value']              = $data['value'];
                $arrDataProcessed['valueNormalized']    = $this->normalizeData($metadata['dataType'], $data['value']);
                $arrDataProcessed['msgError']           = $metadata['msgError'];

				// n�o � campo obrigat�rio se foi preenchi tem que ver se tem valida��o
                $this->processedData[$data['name']] = $arrDataProcessed;

			}

		}

		return ($countError > 0) ? false : true;
    }

    /**
     * seta mensagem de erro
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    public function setMessage($message)
    {
        $this->message[] = $message;
    }

    /**
     * Retorna as mensagens de erro
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    public function getMessage()
    {
        return SysFunctions::formatMessageDialog($this->message);
    }

    /**
     * Retorna os dados detalahos ap�s o processamento
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    public function getProcessedData()
    {
        return $this->processedData;
    }

    /**
     * Valida o dado dos campos
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    private function validateData($function, $data)
    {
        $return = true;

        switch($function){

            // N�o pode ser vazio
            case 'NOTEMPTY' : $return = ($data == '') ? false : true; break;

            // N�o pode ser menor que zero
            case 'SUPERIORZERO' : $return = ($data <= 0) ? false : true; break;

            // Valida e-mail
            case 'EMAIL' : $return = SysFunctions::validateEmail($data) ; break;

            // valida o campo data
            case 'FUTURE_DATE': $return = SysFunctions::sysCheckDate($data); break;
                
            // valida o campo hora
            case 'HOUR': $return = SysFunctions::validateHour($data); break;

        }

        return $return;
    }

    /**
     * Normaliza os dados
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     */
    private function normalizeData($type, $data)
    {
        $return = null;
        
        switch($type){

            // Normaliza data com ano-mes-dia
            case 'date_ymd' : $return = date("Y-m-d", strtotime($data)); break;

        }

        return $return;
    }
    
}