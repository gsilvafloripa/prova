<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

require_once( PATH_LIB_SMARTY . 'libs/Smarty.class.php');

class Template extends Smarty
{
    public function __construct($debugging = false, $compileCheck = true)
    {
        $this->template_dir    = '../view/';
        $this->compile_dir     = '../lib/smarty/templates_c/';
        $this->debugging       = $debugging;
        $this->compile_check   = $compileCheck;
        $this->left_delimiter  = '<@';
        $this->right_delimiter = '@>';
    }
}