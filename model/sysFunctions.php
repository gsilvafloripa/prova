<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */
require_once(PATH_LIB . 'phpmailer/class.phpmailer.php');

class SysFunctions
{
    
    /**
     * Campos do form em colunas
     * @param <array> $data
     * @return <html> 
     */
    public static function fieldsInColumns($data)
    {
        $fields       = array();
        $higherColumn = array(); 

        foreach($data as $value){
            $fields[$value['line']][] = $value['data'];
        }

        foreach($fields as $key => $value){
            $higherColumn[] = count($value);
        }

        rsort($higherColumn);

        $higherColumn = $higherColumn[0];
        
        $template = new Template();
        
        $template->assign('fields'      , $fields);
        $template->assign('higherColumn', $higherColumn);

        return $template->fetch('element/fieldsInColumns.tpl');
    }

    /**
     * Validação de e-mail
     * @param <string> $email
     * @return <boolean> 
     */
    public static function validateEmail($email)
    {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Formata menssagem para o metaDialog
     */
    public static function formatMessageDialog($message)
    {
        $return = '';
        
        foreach ($message as $data){
            $return .= " - " . $data . "<br>";
        }

        return $return;
    }

    /**
     * Função para verifica se uma data é válida
     * @param $value DATE
     * @return boolean
     */
    public static function sysCheckDate($value)
    {
        if(checkdate(date("m",strtotime($value)), date("d",strtotime($value)), date("Y",strtotime($value)))){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Remove caracteres especiais
     * @param <type> $string
     * @return <type>
     */
    public function removeSpecialCaracters($string, $charset = "UTF-8")
    {
		$keys = array(
            "A" => "/À|Á|Â|Ã|Ä|Å/",
            "a" => "/à|á|â|ã|ä|å/",
            "C" => "/Ç/",
            "c" => "/ç/",
            "E" => "/È|É|Ê|Ë/",
            "e" => "/è|é|ê|ë/",
            "I" => "/Ì|Í|Î|Ï/",
            "i" => "/ì|í|î|ï/",
            "N" => "/Ñ/",
            "n" => "/ñ/",
            "O" => "/Ò|Ó|Ô|Õ|Ö/",
            "o" => "/ò|ó|ô|õ|ö/",
            "U" => "/Ù|Ú|Û|Ü/",
            "u" => "/ù|ú|û|ü/",
            "Y" => "/Ý/",
            "y" => "/ý|ÿ/",
            "a." => "/ª/",
            "o." => "/º/",
			"" => "/-/",
			"" => "/&/",
			"" => "/%/");

        //(htmlentities($string, ENT_NOQUOTES, $charset));
		return preg_replace($keys,array_keys($keys), $string);
    }

    /**
     *  Monta menu recursivo
     *  @param $idParent
     */
    public function menuRecursive( $idParent = 0, $i= 0 )
    {
        $return = array(0 => "Raiz");

        // consulta dados do menu
        $menu = Database::loadAll("SELECT * FROM web_menu WHERE id_parent = {$idParent} AND status = 1");

        if($idParent == 0){
            $return[0] = "";
        }

        foreach($menu as $key=>$value){

            for ($c = 0; $c < $i; $c++) {
                $t = $t . "---";
            }

            $return[$value->id] = "---". $t . " " . $value->label;

            $r = self::menuRecursive($value->id, $i + 1);

            if(is_array($r)){
                foreach($r as $key => $data){
                    $return[$key] = $data;
                }
            }
        }

        return $return;
    }

    /**
     * Envia e-mail
     * @param <type> $phpMailer
     * @param <type> $content
     * @param <type> $subject
     * @param <type> $from
     * @param <type> $fromName
     *
     */
    public static function sendEmailSystem($email, $content, $subject, $name ="")
    {

        // consulta dados de configuração do SMTP
        $sqlConfig = Database::load("SELECT * FROM sys_account_email WHERE location = 2");

        $phpMailer = new PHPMailer();

        $phpMailer->IsSMTP();
        $phpMailer->Host        = $sqlConfig->host;       //"mail.agilecom.com.br";
        $phpMailer->SMTPAuth    = true;
		$phpMailer->SMTPSecure  = ($sqlConfig->use_ssl ? 'ssl' : '');
		$phpMailer->Port 		= ($sqlConfig->port ? $sqlConfig->port : '');
        $phpMailer->Username    = $sqlConfig->user_name;  //"sysfacilsc@agilecom.com.br";
        $phpMailer->Password    = $sqlConfig->password;   //"a32026";
        $phpMailer->From        = $sqlConfig->email_from; //"sysfacilsc@agilecom.com.br";
        $phpMailer->FromName    = $sqlConfig->from_name;  //"Fácil sc";
        $phpMailer->CharSet     = "UTF-8";

        $sendFor = explode(",", $sqlConfig->send_for);

        $phpMailer->AddAddress($email, $name);

        $phpMailer->WordWrap    = 50; // Definição de quebra de linha
        $phpMailer->IsHTML(true);
        $phpMailer->Subject     = $subject;
        $phpMailer->Body        = $content;
        $phpMailer->AltBody     = $content;

        return $phpMailer->Send();
    }

    /**
     * Gerar senha de acesso
     * "Senha gerada: ".gerar_senha(8, true, false, false, true)."\n";
     */
    function createNewPassword($tamanho, $maiuscula, $minuscula, $numeros, $codigos)
    {
        $maius = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
        $minus = "abcdefghijklmnopqrstuwxyz";
        $numer = "0123456789";
        $codig = '!@#$%&*()-+.,;?{[}]^><:|';

        $base = '';
        $base .= ($maiuscula) ? $maius : '';
        $base .= ($minuscula) ? $minus : '';
        $base .= ($numeros) ? $numer : '';
        $base .= ($codigos) ? $codig : '';

        srand((float) microtime() * 10000000);

        $senha = '';

        for ($i = 0; $i < $tamanho; $i++) {
            $senha .= substr($base, rand(0, strlen($base)-1), 1);
        }
        return $senha;
    }
    
    /**
     * Transforma minutos em horas ou se inferior a uma hora deixa em minutos
     * @author Gustavo Silva
     * @return string
     */
    public function minutesForHour($mins) {

	// Se os minutos estiverem negativos
        if ($mins < 0)
            $min = abs($mins);
        else
            $min = $mins;
 
        // Arredonda a hora
        $h = floor($min / 60);
        $m = ($min - ($h * 60)) / 100;
        $horas = $h + $m;
 
        // Matemática da quinta série
        // Detalhe: Aqui também pode se usar o abs()
        if ($mins < 0)
            $horas *= -1;
 
        // Separa a hora dos minutos
        $sep = explode('.', $horas);
        $h = $sep[0];
        if (empty($sep[1]))
            $sep[1] = 00;
 
        $m = $sep[1];
 
        // Aqui um pequeno artifício pra colocar um zero no final
        if (strlen($m) < 2)
            $m = $m . 0;
 
        return sprintf('%02d:%02d:00', $h, $m);
    }
	
	public static function createButtonNewWindow($label, $url, $title, $icon, $width, $height, $modal)
	{
		$json = "{'url':'$url', 'title':'$title', 'icon':'$icon', 'width':$width, 'height':$height, 'modal':$modal}";
        $handle = "javascript:Window().create({$json})";
		return '<span style="cursor:pointer" onclick="'.$handle.'"><img src="/image/icon/'.$icon.'.png">'.$label. '</span>';
	}
	
	public static function curl($url, $method, $data)
	{
		// define o curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json'                                                                      
        ));       
        
        // executuca o request
        $output = curl_exec($ch);
        
        // define o output
        return json_decode($output);
	}
   
}