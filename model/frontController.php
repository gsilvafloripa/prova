<?php
/**
 * @author Gustavo Silva
 * @since 01/01/2011
 */

class FrontController
{
    private $m          = null;
    private $c          = null;
    private $connection = true;

    public function __construct()
    {
        //Classe
        $this->c = 'index';
        //Módulo
        $this->m = 'index';

        if((isset($_GET['c']) and $_GET['c'] != 'public') and ($_GET['m'] != 'index')){

            $this->c = $_GET['c'];
            $this->m = $_GET['m'];

        }

        // métodos index e userAuthentication não necessitam de sessão
        if(($this->c != 'index' and $this->m != 'userAuthentication') and ($this->c != 'index' and $this->m != 'index')){
            // verifica se possui sessão
            if(!isset($_SESSION['system']['user']['id'])){
                
                $this->connection = false;
                
                $typeRequest = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) : null;

                switch( $typeRequest ){

                    case 'xmlhttprequest' :
                        
                        $json['result']['response']['dialog']['code']    = $objReturn->code;
                        $json['result']['response']['dialog']['message'] = $objReturn->message;
                        $json['result']['response']['dialog']['title']   = $objReturn->title;

                        $return = json_encode($json);
                        break;
                        
                    default : 
                        
                        header('location: /');
                        die();
                        
                }
                
            }
        }

    }

    public function __autoload($class)
    {
        require_once($class);
    }
    
    public function run()
    {
        if(isset($_SESSION['system']['user']['id'])){
            // registra o log da applição
            $sqlFields['date_created']      = "'".date("Y-m-d H:i:s")."'";
            $sqlFields['id_fk_user']        = $_SESSION['system']['user']['id'];
            $sqlFields['request_uri']       = "'".$_SERVER["REQUEST_URI"]."'";

            Database::insert("sys_log_app", $sqlFields);
        }

        $this->c = 'controller' . ucwords($this->c);
        
        //Caminho do módulo desejado
        $path = PATH_CONTROLLER . $this->c . ".php";

        $return = null;

        if(file_exists($path)){

            $this->__autoload($path);

            //Reflexiona a classe
            $reflectionClass = new ReflectionClass($this->c);

            //Verifica se o método existe
            if($reflectionClass->hasMethod($this->m) == true){

                //Reflexiona o métodos
                $reflectionMethod = new ReflectionMethod($this->c, $this->m);

                //Verifica se o módulo possui permiss�o para ser executado
                //$er = "@bypasspermission";

                //if(ereg($er, $reflectionMethod->getDocComment())){
                    
                    // Tratamento para retorno antigo com classe estáticas
                    if($reflectionMethod->isStatic() == true){
                        
                        $return = $reflectionMethod->invoke(NULL);
                        
                    }else{
                        
                        // Novo método de retorno
                        $class = new $this->c();
                        $method = $this->m;
                        $return = $class->$method();
                        
                    }

                    //TODO::
                    echo $this->processReturn($return);

                //}else{
                //     throw new Exception('Sem permiss�o para acessar!');
                //}
            }else{
                throw new Exception('URL inválida! (módulo)');
            }
        }else{
            throw new Exception('URL inválida!');
        }        
    }
    
    private function processReturn($objReturn)
    {
        $return = '';
        
        $json['result'] = array('response' => array('idContent' => isset($_GET['idContent']) ? $_GET['idContent'] : null,
                                                    'content'   => null,
                                                    'connection'=> $this->connection,
                                                    'dialog'    => array('code'       => null,
                                                                         'message'    => null,
                                                                         'url'        => null,
                                                                         'urlRequest' => null)));

        //Verifica permissões de acesso
        $getClass = is_object($objReturn) ? get_class($objReturn) : null;

        switch($getClass){

                case 'MetaDialog' :
                    $json['result']['response']['dialog']['code']      = $objReturn->code;
                    $json['result']['response']['dialog']['message']   = $objReturn->message;
                    $json['result']['response']['dialog']['title']     = $objReturn->title;
                    $json['result']['response']['dialog']['winParent'] = $objReturn->winParent;

                    $return = json_encode($json);
                    break;
                case 'MetaResult' :
                    $return = json_encode($objReturn);
                    break;
                case 'MetaResultSave':
                    $return = json_encode($objReturn);
                    break;
                default:
                    $typeRequest = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) : null;

                    switch( $typeRequest ){

                        case 'xmlhttprequest' :

                            //Verifica se o retorno é json
                            //$jsonReturn = json_decode($objReturn);

                            if(isset($_GET['formatJson']) && $_GET['formatJson'] == true){
                                $return = $objReturn;
                                break;
                            }

                            if(is_array($objReturn)){
                                $objReturn = json_encode($objReturn);
                            }

                            json_decode($objReturn);

                            //Verifica se no objeto existe o result
                            if(is_object($objReturn)){
                                $return = $objReturn;
                            }else{
                                $json['result']['response']['content'] =  $objReturn;
                                $return = json_encode($json);
                            }

                            break;
                        default :
                            
                            if($this->connection == false){
                                // redireciona para página de authenticação
                                header('location: /');
                            }else{                            
                                print $objReturn;
                            }
                    }					
        }

        return $return;
    }
}