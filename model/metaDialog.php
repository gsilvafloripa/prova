<?php
/**
 * @author Gustavo Silva
 * @since 27/03/2011
 */

class MetaDialog
{
    public $code      = null;

    public $title     = '';

    public $message   = null;

    public $url       = null;
    
    public $winParent = null;

}

