<?php
/**
 * @author Gustavo Silva <gsilva.floripa@gmail.com>
 * @since 03/06/2015
 */
class Api
{
    private $data = null;
    private $method = null;
    private $rowId = null;
    private $dataReturn = array();
    
    /**
     *
     */
    public function __construct() {}
    
    /**
     * Método responsável por direcionar o tipo de execução
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    public function handle($method)
    {
        switch ($method) {
            case 'GET':
                $this->get();
                break;
            case 'PUT':
                $this->put();
                break;
            case 'DELETE':
                $this->delete();
                break;
        }
    }
    
    /**
     * Método responsável por setar os dados de retorno
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @param $data array
     * @return void
     */
    protected function setDataReturn($data, $sucess = true, $msg = '')
    {
        $this->dataReturn = array('sucess' => $sucess,
                                  'msg' => $msg,
                                  'data' => $data
                                  );
    }
    
    /**
     * Método responsável por buscar os dados de retorno
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return array
     */
    public function getDataReturn()
    {
        return $this->dataReturn;
    }
    
    /**
     * Método responsável por setar os dados da variável data
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    public function setData($data)
    {
        $newData = array();
        
        // transforma os dados para array
        foreach ($data as $key => $value) {
            $newData[$key] = $value;
        }
        
        $this->data = $newData;
    }
    
    /**
     * Método responsável por buscar os dados da variável data
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return string | array | json
     */
    public function getData()
    {
       return $this->data;
    }
    
    /**
     * Método responsável por setar os dados da variável rowId
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    public function setRowId($rowId)
    {
        $this->rowId = $rowId;
    }
    
    /**
     * Método responsável por buscar os dados da variável rowId
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return string | array | json
     */
    public function getRowId()
    {
         return $this->rowId;
    }
    
}