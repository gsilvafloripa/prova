<?php
class ExceptionApiRest extends Exception
{
    
    // Redefine a exceção de forma que a mensagem não seja opcional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // garante que tudo está corretamente inicializado
        parent::__construct($message, $code, $previous);
    }
}

/**
 * Classe responsável por fazer o roteamente das classe da API
 *
 * @author Gustavo Silva<gsilva.floripa@gmail.com>
 * @since 03/06/2015
 */
class ApiRest
{
    private $classExec = '';
    private $rowId = '';
    // método referente a requisição HTTP
    private $method = '';
    private $token = '';
    private $data = '';
    private $status = 200;
    
    /**
     *
     *
     */
    public function __construct()
    {
        $this->_getMethod();
        
        $this->_setParams();
    }
    
    /**
     * Método responsável por identificar o tipo de requisição
     * Vou trabalhar somente com GET, PUT e DELETE
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    private function _getMethod()
    {
        $method = $_SERVER['REQUEST_METHOD'];
		$override = isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']) ? $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'] : (isset($_GET['method']) ? $_GET['method'] : '');
		
        if ($method == 'POST' && strtoupper($override) == 'PUT') {
			$method = 'PUT';
		} elseif ($method == 'POST' && strtoupper($override) == 'DELETE') {
			$method = 'DELETE';
		}
        
		$this->method = $method;
    }
    
    /**
     * Método responsável por setar os parametros de validação e execução
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    private function _setParams()
    {
        // o .htccess já encarregou de definir por padrão esse GET
        $this->classExec = $_GET['c'];
        // o .htccess já encarregou de definir por padrão esse GET
        $this->rowId = $_GET['id'];
        // verifica e seta o token
        $this->token = isset($_GET['token']) ?  $_GET['token'] : '';
    }
    
    /**
     * Método responsável por executar as regras de negócio
     * para execução dos metodo solicitados
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    public function run()
    {
        $jsonReturn = array();
        $dataReturn = array();
        
        try {
            // verifica validação do token
            if ($this->_authTocken()) {
                // instancia a classe para buscar os dados
                $dataReturn = $this->_setClassMethod($this->classExec);
            } else {
                throw new ExceptionApiRest ('Token inválido', 401);
            }
        } catch (ExceptionApiRest $e) {
            //
            $this->status = $e->getCode();
        }
        
        // monta o array de retorno
        $jsonReturn = array('status' => array('code' => $this->status,
                                              'text' => $this->getCodeError($this->status)),
                            'return' => $dataReturn
                            );
        
        echo(json_encode($jsonReturn));
        
    }
    
    /**
     * Método responsável por instanciar a classe e chamar o método
     * para executar as ações
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @param $class
     * @return void
     */
    private function _setClassMethod($class)
    {
        // pasta da classe
        $folderClass = $class;
        
        // define o nome da classe acrestando o prefixo 'Api'
        $class = 'Api' . ucwords($class);
        
        // Caminho do módulo desejado
        $path = PATH_API . $folderClass . '/' . $class . ".php";

        //$return = null;

        if (file_exists($path)) {

            require_once($path);

            // reflexiona a classe
            $reflectionClass = new ReflectionClass($class);

            // verifica se o método existe
            if ($reflectionClass->hasMethod('handle') == true) {

                // instancia a classe
                $obj = new $class();
                
                $data = file_get_contents('php://input');
                
                $obj->setData(json_decode($data));
                $obj->setRowId($this->rowId);
                
                // invoca o método handle passando o tipo de método GET | PUT | DELETE
                $obj->handle($this->method);
                
                // retorna os dados processados
                return $obj->getDataReturn();
                
                
            } else {
                throw new ExceptionApiRest('Método inválido', 501);
            }
        } else {
            throw new ExceptionApiRest('Classe inválida', 501);
        }
    }
    
    /**
     * Método responsável por authenticar e validar o token
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @param $token string
     * @return boolean
     */
    private function _authTocken()
    {
        $return = true;
        
        //@TODO:: verificar no bando
        if ($this->token!=TOKEN_API) {
            $return = false;
        }
        
        return $return;
    }
    
    /**
     * Mostra a linha de código possíveis
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @param $code
     * @return array
     */
    private function getCodeError($code)
    {
        $arrCode = array(
            '100' => 'Continue',
            '200' => 'OK',
            '201' => 'Created',
            '202' => 'Accepted',
            '203' => 'Non-Authoritative Information',
            '204' => 'No Content',
            '205' => 'Reset Content',
            '206' => 'Partial Content',
            '300' => 'Multiple Choices',
            '301' => 'Moved Permanently',
            '302' => 'Found',
            '303' => 'See Other',
            '304' => 'Not Modified',
            '305' => 'Use Proxy',
            '307' => 'Temporary Redirect',
            '400' => 'Bad Request',
            '401' => 'Unauthorized',
            '402' => 'Payment Required',
            '403' => 'Forbidden',
            '404' => 'Not Found',
            '405' => 'Method Not Allowed',
            '406' => 'Not Acceptable',
            '409' => 'Conflict',
            '410' => 'Gone',
            '411' => 'Length Required',
            '412' => 'Precondition Failed',
            '413' => 'Request Entity Too Large',
            '414' => 'Request-URI Too Long',
            '415' => 'Unsupported Media Type',
            '416' => 'Requested Range Not Satisfiable',
            '417' => 'Expectation Failed',
            '500' => 'Internal Server Error',
            '501' => 'Not Implemented',
            '503' => 'Service Unavailable'
        );
        
        return $arrCode[$code];
    }
    
}

