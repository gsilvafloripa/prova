<?php
require_once (PATH_API . 'api.php');

/**
 * Classe responsável pelos métodos do gereniador de tarefas
 *
 * @author Gustavo Silva <gsilva.floripa@gmail.com>
 * @since 03/06/2015
 */
class ApiTaskmanager extends Api
 {
    private $sucess = true;
    private $msg = '';
    private $array = array();
    
    /**
     * Método responsávael por listar um ou mais dados
     *
     * @author Gustava Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    public function get()
    {
        try {
            if ($this->getRowId()!=null) {
                // consulta por um determinado usuário
                $sql = Database::load("SELECT id, title, description, priority FROM sys_taskmanager WHERE id = {$this->getRowId()}");
                //
                $this->array = $sql;
            } else {
                // consulta por todos os registros
                $sql = Database::loadAll("SELECT id, title, description, priority FROM sys_taskmanager ORDER BY priority DESC");
                //
                $this->array = $sql;
            }
        } catch (PDOException $e) {
            $this->sucess = false;
            $this->msg = $e->getMessage();
        }
        
        $this->setDataReturn($this->array, $this->sucess, $this->msg);
    }
    
    /**
     * Método responsávael por criar ou atualizar um registro
     *
     * @author Gustava Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    public function put()
    {
        $data = $this->getData();
        
        try {
            if ($this->getRowId()!=null) {
                // atualiza os dados
                $id = Database::update('sys_taskmanager', $data, "id = {$this->getRowId()}");
            } else {
                // inseri um novo registro
                $id = Database::insertFull('sys_taskmanager', $data, false);
                //retorn o id do novo registro
                $this->array = array('id' => $id);
            }
        } catch (PDOException $e){
            $this->sucess = false;
            $this->msg = $e->getMessage();
        }
        
        $this->setDataReturn($this->array, $this->sucess, $this->msg);
    }
    
    /**
     * Método responsávael por deletar um registro
     *
     * @author Gustava Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return void
     */
    public function delete()
    {
        try {
            if ($this->getRowId()!=null) {
                // atualiza os dados
                Database::execution("DELETE FROM sys_taskmanager WHERE id = {$this->getRowId()}");
            } else {
                throw new PDOException('Método inválido');
            }
        } catch (PDOException $e){
            $this->sucess = false;
            $this->msg = $e->getMessage();
        }
        
        $this->setDataReturn($this->array, $this->sucess, $this->msg);
    }
    
 }
 