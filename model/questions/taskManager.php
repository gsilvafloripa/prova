<?php


class ModelTaskManager
{

    /**
     * Método responsável por salvar os dados
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @param $data
     * @param $rowId
     * @return boolean
     */
    public function save($data, $rowId=null)
    {
        // url definida para inserção
        $url = sprintf(URL_TOKEN, "taskmanager");
        
        if($rowId!=null){
            // define o método passando o id para editar
            $url = sprintf(URL_TOKEN, "taskmanager/{$rowId}");
        }
        
        if (!is_int((int) $_POST['priority'])){
            throw new Exception("Campo prioridade inválido");
        }
        
        // define o dados a serem passados para a REST Api
        $data = array('title' => $_POST['title'],
                      'description' => $_POST['description'],
                      'priority' =>  $_POST['priority']
                      );
        
        // executa o CURL
        $response = SysFunctions::curl($url, 'PUT', $data);
        
        if($response->status->code != 200 or $response->return->sucess != true){
            throw new Exception("Não foi possível salvar os dados [{$response->status->text}]");
        }
    }
    
    /**
     * Método responsável por listar as tarefas por prioridade
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @return array
     */
    public function listDataByPriority()
    {
        // define o método passando o id para editar
        $url = sprintf(URL_TOKEN, "taskmanager/");
        
        // executa o CURL
        $response = SysFunctions::curl($url, 'GET', array());
        
        if($response->status->code != 200 or $response->return->sucess != true){
            throw new Exception("Não foi possível buscar os dados [{$response->status->text}]");
        }
        
        // define os dados de retorno
        $arrResponse =  $response->return->data;
        $return = array();
        
        foreach ($arrResponse as $obj) {
            $return[] = (array) $obj;
        }
        
        return $return;
    }
    
    /**
     * Método responsável por buscar uma tarefa específica
     *
     * @author Gustava Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @param $rowId
     * @return object
     */
    public function getById($rowId)
    {
        // define o método passando o id para editar
        $url = sprintf(URL_TOKEN, "taskmanager/{$rowId}");
        
        // executa o CURL
        $response = SysFunctions::curl($url, 'GET', array());
        
        if($response->status->code != 200 or $response->return->sucess != true){
            throw new Exception("Não foi possível buscar o registro [{$response->status->text}]");
        }
        
        return $response->return->data;
    }
    
    /**
     * Método responsável por deletar uma registro específico
     *
     * @author Gustava Silva <gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @param $rowId
     * @return booelan
     */
    public function deleteById($rowId)
    {
        // define o método passando o id para editar
        $url = sprintf(URL_TOKEN, "taskmanager/{$rowId}");
        
        // executa o CURL
        $response = SysFunctions::curl($url, 'DELETE', array());
        
        if($response->status->code != 200 or $response->return->sucess != true){
            throw new Exception("Não foi possível deletar o registro [{$response->status->text}]");
        }
        
        return $response->return->data;
    }
}