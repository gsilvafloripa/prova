<?php

/**
 * Classe responsável pela regras de negócio 
 *
 * @author Gustavo Silva <gsilva.floripa@gmail.com>
 * @since 03/06/2015
 *
 */
class ModelQuestions
{
    /**
     * Método responsável por aplicar as regras de negócio
     * da listagem de dados verificando os multiplos
     *
     * @author Gustavo Silva<gsilva.floripa@gmail.com>
     * @since 03/06/2015
     * @
     */
    public function listlNumbers($start, $limit)
    {
        // total de resgistros
        $fontStyle = "<span style='color:%s;font-weight:bold'>%s</span>";
        $return = array();
        
        for ($i=$start; $i <= $limit; $i++) {
            $label = (int) $i;
            
            // verifica se é multiplo de 3
            if ($i%3==0) {
                $label = sprintf($fontStyle, '#005984', 'Fizz');
            }
            
            // verifica se é multiplo de 5
            if ($i%5==0) {
                // se o label for int quer dizer que ele não é multiplo
                // de três, então ele não concatena
                $labelBuzz = sprintf($fontStyle, '#C3191E', 'Buzz');
                $label = is_int($label) ? $labelBuzz : $label.$labelBuzz;
            }
            
            // define o dados de cada linha
            $data = array('id' => $i, 'indice' => $i, 'label' => $label);
            
            // define para o array principal
            $return[] = $data;
        }
        
        return $return;
    }
}