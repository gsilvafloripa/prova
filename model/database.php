<?php
/**
 * @author Gustavo Silva
 * @since 06/01/2011
 */

class Database{
    
    public static $PDO   = null;
    
    public static $trans = false;
    
    public static $force = true;
    
    /**
     *  Método contrutor da classe
     *  @author Gustavo Silva
     *  @param $param parametros para conexão
     *  @return objeto PDO
     */
    private static function _createConnection($dns, $user, $pass)
    {
        try{
            
            //Conecta de acordo com o tipo de banco de dados
            $PDO = new PDO($dns, $user, $pass);
            
            // seta os atributos para gerar execessáo
            $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            return $PDO;
            
        }catch(PDOException $e){
            
            throw $e;
            
        }
    }

    /**
     *  Conecta o banco de dados
     *  @author Gustavo Silva
     */
    private static function _connect($force = false)
    {
        $dns  = '';
        $user = DB_USER;
        $pass = DB_PASS;
        $name = DB_NAME;
        $port = DB_PORT;
        $host = DB_HOST;
        $type = DB_TYPE;

        //Verifica o tipo de banco de dados
        if(strtolower($type) == 'mysql'){
            $dns = "mysql:host={$host};port={$port};dbname={$name}";
        }
        
        if( isset(self::$PDO) and self::$force == true){
            
            $PDO = self::$PDO;
            
        }else{
            
            //Cria a conexão
            $PDO = self::_createConnection($dns, $user, $pass);
            
            self::$PDO = $PDO;
        }
        
        return $PDO;
    }

    public static function initTransaction()
    {
        self::$trans = true;
        self::$force = false;
        
        self::_connect('local', true);
        self::$PDO->beginTransaction();
        self::$force = true;
    }
    
    public static function commit()
    {
        self::$PDO->commit();
        self::$trans = false;
        self::$force = true;
        self::$PDO->setAttribute(PDO::ATTR_AUTOCOMMIT, TRUE);
        
    }
    
    public static function rollback()
    {
        self::$trans = false;
        self::$PDO->rollback();
        self::$force = true;
        self::$PDO->setAttribute(PDO::ATTR_AUTOCOMMIT, TRUE);
    }
    
    /**
     *  Realiza uma consulta (SELECT)
     *  @author Gustavo Silva
     *  @param $query
     *  @return objeto
     */
    public static function load($query)
    {
        try{

            $PDO = self::_connect();

            $result = $PDO->prepare($query);
            
            $result->execute();

            if($result != false){
                return $result->fetch(PDO::FETCH_OBJ);
            }else{
                throw $e;
            }

        }catch(PDOException $e){
            //Erro na conexão do banco
            throw $e;
        }
    }

    /**
     *  Realiza uma consulta (SELECT)
     *  @author Gustavo Silva
     *  @param $query
     *  @return objeto
     */
    public static function loadAll($query)
    {
        try{

            $PDO = self::_connect();

            $result = $PDO->prepare($query);
            
            $result->execute();

            if($result != false){
                return $result->fetchAll(PDO::FETCH_OBJ);
            }else{
                throw $e;
            }

        }catch(PDOException $e){
            //Erro na conexão do banco
            throw $e;
        }
    }
    
    /**
     *  Executa uma ação no banco (INSET, UPDATE, DELETE)
     *  @author Gustavo Silva
     *  @param $sql
     *  @return objeto
     */
    public static function execution($sql)
    {
        try{

            $PDO = self::_connect();

            $result = $PDO->exec($sql);
           
           return $PDO->lastInsertId();

        }catch(PDOException $e){
            //Erro na conexão do banco
            throw $e;
        }
    }
    
    /**
     * Define os valores para os campos de log de criação de registros.
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @since 25/03/2013
     *
     * @return array Usuário e data corrente para registro do log.
    **/
    public static function getTrackData() {
        
        $user = $_SESSION['system']['user']['id'];
        $currentDate = date('Y-m-d H:i:s');
        
        return array( 'user' => $user, 'date' => $currentDate);
        
    }
    
    /**
     * Define os campos e valores de log de criação do registro.
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @since 25/03/2013
     *
     * @return array Dados de campo de log de criação e seus valores
    **/
    public static function getCreateData() {
        
        $data = self::getTrackData();
        
        //Dados para created_by, created_date, modified_by, modified_date
        $createValue = array('created_by'    => $data['user'],
                             'created_date'  => $data['date'],
                             'modified_by'   => $data['user'],
                             'modified_date' => $data['date']);
        
        return $createValue;
        
    }
    
    /**
     * Adiciona os campos de log de criação aos campos da tabela.
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @since 25/03/2013
     *
     * @param array $data Dados com campo e valor a serem salvos
     *
     * @return array Os mesmos dados recebidos mais os campos e valores de log
    **/
    public static function setLogCreateData($data) {
        
        $fieldsCreateData = self::getCreateData();
        
        foreach($fieldsCreateData as $field => $value ) {
            
            if( is_array($data) ) {
                
                $data[$field] = $value;
                
            } elseif( is_object($data) ) {
                
                $data->$field = $value;
            }
            
            
            
        }
        
        return $data;
        
    }
    
    /**
     * Define os campos e valores de log de alteração de registro.
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @since 25/03/2013
     *
     * @return array Dados com usuário e data corrente para log de alteração de registro
    **/
    public static function getModifyData() {
        
        $data = self::getTrackData();
        
        //Dados para modified_by e modified_date
        $modifiedData = array('modified_by'   => $data['user'],
                              'modified_date' => $data['date']);
        
        return $modifiedData;
    }
    
    /**
     *  Executa uma ação no banco (INSET, UPDATE, DELETE)
     *  @author Gustavo Silva
     *  @param $table
     *  @param $data array
     *  @param $sql
     *  @return objeto
     */
    public static function insert($table, $data)
    {
        $fields = implode(",", array_keys($data));
        $values = implode(",", $data);
        
        // monsta o sql
        $sql = "INSERT INTO {$table} ({$fields}) VALUES({$values})";
        
        return self::execution($sql);
    }

    /**
     *  Monta um SQL Insert a partir dos dados recebidos.
     *
     *  @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     *  @since 05/03/2013
     *
     *  @param string   $table  Nome da tabela no banco de dados
     *  @param array    $data   Nome do campo como índice do array e seus valores a serem gravados
     *  @param boolean  $logCreateData  Identifica se deve ser gravados os dados de usuário e data de criação
     *
     *  @return int     Último id inserido no banco de dados.
     */
    public static function insertFull($table, $data, $logCreateData = true)
    {
        
        //Verifica se deve ser gravado os dados de criação do registro
        /*if( $logCreateData ) {
            
            $data = self::setLogCreateData($data);
            
        }*/
        
        //Define os campos da tabela a partir das chaves do array
        $fields = array_keys($data);
        
        //Define os valores de cada campo do banco de dados
        $values = array_values($data);
        
        //Prepara os dados para o sql
        $fields = implode(",", $fields);
        $values = "'". implode("','", $values ) ."'";
        
        //Monta o SQL com os valores entre aspas
        $sql = "INSERT INTO {$table} ({$fields}) VALUES({$values})";
        
        return self::execution($sql);
    }
    
    /**
     * Monta sql estendido, uma única instrução INSERT para vários registros
     * a serem inseridos no banco.
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @since 25/03/2013
     *
     * @param string    $table              Nome da tabela de relacionamento no banco de dados.
     * @param array     $data               Dados da tabela de relacionamento.
     * @param int       $foreignKeyValue    Id da chave estrangeira ao qual será feita a referência.
     * @param boolean   $logCreateData      Identifica se deve ser gravados os dados de usuário e data de criação.
     *
     * @return int     Último id inserido na tabela do banco de dados.
    **/
    public static function insertExtended($table, $data, $foreignKeyValue = '', $logCreateData = true) {
        
        $createDataFields = '';
        $createDataValues = '';
        
        //Verifica se deve ser gravado os dados de criação do registro
        if( $logCreateData ) {
            
            $createData = self::getCreateData();
            
            $createDataFields = array_keys($createData);
            $createDataFields = ','. implode(',', $createDataFields);
            
            $createDataValues = array_values($createData);
            $createDataValues = ",'". implode("','", $createDataValues ) ."'";
            
        }
        
        //Define os campos de acordo com o primeiro elemento do array
        $fields = implode(',', array_keys( current($data) ) ) . $createDataFields;
        
        $sqlValues = array();
        foreach( $data as $key => $referenceData ) {
            
            if( is_array($referenceData) ) {
                
                $values = array_values($referenceData);
                
                $sqlPart = implode("','", $values );
                
                $sqlValues[] = "('". $foreignKeyValue . $sqlPart ."'". $createDataValues .")";
                
            } 
            
        }
        
        $sql = "INSERT INTO {$table} ({$fields}) VALUES ". implode(',', $sqlValues);
        
        return self::execution($sql);
        
    }
    
    /**
     * Faz o insert de um item, caso ele já exista o atualiza.
     *
     * @author Gustavo Silva <gsilva.floripa@gmail.com.br>
     * @since 23/04/2013
     *
     * @param string    $table              Nome da tabela de relacionamento no banco de dados.
     * @param array     $data               Dados da tabela de relacionamento.
     * @param int       $foreignKeyValue    Chave estrangeira ao qual será feita a referência.
     * @param boolean   $logCreateData      Identifica se deve ser gravados os dados de usuário e data de criação.
     *
     * @return int     Último id inserido na tabela do banco de dados.
    **/
    public static function insertOnDuplicateUpdate($table, $data, $foreignKeyValue = '', $logCreateData = true) {
        
        
        try {
            
            
            
            $createDataFields = '';
            $createDataValues = '';
            $createDataFieldsOnDuplicated = '';
            
            //Verifica se deve ser gravado os dados de criação do registro
            if( $logCreateData ) {
                
                $createData = self::getCreateData();
                
                //Prepara os campos a serem atualizados
                $createDataFields = array_keys($createData);
                $createDataFields = ','. implode(',', $createDataFields);
                
                //Prepara os valores gerados a partir do self::getCreateData
                $createDataValues = array_values($createData);
                $createDataValues = ",'". implode("','", $createDataValues ) ."'";
                
                //Prepara a sintax do ON DUPLICATE KEY UPDATE para os campos de log
                $createDataFieldsOnDuplicated = array_keys($createData);
                $onDuplicated = array();
                foreach($createDataFieldsOnDuplicated as $key => $field ) {
                    
                    $onDuplicated[] = $field ."= VALUES({$field})";
                    
                }
                $createDataFieldsOnDuplicated = ','. implode(',', $onDuplicated);
                
            }
            
            //Cria a sintax dos campos padrão junto com os campos de log de criação/atualização de registro
            $fields = implode(',', array_keys( current($data) ) ) . $createDataFields;
            
            
            
            //Prepara a sintax do ON DUPLICATE KEY UPDATE para os campos do post
            $fieldsOnDuplicated = array_keys(current($data));
            $onDuplicated = array();
            foreach($fieldsOnDuplicated as $key => $field ) {
                
                $onDuplicated[] = $field ."= VALUES({$field})";
                
            }
            $fieldsOnDuplicated = implode(',', $onDuplicated) . $createDataFieldsOnDuplicated;
            
            
            //Prepara os valores a serem salvos
            $sqlValues = array();
            foreach( $data as $key => $referenceData ) {
                
                if( is_array($referenceData) ) {
                    
                    $values = array_values($referenceData);
                    
                    $sqlPart = implode("','", $values ) ."'";
                    
                    //Monta a sintax dos valores para o insert, juntando os valores de cada campo
                    $sqlValues[] = "('". $foreignKeyValue . $sqlPart . $createDataValues .")";
                    
                }
            }
            
            $sql = "INSERT INTO {$table} ({$fields}) VALUES ". implode(',', $sqlValues) ."
                    ON DUPLICATE KEY UPDATE {$fieldsOnDuplicated}";
            
            return self::execution($sql);
            
        } catch(PDOException $e) {
            
            
            
        }
        
    }
    
    
    public static function delete($table, $where) {
        
        $sql = "DELETE FROM $table WHERE $where";
        return self::execution($sql);
        
    }
    
    /**
     *  Executa insert no banco (INSET, UPDATE, DELETE)
     *  @author Gustavo Silva
     *  @param $table
     *  @param $data array
     *  @param $sql
     *  @return objeto
     */
    public static function insertFullIgnore($table, $data)
    {
        $fields = implode(",", array_keys( current($data) ));

        foreach($data as $key => $value){
            //$data[$key] = "'" . $value . "'";
            
            $sqlValues[] = "('". implode("','", array_values($value) ) ."')";
            
        }
        
        // monsta o sql
        $sql = "INSERT IGNORE INTO {$table} ({$fields}) VALUES ". implode(',', $sqlValues);
        
        return self::execution($sql);
    }

    /**
     *  Executa um update no banco (INSET, UPDATE, DELETE)
     *  @author Gustavo Silva
     *  @param $table
     *  @param $data array
     *  @param $sql
     *  @return objeto
     */
    public static function update($table, $params, $where, $logModifyData = true)
    {
        $data = array();
        
        //Verifica se deve ser gravado os dados de criação do registro
        /*if( $logModifyData ) {
            
            $fieldsModifyData = self::getModifyData();
            
            foreach($fieldsModifyData as $field => $value ) {
                
                if( is_array($params) ) {
                    
                    $params[$field] = $value;
                    
                } elseif( is_object($params) ) {
                    $params->{$field} = $value;
                }
                
            }
            
        }*/
        
        foreach($params as $key => $value){
            $data[] = " {$key} = '{$value}' ";
        }
        
        $data = implode(", ", $data);
        
        // monsta o sql
        $sql = "UPDATE {$table} SET {$data} WHERE {$where}";
        
        return self::execution($sql);
    }
}