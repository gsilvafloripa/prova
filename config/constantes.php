<?php
/**
 * @author Gustavo Silva
 * @since 01/02/2011
 */

define('CHARSET', 'UTF-8');

define('TITLE'  , 'Prova');

define('DB_USER', 'XXXXXXX');
define('DB_PASS', 'XXXXXXX');
define('DB_NAME', 'XXXXXXX');
define('DB_PORT', '3306');
define('DB_HOST', 'localhost');
define('DB_TYPE', 'mysql');

define('URL_SYS', 'xxxxxxx');

define('PATH_IMG_REL' , 'xxxxx');
define('PATH_IMG_AB'  , 'xxxxxx');

define('PATH'        , "../");

define('PATH_PUBLIC' , PATH . "public/");

define('PATH_IMG'    , PATH_PUBLIC . "image/");

define('PATH_CSS'    , PATH_PUBLIC . "css/");

define('PATH_JS'     , PATH_PUBLIC . "js/");

define('PATH_MODULE' , PATH . "module/");

define('PATH_CONTROLLER' , PATH . "controller/");

define('PATH_MODEL'  , PATH . "model/");

define('PATH_VIEW'   , PATH . "view/");

define('PATH_LIB'    , PATH . "lib/");

define('PATH_LIB_SMARTY'   , PATH_LIB . "smarty/");

define('VERSAO_JS'   , 'v3');

define('VERSAO_CSS'   , 'v2');

define('PATH_FILE_CURRICULUM', '/files/curriculum/');

// API

define('PATH_API'  , PATH_MODEL . "api/");

define('TOKEN_API', 'hnklhsa6786dasbdjgh45342jdfsfgfvadrweADSAgASDakg');

define('URL_TOKEN', "http://prova.dev.com.br/api/%s?token=" . TOKEN_API);
