<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><@$smarty.const.TITLE@></title>
        <link type="text/css" href="/css/ui-lightness/jquery-ui-187custom.css" rel="stylesheet" />
        <link type="text/css" href="/css/ui-lightness/jquery-dialogr.css" rel="stylesheet" />
        <link type="text/css" href="/css/elements.css" rel="stylesheet" />
        <link type="text/css" href="/css/sys.css" rel="stylesheet" />

	<script type="text/javascript" src="/js/jquery-144min.js"></script>
        <script type="text/javascript" src="/js/jquery-ui-187custommin.js"></script>
        <script type="text/javascript" src="/js/ui-dialogr.js"></script>
        <script type="text/javascript" src="/js/sys/Login.js?v=<@ $versionJs @>"></script>
        <script type="text/javascript" src="/js/sys/MaskLoad.js?v=<@ $versionJs @>"></script>
	</head>
	<body style="background:#eeeeee">
		<@ include file="dialog.tpl" @>
		<div id="mask"></div>
		<div style="margin-top:100px;width:500px; margin-left:auto; margin-right:auto">
			<div style="background: #e1e1e1; padding: 5px; border-radius: 4px 4px 4px 4px;">
				<div id="content" style="padding:10px; background:#ffffff; border:1px solid #cccccc">
					<fieldset>
						<legend>Acessar sistema</legend>
						<form id="contentForm" name="contentForm" action="/index/userAuthentication/">
							<div style="margin-left:auto; margin-right:auto; width:400px">
								<div style="margin-top: 25px; margin-bottom: 15px; text-align: center;float: left; margin-right: 30px">
									
								</div>
								<div style="float: left">
								<label>Login:</label><input type="text" name="login" id="login" />
								<label>Senha:</label><input type="password" name="password" id="password" />
								<div style="clear: both"></div>
								<div style="margin-top:20px; margin-bottom:20px">
									<span id="buttonEnter" class="classButton" style="float: left"><span style="background: url('/image/icon/accept.png') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span>Entrar</span></span>
									<div style="clear: both"></div>
								</div>
								<div style="font-size:11px"><a href="#" id="linkRecoverPassword">Recuperar senha</a></div>
								</div>
								<div style="clear: both"></div>
							</div>
						</form>
					</fieldset>
				</div>
			</div>
			<div style="clear:both"></div>
				<div>
					<div id="contentPassword" style="padding:10px; display: none;">
						<fieldset>
							<legend>Recuperar senha de acesso</legend>
							<form id="contentPasswordForm" name="contentPasswordForm" action="/index/recoverPassword/">
								<div style="margin-left:auto; margin-right:auto; width:200px">
									<label>Login:</label><input type="text" name="login" id="login" />
									<div style="margin-top:20px; margin-bottom:20px">
											<span id="buttonRecoverPassword" class="classButton"><span style="background: url('/image/icon/accept.png') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span>Enviar</span></span>
									</div>
								</div>
							</form>
						</fieldset>
					</div>
				</div>
		</div>
	</body>
</html>
