<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><@$smarty.const.TITLE@></title>
        <link type="text/css" href="/css/ui-lightness/jquery-ui-187custom.css" rel="stylesheet" />
        <link type="text/css" href="/css/elements.css" rel="stylesheet" />
        <link type="text/css" href="/css/ui-lightness/jquery-dialogr.css" rel="stylesheet" />
        <link type="text/css" href="/css/sys.css" rel="stylesheet" />
        <link type="text/css" href="/css/yui.css" rel="stylesheet" />
        <link type="text/css" href="/css/lib/jquery-notice.css" rel="stylesheet" />

        <script type="text/javascript" src="/js/lib/yui.js"></script>
        <script type="text/javascript" src="/js/jquery-144min.js"></script>
        <script type="text/javascript" src="/js/jquery-ui-187custommin.js"></script>
        <script type="text/javascript" src="/js/ui-dialogr.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.notice.js"></script>
        <script type="text/javascript" src="/js/jquery-limit-1-2.js"></script>
        <script type="text/javascript" src="/js/lib/jquery-form.js"></script>
        <script type="text/javascript" src="/js/lib/jquery.maskediput-1.3.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/js/sys/Functions.js"></script>
        <script type="text/javascript" src="/js/sys/Window.js"></script>
        <script type="text/javascript" src="/js/sys/Dashboard.js"></script>
        <script type="text/javascript" src="/js/sys/Process.js"></script>
        <script type="text/javascript" src="/js/sys/MaskLoad.js"></script>
        <script type="text/javascript" src="/js/sys/Grid.js"></script>
        <script type="text/javascript" src="/js/sys/NewGrid.js"></script>
        <script type="text/javascript" src="/js/sys/LineBus.js"></script>
        <script type="text/javascript" src="/js/sys/TreeView.js"></script>
        
    </head>
    <body onload="document.getElementById('mask_load_system').style.display='none';" class="yui-skin-sam" style="width:100%; height:100%; overflow:hidden; margin: 0px">
        <div id="mask_load_system" class="mask_load_system"><div style="width:400px; margin-left:auto; margin-right:auto; margin-top:300px; text-align:center"><div><img src="/image/loadingBar.gif" style="margin:5% 0px 0px 0px"></div><div><b>Carregando Sistema...</b></div></div></div>
        <@ include file="dialog.tpl" @>
        <div id="dashboard">
            <div>
                <div id="topMenu"></div>
            </div>
            <div id="menus">
                <div style="float:right"><div id="headermenu" style="text-align:right"><h1></h1></div></div>
            </div>
            <!-- end new menu -->
            <div id="mask"></div>
            <div style="height:200px;-moz-border-radius-bottomright: 10px; -moz-border-radius-bottomleft:10px; height:auto">
            <div id="sysContent" style="padding: 2px 3px 2px 3px">
                <div style="width:600px; padding:5px 10px 5px 10px"></div>
            </div>
            </div>
            <div style="width:100%;position:absolute; height:30px; bottom:0px; z-index:100; background:url('/image/img_button.png')">
            <div style="float: left;background: url('/image/icon/user.png') no-repeat scroll 2px 6px transparent; padding: 7px 0px 0px 20px;"><span><b>Usuário: </b><@ $user @></span></div>
            <div style="float:left; padding: 6px 0px 0px 3px">|</div>
            <div style="float: left; background: url('/image/icon/time.png') no-repeat scroll 2px 6px transparent; padding: 7px 0px 0px 20px;"><span><b>Entrada: </b><@ $startDate @></span></div>
            <div style="float: right; margin-top: 6px; margin-right: 5px"></div>
            <div style="float: right"><div id="timer_sys"></div></div>
            </div>
        </div>        
    </body>
</html>

<script type="text/javascript">

    YAHOO.util.Event.onDOMReady(function () {

        var aItemData = eval( <@ $dataMenu @> );

        var oMenuBar = new YAHOO.widget.MenuBar("menubar", {  lazyload: true, itemdata: aItemData  });

        oMenuBar.render(document.body);
    });
    
    Dashboard().postit(<@ $postit @>);
</script>
