<style>
.yui-calcontainer{
    border: 1px solid #cccccc !important;
}
.title{
    color:#999999 !important;
}
</style>
<style>
    #cal2Container_<@ $windowId @> { display:none; position:absolute; left:240px; top:auto; z-index:1; bottom:25px}
</style>
<label for="title_<@ $windowId @>" style="float:left; margin:6px 5px 0px 0px; min-width:100px"><@ $label @></label>
<div style="float:left">
    <INPUT type="text" name="calendarDate" id="calendarDate_<@ $windowId @>" value="<@ if $selectedDate <> '' @><@ $selectedDate @><@ /if @>" size="8" disabled=disabled>
</div>
<div style="float:left; padding:4px"><span id="showCalendar_<@ $windowId @>" style="cursor:pointer"><image id="imgCalendar_<@ $windowId @>" src="image/icon/calendar.png"/></span></div>
<div class="yui-skin-sam" style="font-size:10px;">
    <div id="cal2Container_<@ $windowId @>"></div>
</div>
</div>
<div id="hourMinute_<@ $windowId @>" style="<@ if $selectedHour == '' || $selectedMinute == '' @>display:none<@ /if @>">
<div id="calendar_hour_<@ $windowId @>" style="float:left">
    <SELECT style="visibility:visible" name="calendarHour" id="calendarHour_<@ $windowId@>">
        <@ if is_array($calendarHour) @>
            <@ foreach key = key item = item from = $calendarHour @>
                <OPTION <@ if $key == $selectedHour  @> selected <@ /if @>  value="<@ $item.value @>"><@ $item.label @></OPTION>
            <@ /foreach @>
        <@ /if @>
    </SELECT>
</div>

<div style="padding:4px 3px 0px 3px; float:left">:</div>
<div id="calendar_minute_<@ $windowId @>" style="float:left">
    <SELECT style="visibility:visible" name="calendarMinute" id="calendarMinute_<@ $windowId@>">
        <@ if is_array($calendarMinute) @>
            <@ foreach key = key item = item from = $calendarMinute @>
                <OPTION <@ if $key == $selectedMinute  @> selected <@ /if @> value="<@ $item.value @>"><@ $item.label @></OPTION>
            <@ /foreach @>
        <@ /if @>
    </SELECT>
</div>
<div id="remove_<@ $windowId @>" style="float:left; margin: 7px; cursor: pointer"><img src="/image/icon/cross.png" title="Remover data"/></div>
</div>
<div style="clear:both"></div>
<input type="hidden" value="<@ if $selectedDateDatabase <> '' @><@ $selectedDateDatabase @><@ /if @>" name="<@ $name @>_dateDatabase" id="dateDatabase_<@ $windowId @>"/>
<input type="hidden" value="" name="<@ $name @>_dateView" id="dateView_<@ $windowId @>"/>
<script type="text/javascript">
    YAHOO.namespace("example.calendar");
    YAHOO.example.calendar.init = function() {

       function selectHandler(type, args, obj) {

                var selected = args[0];

                try{
                    obj.hide();
                }catch(e){}

                var elHour   = new YAHOO.util.Element('calendarHour_<@ $windowId@>').get('value');
                var elMinute = new YAHOO.util.Element('calendarMinute_<@ $windowId@>').get('value');

                var dateDatabase = selected[0][0] + "-" + selected[0][1] + "-" + selected[0][2] + " " + elHour + ":" + elMinute;
                var dateView     = selected[0][2] + "/" + selected[0][1] + "/" + selected[0][0];

                var calendarDate = new YAHOO.util.Element('calendarDate_<@ $windowId@>').set('value', dateView);
                var calendarDate = new YAHOO.util.Element('dateDatabase_<@ $windowId@>').set('value', dateDatabase);
                var calendarDate = new YAHOO.util.Element('dateView_<@ $windowId@>').set('value', dateView + " " + elHour + ":" + elMinute);

                YAHOO.util.Dom.setStyle('hourMinute_<@ $windowId @>', 'display', 'block');

 	};

        // instância o elemento calendário
        YAHOO.example.calendar.cal2 = new YAHOO.widget.Calendar("cal2_<@ $windowId @>","cal2Container_<@ $windowId @>", { title:"Seleciona uma data:", close:true } );
        YAHOO.example.calendar.cal2.selectEvent.subscribe(selectHandler, YAHOO.example.calendar.cal2, true);
        YAHOO.example.calendar.cal2.render();

        // Ao clicar em uma data do editor seta o hidden e fecha o elemento
        YAHOO.util.Event.addListener("showCalendar_<@ $windowId @>", "click", YAHOO.example.calendar.cal2.show, YAHOO.example.calendar.cal2, true);

        // Atualiza os dados do hidden
        function update() {

            var valueDate    = new YAHOO.util.Element('calendarDate_<@ $windowId@>').get('value');
            valueDate = valueDate.split("/");
            valueDate = Array(Array(Array(valueDate[2], valueDate[1], valueDate[0])));

            selectHandler('update', valueDate);

        }

        // Adiciona o evento de onChange nos campos de hora e minuto para atualizar os dados
        YAHOO.util.Event.addListener(["calendarHour_<@ $windowId@>", "calendarMinute_<@ $windowId@>"], "change", update);

        function removeDate()
        {
            var calendarDate = new YAHOO.util.Element('calendarDate_<@ $windowId@>').set('value', '');
            var calendarDate = new YAHOO.util.Element('dateDatabase_<@ $windowId@>').set('value', '');
            var calendarDate = new YAHOO.util.Element('dateView_<@ $windowId@>').set('value', '');

            YAHOO.util.Dom.setStyle("hourMinute_<@ $windowId @>", 'display', "none");
        }

        // Adiciona o evento de onChange nos campos de hora e minuto para atualizar os dados
        YAHOO.util.Event.addListener("remove_<@ $windowId@>", "click", removeDate);
    }
    YAHOO.util.Event.onDOMReady(YAHOO.example.calendar.init);
</script>