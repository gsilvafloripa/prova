<div>
    <label style="float:left; margin:5px 5px 0px 0px; min-width:100px" for="<@ $name @>_<@ $windowId @>"><@ $label @></label>
    <SELECT style="visibility:visible; <@ if $size @>width: <@ $size @> <@ /if @>" name="<@ $name @>" id="<@ $name @>_<@ $windowId@>" <@ if $disabled == true@>disabled="disabled"<@ /if @>>
        <@ if is_array($data) @>
            <@ foreach key = key item = item from = $data @>
                <OPTION <@ if $key == $valueSelected  @> selected <@ /if @> label="<@ $item @>" value="<@ $key @>"><@ $item @></OPTION>
            <@ /foreach @>
        <@ /if @>
    </SELECT>
</div>