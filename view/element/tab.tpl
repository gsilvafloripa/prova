<div id="<@ $name @>" class="yui-navset" style="margin-top:7px">
    <ul class="yui-nav">
        <@ foreach key = key item = item from = $data @>
            <li <@ if $selected == $key @>class="selected"<@ /if @>><a href="#<@ $key @>"><em><@ $item.label @></em></a></li>
        <@ /foreach @>
    </ul>
    <div class="yui-content">
        <@ foreach key = key item = item from = $data @>
            <div id="<@ $key @>"><p><@ $item.data @></p></div>
        <@ /foreach @>
    </div>
</div>
<script>
    (function() {
        var tabView = new YAHOO.widget.TabView('<@ $name @>');
    })();
</script>
