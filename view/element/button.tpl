<div id="buttons_<@ $windowId @>">
    <@ foreach from=$buttons key = s item = data @>
    <span id="<@ $data.type @>_<@ $windowId @>" class="classButton" <@ if $data.action != '' @>actionButton="<@ $data.action @>"<@ /if @> <@ if $data.messageConfirm != '' @>messageConfirm="<@ $data.messageConfirm @>"<@ /if @>><span style="background: url('/image/icon/<@ $data.icon @>.png') no-repeat scroll -1px 0pt transparent; padding:0px 0px 0px 20px"></span><span><@ $data.label @></span></span>
    <@ /foreach @>
</div>