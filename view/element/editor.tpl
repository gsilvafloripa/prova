<style>
    .yui-editor-container {
        position: inherit;
    }
    .yui-skin-sam .yui-toolbar-container .yui-toolbar-addMovie span.yui-toolbar-icon {
        background-image: url( '/image/icon/television.png' );
        background-position: 3px 0px;
    }
</style>
<div class="yui-skin-sam"><textarea id="editor_<@ $windowId @>" name="editor"><@ $value @></textarea></div>
<input type="hidden" name="windowId" value="<@ $windowId @>"/>
<script type="text/javascript">

(function() {

    var Dom = YAHOO.util.Dom,
        Event = YAHOO.util.Event,
        win = null;

    var editorTemplate = new YAHOO.widget.Editor("editor_<@ $windowId @>", {
        height  : '360',
        width   : '100%',
        titlebar: '',
        dompath : true,
        animate : true,
    });
    editorTemplate.on('editorKeyPress', function(){
        editorTemplate.saveHTML();
    });
    editorTemplate.on('beforeNodeChange', function(){
        editorTemplate.saveHTML();
    });
    editorTemplate.on('toolbarLoaded', function() {
        //When the toolbar is loaded, add a listener to the insertimage button
        this.toolbar.on('insertimageClick', function() {
            //Get the selected element
            var _sel = this._getSelectedElement();
            //If the selected element is an image, do the normal thing so they can manipulate the image
            if (_sel && _sel.tagName && (_sel.tagName.toLowerCase() == 'img')) {
                //Do the normal thing here..
            } else {
                //Abra uma janela com as imagens
                Window().create({'url':'/file/showFileDir/?windowParent=<@ $windowId @>', 'title':'Disco virtual', 'icon':'group', 'width':920, 'height':400, 'modal':true});
                return false;
            }
        }, this, true);
    }, editorTemplate, true);
    
    editorTemplate.on('toolbarLoaded', function() {
        //Simple button config
        var button = [{
            type: 'push',
            label: 'Adicionar vídeo',
            value: 'addMovie',
            disabled: false
        }];

        this.toolbar.addButtonGroup({label: 'Vídeos', group: 'insertMovie', buttons: button});

        this.toolbar.on('addMovieClick', function(o) {
            var ttt = '%INIT_VIDEO%http://www.terra.com.br%END_VIDEO%';

            Window().create({'url':'/file/showInputMovie/?windowParent=<@ $windowId @>', 'title':'Adicionar vídeo', 'icon':'television', 'width':450, 'height':50, 'modal':true});

        }, editorTemplate, true);

        //Setup the button to be enabled, disabled or selected
        this.on('afterNodeChange', function(o) {
            //Get the selected element
            var el = this._getSelectedElement();

            //Get the button we want to manipulate
            var button = this.toolbar.getButtonByValue('addMovie');

            if (el && el.tagName == 'div') {
                this.toolbar.enableButton(button);
            }
        }, this, true);
    }, editorTemplate, true);

    editorTemplate.render();

})();
</script>