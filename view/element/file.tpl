<div style="padding: 3px">
    <label style="float:left; margin:5px 5px 0px 0px; min-width:100px" for="<@ $name @>_<@ $windowId @>"><@ $label @></label>
    <div id="container_<@ $windowId @>" style="float:left"><div id="element_<@ $name @>_<@ $windowId @>"><@ if $value @><img src="<@ $value @>" /><@ /if @></div></div>
    <div style="float:left; cursor:pointer; padding: 4px" onclick="Window().create({'url':'/file/showFileDir/?windowParent=<@ $windowId @>&elementParent=element_<@ $name @>_<@ $windowId @>&name=<@ $name @>&width=<@ $width @>&height=<@ $height @>', 'title':'Disco virtual', 'icon':'group', 'width':920, 'height':400, 'modal':true});"><img src="/image/icon/add.png" /></div>
    <div id="removeElement_<@ $name @>_<@ $windowId @>" style="float:left; cursor:pointer; padding: 4px"><img src="/image/icon/cross.png"/></div>
    <div style="clear:both"></div>
    <@ if $message @>
    <div style="margin:5px"><span style="border:1px solid #ffe45c; margin: 3px; background: #FFFFCC; margin-left: 100px; padding: 6px; font-size: 12px; font-weight:normal; border-radius:4px"><@ $message @></span></div>
    <@ /if @>
    <input type="hidden" value="<@ $value @>" name="elementFile_<@ $name @>" id="elementFile_<@ $name @>_<@ $windowId @>">
</div>
<script>
    YAHOO.util.Event.addListener("removeElement_<@ $name @>_<@ $windowId @>", "click", function(){

        YAHOO.util.Dom.get("element_<@ $name @>_<@ $windowId @>").innerHTML = '';
        new YAHOO.util.Element('elementFile_<@ $name @>_<@ $windowId @>').set('value', '');

    });
</script>