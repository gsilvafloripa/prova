<style>
    
    .contentGridPaginationLink ul{
        margin : 0px;
        padding : 0px;
    }
    
    .contentGridPaginationLink li{
        float           : left;
        list-style-type : none;
        padding         : 3px 3px 3px 3px;
        border          : 1px solid #C1C1C1;
        margin          : 0px 0px 0px 1px;
        background      : #F5F5F5;
        color           : #999999;
        min-width       : 17px;
        height          : 15px;
        border          : 1px solid #C1C1C1;
        cursor          : pointer;
        text-align      : center;
        -moz-border-radius : 2px;
    }
    
    .contentGridPaginationLink li:hover{
        background : #FCEC94;
        color      : #333333;
    }

    .contentGridPaginationLink selected{
        background:#FFFFFF;
    }

    /*.bd{
        border: 1px solid #cccccc !important;
        border-radius: 4px;
    }*/

    .yui-menu-shadow-visible{
        border-radius: 4px;
    }
    
</style>
<div id="componentGrid_<@ $windowId @>" style="width:<@ $width @>; height:auto">
<div id="contentGrid_<@ $windowId @>">
<div>

<table id="table_<@ $windowId @>" width="100%" cellspacing="0" cellpadding="0" class="tableHeader">
    <tbody>
        <tr class="tableBgHeader">
            <@ foreach key = key item = item from = $metadata.metadata @>
                <@ if $item.id <> 'id' @>
                <th align="<@ $item.align @>" width="<@ $item.width @>"><@ $item.header @></th>
                <@ /if @>
            <@ /foreach  @>
        </tr>

        <@ if $metadata.data|@count > 0 @>
            <@ assign var = 'firstTd' value = true @>
            <@ assign var = 'firstTr' value = false @>

            <@ foreach key = key item = item from = $metadata.data @>

                <tr <@ if $firstTr == true @> class="tableBgRow" <@ /if @>>
                    <@ if $firstTr == true @> <@ assign var = 'firstTr' value = false @> <@ else @> <@ assign var = 'firstTr' value = true @> <@ /if @>
                
                    <@ foreach key = keyL item = itemL from = $item @>
            
                        <@ if $metadata.metadata[$keyL].id <> 'id' @>
                        <@ if $firstTd == true  @>
                        <td id="<@ $metadata.data[$key][0] @>" class="rowSegment">
                            <div style="background: none repeat scroll 0% 0% rgb(37, 158, 1); float: left; margin: 2px 5px 0pt 0pt ! important;"></div>
                            <@ if $itemL <> '' @><@ $itemL @><@ else @> . <@ /if @>
                        </td>
                         <@ assign var = 'firstTd' value = false @>
                        <@ else @>
                        <td id="<@ $metadata.data[$key][0] @>" class="rowRight"><@ if $itemL <> '' @><@ $itemL @><@ else @> &nbsp <@ /if @></td>
                        <@ /if @>
                        <@ /if @>
                    <@ /foreach @>
                    <@ assign var = 'firstTd' value = true @>
                </tr>
            <@ /foreach @>
        <@ else @>
        <div>Nenhum registro encontrado.</div>
        <@ /if @>
    </tbody>
</table>

</div>
<div  id="contentGridPagination_<@ $windowId @>"style="width:100%; height:30px; padding:10px 0px 10px 0px; float:none">
    <@ if $metadata.data|@count > 0 @>
    <div style="float:left">
    <@ if $pagination == true @>
    <select id="pagLimit_<@ $windowId @>">
        <@ foreach key = key item = item from = $pagLimit @>
            <option label="<@ $item @>" <@ if $item == $limit @> selected="selected" <@ /if @> value="<@ $item @>"><@ $item @></option>
        <@ /foreach @>
    </select>
    <@ /if @>
    </div>
    <div class="contentGridPaginationLink" id="contentGridPaginationLink_<@ $windowId @>" style="float:left; margin:2px 1px 1px 3px">
        <ul>
            <@ if $pagination == true @>
            <li id="_<@ $linkPaginationFirst @>"><< </li>
            <li id="_<@ $before @>"><</li>
            <@ /if @>
            <@ foreach key = key item = item from = $linkPagination @>
                <li id="<@ $item @>" <@ if $item == $start @> style="background:#FCEC94; color:#333333;" <@ /if @>> <@ $key @> </li>
            <@ /foreach @>
            <@ if $pagination == true @>
            <li id="_<@ $after @>" > > </li>
            <li id="_<@ $linkPaginationLast @>"> >> </li>
            <@ /if @>
        </ul>
    </div>
    <@ /if @>
</div>
</div>
</div>
<input type="hidden" id="pagStart_<@ $windowId @>" name="pagStart" value="<@ $pagStart @>">
