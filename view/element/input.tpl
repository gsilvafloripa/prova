<div>
<@ if $type <> 'hidden' @>
    <@ if ($type <> 'radio') && ($type <> 'checkbox') @><label style="float:left; margin:6px 5px 0px 0px; min-width:100px" for="<@ $name @>_<@ $windowId @>"><@ $label @></label><@ else @><div style="min-width:106px; float:left">&nbsp;</div><@ /if @>
<@ /if @>
    <INPUT type="<@ $type @>" name="<@ $name @>" id="<@ $name @>_<@ $windowId @>" <@ if $type == 'checkbox' && $value == true @>checked="" <@ /if @> value="<@ $value @>" size="<@ $size @>" maxlength="<@ $maxlength @>" alt="<@ $alt @>" <@ if $disabled == true@>disabled="disabled"<@ /if @> >
<@ if ($type == 'radio') || ($type == 'checkbox') @><label style="cursor:pointer;margin:6px 5px 0px 0px;" for="<@ $name @>_<@ $windowId @>"><@ $label @></label><@ /if @>
</div>